<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*
	Home Page
*/
Route::any('/',function() {
	return View::make('pages.home');
});


/*
	Logout Page
*/
Route::any('/logout',function() {
	//Clear Session
	Session::flush();
	
	return View::make('pages.home');
});



/*
	Confirmation Page
*/
Route::any('/confirmation',function() {
	
	return View::make('pages.confirmation');
});




/*
	Login Page
*/
Route::any('/login',function() {

	if ( Session::get('registereduser') ) {

		$regId = Session::get('userid');
		$userRegData = App::make('AppController')->getUserRegDetails( $regId );

		$jobRole = $userRegData[0]->jobrole;
		//Check what module is selected based on job role
		if ($jobRole == "2"  || $jobRole == "3" ) {
			$module = "2";
		} else {
			$module = "1";
		}

		//Get User reg. first name
		$name =  $userRegData[0]->firstname;

		//Get Total key acquired 
		$totalKeyCount = App::make('AppController')->getUserKeyCount( $userRegData[0]->id );


		return View::make('pages.complete', array('module' => $module , 'name' => $name , 'keycount' => $totalKeyCount ) );

	} else {

		return View::make('pages.login');

	}

});


/*
	Response / Thank you  page
*/
Route::any('/response',function() {
	return View::make('pages.response');
});





/*
	Register Page
*/
Route::any('/register',function() {
	return View::make('pages.register');
});


/*
	How To Play Page
*/
Route::any('/howtoplay',function() {
	return View::make('pages.howtoplay');
});



/*
	Learning Module Page
*/
Route::any('/learnmodule',function() {
	return View::make('pages.learnmodule');			
});



/*
	Dashboard page

	Usage : http://domain.com/index.php/dashboard/thankyou/1/xerxis

	http://phpuat.flok.co/coopervision-site/public/dashboard/complete/1/xerxis
	http://phpuat.flok.co/coopervision-site/public/dashboard/complete/2/xerxis


*/
Route::any('/dashboard/{status}/{module}/{name}/{keycount?}',function($status,$module = null , $name = null, $keycount = null) {
	
	switch ($status) {

		case 'thankyou':
			return View::make('pages.thankyou' ,  array('module' => $module , 'name' => $name ) );							
			break;

		case 'complete':
			return View::make('pages.complete', array('module' => $module , 'name' => $name , 'keycount' => $keycount ) );
			break;

		
		default:
			break;
	}
	

});



/*
	Module Page
*/
Route::any('/module/{module?}',function($module) {

	switch ($module) {
		case '2':
				return View::make('pages.practicestaff');			
				break;

		case '1':				
		default :
				return View::make('pages.optometrists');			
				break;
	}

});



/*
	Rank Page
*/
Route::any('/rank',function() {
	return View::make('pages.rank');			
});


/*
	Refer Page
*/
Route::any('/refer',function() {
	return View::make('pages.refer');			
});


/*
	Terms and Condition Page
*/
Route::any('/tnc',function() {
	return View::make('pages.tnc');			
});


/*
	AJAX Calls ( Application API )
*/
Route::controller('api', 'AppController');


Route::get('checker', 'AppController@cvchecker');




//temporary route for generating key code for sales rep
Route::any('/gencodeforsalesrep/{salesrepid?}',function($salesrepid) {
  App::make('AppController')->awardKeyCodeToSalesRep($salesrepid);
});


/*
	Admin Page
*/
Route::any('/admin', function() {

	//Check if still login to session
	if ( Session::get('adminisin') ) {

		//Get to the report page
		$data = App::make('AdminController')->get_total_reports();
		return View::make('admin.report', $data);	

	} else {

		//Get Form inputs
		$requests = Input::all();
		$credentials =[
			'username' => isset($requests['username']) ? $requests['username'] : null,
			'password' => isset($requests['password']) ? $requests['password']  : null
		];

		//check if valid admin account
		if 	( App::make('AdminController')->is_validadmin( $credentials['username'] , $credentials['password'] ) ) {	
			
			//Save Session
			Session::put('adminisin' , true);
			//Get to the report page
			$data = App::make('AdminController')->get_total_reports();
			return View::make('admin.report', $data);	
			

		} else {

	 		$data = array('error' => 'Access Denied');
			return View::make('admin.login', $data);	

		}

	}	

});


/*
	Admin Download Page
	@param Optional 'Report'
*/
Route::any('/admin/downloads/{report?}' , function($report = null) {

	$ts = date('Y_m_d_H_i_s');

	//Check if still login to session
	if ( Session::get('adminisin') ) {

		switch ($report) {

			case 'registrants':
				$filename = $ts . '_registrants.csv';			
				$contents = App::make('AdminController')->registrants_report();
				$response = Response::make($contents, 200);
				$response->header('Content-Type', 'text/csv');
				$response->header('Content-Disposition', 'attachment;filename='.  $filename .';');
				$response->header('Pragma', 'no-cache');
				$response->header('Expires', '0');
				return $response;
				break;

			case 'keycodes':
				$filename = $ts . '_ticketcodes.csv';			
				$contents = App::make('AdminController')->keycodes_report();
				$response = Response::make($contents, 200);
				$response->header('Content-Type', 'text/csv');
				$response->header('Content-Disposition', 'attachment;filename='.  $filename .';');
				$response->header('Pragma', 'no-cache');
				$response->header('Expires', '0');
				return $response;
				break;


			case 'gameplay':
				$filename = $ts . '_rankinggame.csv';			
				$contents = App::make('AdminController')->rankinggame_report();
				$response = Response::make($contents, 200);
				$response->header('Content-Type', 'text/csv');
				$response->header('Content-Disposition', 'attachment;filename='.  $filename .';');
				$response->header('Pragma', 'no-cache');
				$response->header('Expires', '0');
				return $response;
				break;


			case 'salesrep':
				$filename = $ts . '_salesrep.csv';			
				$contents = App::make('AdminController')->sales_rep_report();
				$response = Response::make($contents, 200);
				$response->header('Content-Type', 'text/csv');
				$response->header('Content-Disposition', 'attachment;filename='.  $filename .';');
				$response->header('Pragma', 'no-cache');
				$response->header('Expires', '0');
				return $response;
				break;


			default:
				return View::make('admin.download');	
				break;
		}
	

	} else {

 		$data = array('error' => 'Access Denied');
		return View::make('admin.login', $data);	


	}
});


/*
	Logout Page
*/
Route::any('/admin/logout' ,function(){
	//Clear session
	Session::forget('adminisin');

	$data = array('error' => 'Welcome');
	return View::make('admin.login', $data);		

});
