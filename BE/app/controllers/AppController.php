<?php

class AppController extends \BaseController {


	/*
		Custom link to reset database		
	*/
	public function getResetdatabase() {

		DB::beginTransaction();
		DB::transaction(function()  {
			
			DB::table('error_logs')->truncate();
			DB::table('gameplay')->truncate();			
			DB::table('keys')->truncate();
			DB::table('referral')->truncate();			
			DB::table('registrants')->truncate();
	        $salesRepDetais = [
	            ['firstname'=> 'Angi'   , 'lastname'=> 'Hill'       , 'email'=> 'ahill@au.coopervision.com'     ],
	            ['firstname'=> 'Cherie' , 'lastname'=> 'Norton'     , 'email'=> 'cnorton@coopervision.com'      ],            
	            ['firstname'=> 'Erlen'  , 'lastname'=> 'Septiana'   , 'email'=> 'eseptiana@au.coopervision.com' ],
	            ['firstname'=> 'Leah'   , 'lastname'=> 'Hollway'    , 'email'=> 'Lhollway@au.coopervision.com'  ],            
	            ['firstname'=> 'Deepa'  , 'lastname'=> 'Bhana'      , 'email'=> 'Dbhana@au.coopervision.com'    ],
	            ['firstname'=> 'Paul'   , 'lastname'=> 'Churchill'  , 'email'=> 'Pchurchill@au.coopervision.com'],            
	            ['firstname'=> 'Peter'  , 'lastname'=> 'Fahey'      , 'email'=> 'Pfahey@au.coopervision.com'    ],
	            ['firstname'=> 'Phil'   , 'lastname'=> 'Zirbel'     , 'email'=> 'Pzirbel@au.coopervision.com'   ],            
	            ['firstname'=> 'Sarah'  , 'lastname'=> 'Mahony'     , 'email'=> 'Smahony@au.coopervision.com'   ],
	            ['firstname'=> 'Sharon' , 'lastname'=> 'Steinert'   , 'email'=> 'Ssteinert@au.coopervision.com' ],            
	            ['firstname'=> 'uat' 	, 'lastname'=> 'flok'       , 'email'=> 'amelia@bamboomarketing.com.au' ]            
	        ];

	        foreach ($salesRepDetais as $sr) {
	            try {
	                DB::table('registrants')->insert($sr);
	            } catch(Exception $e) {}            
	        }			
			DB::table('trainings')->truncate();					
			DB::table('sales_rep')->where('rep_no', 'AAAAAAAAA' )->update(array(
					'no_credits' => 8,
					'no_keys' => 0
					));
		    
		});
		DB::commit();

		echo "You have cleared database entries.<br />"
		     ."Note: <br />"
		     ."Dummy Sales rep is initalized with 8 credits";

	}



	/*
		Cronjob that will run to send training EDM's from DB lists	
		$mode = 'Completed or UnComplete training'
		$edmno = Which of the 3 EDM to send	( 3 dates )

		Cron Job : 
		# m h  dom mon dow   command

		#---- CooperVision Cron Job for System EDM  ----------------

		#-- After 3 weeks ( April 28, 2014 )
		0 9 28 4 * nohup curl http://www.coopervisiontraining.com.au/index.php/api/send-system-edm/1/1 > /var/www/coopervisioncronjob.out 2> /var/www/coopervisioncronjob.err < /dev/null &

		0 9 28 4 * nohup curl http://www.coopervisiontraining.com.au/index.php/api/send-system-edm/2/1 > /var/www/coopervisioncronjob.out 2> /var/www/coopervisioncronjob.err < /dev/null &


		#-- After 6 weeks ( May 19, 2014 )
		0 9 19 5 * nohup curl http://www.coopervisiontraining.com.au/index.php/api/send-system-edm/1/2 > /var/www/coopervisioncronjob.out 2> /var/www/coopervisioncronjob.err < /dev/null &

		0 9 19 5 * nohup curl http://www.coopervisiontraining.com.au/index.php/api/send-system-edm/2/2 > /var/www/coopervisioncronjob.out 2> /var/www/coopervisioncronjob.err < /dev/null &



		#-- After 11 weeks ( June 23, 2014 )
		0 9 23 6 * nohup curl http://www.coopervisiontraining.com.au/index.php/api/send-system-edm/1/3 > /var/www/coopervisioncronjob.out 2> /var/www/coopervisioncronjob.err < /dev/null &

		0 9 23 6 * nohup curl http://www.coopervisiontraining.com.au/index.php/api/send-system-edm/2/3 > /var/www/coopervisioncronjob.out 2> /var/www/coopervisioncronjob.err < /dev/null &
	*/
	public function getSendSystemEdm($mode, $edmno)	{

		//Completed Training
		if ( $mode == '1' )	{
			
			//Pull email reciepients
			$data = $this->pullTrainingEdmEmails($mode);

			//check which EDM so send
			if ( $edmno == '1' ) {	
				foreach ($data as $row) {
					$username = $this->pullRegistrantsNameViaEmail($row->email);
					//echo "Name: " . $username . " , Email : " . $row->email . "<br />";
					$this->getSendTrainingEdm( $mode, $edmno, $username, $row->email);
				}
				//Log when was this was sent
				$this->app_log_error('getSendSystemEdm','Sent out System EDM (Training Complete) for April 28,2014');
			}

			//check which EDM so send
			if ( $edmno == '2' ) {	
				foreach ($data as $row) {
					$username = $this->pullRegistrantsNameViaEmail($row->email);
					//echo "Name: " . $username . " , Email : " . $row->email . "<br />";
					$this->getSendTrainingEdm( $mode, $edmno, $username, $row->email);
				}
				//Log when was this was sent
				$this->app_log_error('getSendSystemEdm','Sent out System EDM (Training Complete) for May 19,2014');				
			}			


			//check which EDM so send
			if ( $edmno == '3' ) {	
				foreach ($data as $row) {
					$username = $this->pullRegistrantsNameViaEmail($row->email);
					//echo "Name: " . $username . " , Email : " . $row->email . "<br />";
					$this->getSendTrainingEdm( $mode, $edmno, $username, $row->email);
				}
				//Log when was this was sent
				$this->app_log_error('getSendSystemEdm','Sent out System EDM (Training Complete) for June 23,2014');								
			}	


		}

		//UnComplete Training
		if ( $mode == '2' ) {

			//Pull email reciepients
			$data = $this->pullTrainingEdmEmails($mode);

			//check which EDM so send
			if ( $edmno == '1' ) {	
				foreach ($data as $row) {
					$username = $this->pullRegistrantsNameViaEmail($row->email);
					//echo "Name: " . $username . " , Email : " . $row->email . "<br />";
					$this->getSendTrainingEdm( $mode, $edmno, $username, $row->email);
				}
				//Log when was this was sent
				$this->app_log_error('getSendSystemEdm','Sent out System EDM (Training UnComplete) for April 28,2014');

			}

			//check which EDM so send
			if ( $edmno == '2' ) {	
				foreach ($data as $row) {
					$username = $this->pullRegistrantsNameViaEmail($row->email);
					//echo "Name: " . $username . " , Email : " . $row->email . "<br />";
					$this->getSendTrainingEdm( $mode, $edmno, $username, $row->email);
				}
				//Log when was this was sent
				$this->app_log_error('getSendSystemEdm','Sent out System EDM (Training UnComplete) for May 19,2014');

			}			


			//check which EDM so send
			if ( $edmno == '3' ) {	
				foreach ($data as $row) {
					$username = $this->pullRegistrantsNameViaEmail($row->email);
					//echo "Name: " . $username . " , Email : " . $row->email . "<br />";
					$this->getSendTrainingEdm( $mode, $edmno, $username, $row->email);
				}
				//Log when was this was sent
				$this->app_log_error('getSendSystemEdm','Sent out System EDM (Training UnComplete) for June 23,2014');

			}

		}

	}


	/*
		Pull registrants Name via Email
	*/
	private function pullRegistrantsNameViaEmail( $email ) {
		return DB::table('registrants')->where('email', $email )->pluck('firstname');
	}


	/*
		Training EDM email lists getter
		$mode = 'Completed or UnComplete training'
		Ref : http://laravel.com/docs/queries#advanced-wheres
	*/
	public function pullTrainingEdmEmails($mode) {

		if ($mode == '1') {

			$data = DB::table('registrants')
						->select(['email'])
						->whereIn('id',function($query) {
							//Subquery
							$query->select(['user_reg_id'])
								  ->from('trainings')							
								  ->where('status','=','completed');	
						})
						->get();

		}

		if ( $mode == '2') {

			$data = DB::table('registrants')
						->select(['email'])
						->whereIn('id',function($query) {
							//Subquery
							$query->select(['user_reg_id'])
								  ->from('trainings')
								  ->where('status','=','pending');	
						})
						->get();
		}

		return $data;
	}




	/*
		Controller method  that sends training EDM
		$mode = 1 - completed, 2 - uncomplete

		$mode = 'Completed or UnComplete training'
		$edmno = Which of the 3 EDM to send	( 3 dates )
		(Used for testing EDM as well)
	*/
	public function getSendTrainingEdm($mode, $edmNo, $name, $email ) {

		if ( $mode == '1' ) {

			$this->sendCompleteTrainingEdm( $edmNo, $name, $email );

		} elseif( $mode == '2' ) {

			$this->sendUnCompleteTrainingEdm( $edmNo, $name, $email );
			
		} else {

			echo "EDM method missing";

		}


	}



	/*
		Send EDM to user who completed the training
	*/
	public function sendCompleteTrainingEdm( $edmNo, $name, $email ) {


		try {

			//Check which EDM to send ( Subject )
			switch ($edmNo) {			
				case '1':
					$subjecttitle = 	$name . ', Thanks for Getting Started!';	
					break;				

				case '2':
					$subjecttitle = 	'With "Visions of Vegas," the More You Refer, the More Tickets You Earn!';	
					break;				

				case '3':
					$subjecttitle = 	'Hurry, ' . $name . '! Only 2 Weeks Left to Earn More Tickets!';	
					break;				


				default:
					$subjecttitle = 	$name . ', Thanks for Getting Started!';	
					break;				

			}			

			$emailData = array(
				'subject' => $subjecttitle,
				'emailFrom' => 'noreply@coopervision.com',
				'SenderName' => 'CooperVision',
			    'emailTo'=> $email,
			    'RcvrName'=> $name
			);
			 
			// the data that will be passed into the mail view blade template
			$data = array(
				'username' => $name
			);
			 
			//Check which EDM to send ( Template )
			switch ($edmNo) {
				case '1':
					$edm = 	'pages.completetrainingemail1';	
					break;
				
				case '2':
					$edm = 	'pages.completetrainingemail2';	
					break;

				case '3':
					$edm = 	'pages.completetrainingemail3';	
					break;

				default:
					$edm = 	'pages.completetrainingemail1';	
					break;
			}
			

			Mail::send( $edm , $data, function($message) use ($emailData)
			{
			  $message->from( $emailData['emailFrom'] , $emailData['SenderName'] );
			  $message->to( $emailData['emailTo'] , $emailData['RcvrName']  )->subject( $emailData['subject'] );
			});

			return "SENT";

		} catch(Exception $e) {

			//Log what happen here
			$this->app_log_error('sendCompleteTrainingEdm', "EmailTo: ". $email . " >> Error:" . $e->getMessage());

			return $e->getMessage();
		}


	}



	/*
		Send EDM to user who have not completed the training yet
	*/
	public function sendUnCompleteTrainingEdm( $edmNo, $name, $email ) {


		try {

			//Check which EDM to send ( Subject )
			switch ($edmNo) {			
				case '1':
					$subjecttitle = 	$name . ', Thanks for Getting Started!';	
					break;				

				case '2':
					$subjecttitle = 	$name . ', Complete Your Learning Module for an Extra Chance to Win!';	
					break;				

				case '3':
					$subjecttitle = 	$name . ', Hurry! Only 2 Weeks Left to Earn "Visions of Vegas" Tickets!';
					break;				


				default:
					$subjecttitle = 	$name . ', Thanks for Getting Started!';	
					break;				

			}			

			$emailData = array(
				'subject' => $subjecttitle,
				'emailFrom' => 'noreply@coopervision.com',
				'SenderName' => 'CooperVision',
			    'emailTo'=> $email,
			    'RcvrName'=> $name
			);
			 
			// the data that will be passed into the mail view blade template
			$data = array(
				'username' => $name
			);
			 
			//Check which EDM to send ( Template )
			switch ($edmNo) {

				case '1':
					$edm = 	'pages.uncompletetrainingemail1';	
					break;
				
				case '2':
					$edm = 	'pages.uncompletetrainingemail2';	
					break;

				case '3':
					$edm = 	'pages.uncompletetrainingemail3';	
					break;

				default:
					$edm = 	'pages.uncompletetrainingemail1';	
					break;
			}
			

			Mail::send( $edm , $data, function($message) use ($emailData)
			{
			  $message->from( $emailData['emailFrom'] , $emailData['SenderName'] );
			  $message->to( $emailData['emailTo'] , $emailData['RcvrName']  )->subject( $emailData['subject'] );
			});

			return "SENT";

		} catch(Exception $e) {

			//Log what happen here
			$this->app_log_error('sendUnCompleteTrainingEdm', "EmailTo: ". $email . " >> Error:" . $e->getMessage());

			return $e->getMessage();
		}


	}

	/*
		Update No. Of SR keys used
	*/
	public function updateNoOfSRKeysUsed($regid) {
		$srdetails = $this->getUserRegDetails($regid);
		$salesRepId = DB::table('sales_rep')->where('email', $srdetails[0]->email )->pluck('rep_no');
		$curKeys = DB::table('sales_rep')->where('rep_no', $salesRepId )->pluck('no_keys');

		DB::table('sales_rep')->where('rep_no', $salesRepId )->update(array('no_keys' => $curKeys+1 ));
	}




	/*
		Save Game Play
	*/
	public function getSaveGamePlay() {

		$data = Input::all(true); //Enable XSS filtering
		$regId = $data['regid'];
		$keycode = $data['kcode'];
		$sortings = $data['sorting'];

		//Get Server Timestamp base on application timezone
		$appTimeStamp = $this->get_app_timestamp();	

		try {	

			//Save Gameplay			
			$sql = 'INSERT IGNORE INTO gameplay(user_reg_id,keycode,sortings,date_played) VALUES('.$regId.',"'.$keycode.'","'.$sortings.'","'. $appTimeStamp .'")';
			DB::statement($sql);    

			/*
			DB::table('gameplay')->insert(
				array(
					'user_reg_id' => $regId ,
					'keycode' => $keycode ,				
					'sortings' => $sortings,
					'date_played' => $appTimeStamp
					)
			);
			*/
			

			//Update Sales Rep no. of keys used
			$this->updateNoOfSRKeysUsed($regId);


			//Send user GamePlay email
			$this->SendGamePlayEmail($regId, $sortings);

			return Response::json(array(
    			'status' => 'success',
    			'msg' => 'Successfully saved user gameplay'
    		));					


		} catch(Exception $e) {			

			//Log what happen here
			$this->app_log_error('getSaveGamePlay', 'KEY Code:' . $keycode . ' Error:' . $e->getMessage());

   			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'Something went wrong, check developer for more detais: Error :'.$e->getMessage()
    		));												   		
		}

	}	

	/*
		Send Game Play email notification
	*/
	protected function SendGamePlayEmail($userRegId,$sortings) {

		//get User Reg. Email and Name
		$userRegData = $this->getUserRegDetails( $userRegId );
		$userName = $userRegData[0]->firstname;
		$userEmail = $userRegData[0]->email;
		$rankingOrder = explode("|",$sortings);

		try {

			$emailData = array(
				'subject' => 'Visions of Vegas - Entry Confirmed',
				'emailFrom' => 'noreply@coopervision.com',
				'SenderName' => 'CooperVision',
			    'emailTo'=> $userEmail,
			    'RcvrName'=> $userName
			);
			 
			// the data that will be passed into the mail view blade template
			$data = array(
				'username' => $userName,	
				'rankings'=> $rankingOrder
			);
			 
			Mail::send('pages.rankingemail', $data, function($message) use ($emailData)
			{
			  $message->from( $emailData['emailFrom'] , $emailData['SenderName'] );
			  $message->to( $emailData['emailTo'] , $emailData['RcvrName']  )->subject( $emailData['subject'] );
			});

			return "SENT";

		} catch(Exception $e) {

			//Log what happen here
			$this->app_log_error('SendGamePlayEmail', $e->getMessage());

			return $e->getMessage();
		}
	}




	/*
		Send Referal Email
	*/
	public function getSendReferals() {

		$data = Input::all(true); //Enable XSS filtering
		$senderData = $data['sndr'];
		$rcvrData = $data['rcvr'];

		//validates emails first
		foreach ($rcvrData as $rdata) {
			if ( !filter_var($rdata['email'], FILTER_VALIDATE_EMAIL) ) {
	   			return Response::json(array(
					'status' => 'failed',
	    			'msg' => 'Invalid Email : ' . $rdata['email']
		    	));					
			}
		}

		//Filter only those email that is not yet on our db
		//Because only the first to send email to a friends gets the credit
		//Get valid recipients
		$validRecipeints = $this->ValidateReferalRecipients($rcvrData);

		//Send invidual referal email and get status
		// NOTE : Ignoring if email really exists
		foreach ($validRecipeints as $rdata) {
			$this->SendReferalEmail($senderData['sndrregid'],$rdata['fname'],$rdata['email']);
		}


		//Get Server Timestamp base on application timezone
		$appTimeStamp = $this->get_app_timestamp();	

		//Save Referal detals
		//Only save those valid, ignoring those that already in the database (rcpt email)
		$datas = array(
			'senderinfo' => $senderData,
			'rcvrsinfo' => $validRecipeints,
			'apptime' => $appTimeStamp
		);
		DB::beginTransaction();
		DB::transaction(function() use ($datas) {
			
			foreach ($datas['rcvrsinfo'] as $rdata) {

				//Do not allow SR to refer
				//Check if Sender is a SR
				if (!$this->isUserRegisteredSalesRep($datas['senderinfo']['sndrregid'])) {

					DB::table('referral')->insert(
						array(
							'sndr_reg_id' => $datas['senderinfo']['sndrregid'] ,
							'sndr_email' => $datas['senderinfo']['sndremail'] ,
							'rcvr_firstname' => $rdata['fname'] ,				
							'rcvr_lastname' => $rdata['lname'] ,
							'rcvr_email' => $rdata['email'] ,
							'date_sent' => $datas['apptime']
							)
					);

				}		
			}

		    
		});
		DB::commit();

		return Response::json(array(
			'status' => 'success',
			'msg' => 'Successfully sent referral'
    	));	

	}



	/*
		Send Sales Rep Key Code email notification
	*/
	protected function SendSalesRepKeyCodeEmail($salesRepId, $keyCode) {

		//get Sales Rep Email and Name
		$userRegData = $this->getSalesRepDetails( $salesRepId );
		$userName = $userRegData[0]->first_name;
		$userEmail = $userRegData[0]->email;

		try {

			$emailData = array(
				'subject' =>'"Visions of Vegas" Ticket from CooperVision',
				'emailFrom' => 'noreply@coopervision.com',
				'SenderName' => 'CooperVision',
			    'emailTo'=> $userEmail,
			    'RcvrName'=> $userName
			);
			 
			// the data that will be passed into the mail view blade template
			$data = array(
				'name' => $userName,
				'keycode' => $keyCode
			);
			 
			Mail::send('pages.salesrepemail', $data, function($message) use ($emailData)
			{
			  $message->from( $emailData['emailFrom'] , $emailData['SenderName'] );
			  $message->to( $emailData['emailTo'] , $emailData['RcvrName']  )->subject( $emailData['subject'] );
			});

			return "SENT";

		} catch(Exception $e) {

			//Log what happen here
			$this->app_log_error('SendSalesRepKeyCodeEmail', $e->getMessage());

			return $e->getMessage();
		}
	}






	/*
		Validates if referal recepient is not yet existing on our DB
	*/
	private function ValidateReferalRecipients( $rcvrdata ) {
		
		$validRcvrEntries = [];

		foreach ($rcvrdata as  $rdata) {
			if ( !$this->isAlreadyExistingReferalEmail( $rdata['email'] )  ) {
				$validRcvrEntries[] = $rdata;//push all details
			}					
		}

		return $validRcvrEntries;

	}



	protected function SendReferalEmail($userRegId,$friendName,$friendEmail) {

		//get User Reg. Email and Name
		$userRegData = $this->getUserRegDetails( $userRegId );
		$sndrName = $userRegData[0]->firstname;
		$sndrEmail = $userRegData[0]->email;

		try {

			$emailData = array(
				'subject' => $sndrName . ' has given you a chance to win!',
				'emailFrom' => 'noreply@coopervision.com',
				'SenderName' => 'CooperVision',
			    'emailTo'=> $friendEmail,
			    'RcvrName'=> $friendName
			);
			 
			// the data that will be passed into the mail view blade template
			$data = array(
				'sndername' => $sndrName,
			    'rcvrname'=> $friendName			    
			);
			 
			Mail::send('pages.referalemail', $data, function($message) use ($emailData)
			{
			  $message->from( $emailData['emailFrom'] , $emailData['SenderName'] );
			  $message->to( $emailData['emailTo'] , $emailData['RcvrName']  )->subject( $emailData['subject'] );
			});

			return "SENT";

		} catch(Exception $e) {

			//Log what happen here
			$this->app_log_error('SendReferalEmail', $e->getMessage());

			return $e->getMessage();
		}
	}










	/*
		Referal recepient checker
	*/
	private function isAlreadyExistingReferalEmail( $email ) {

		$isThere = DB::table('referral')
                    ->where('rcvr_email', '=', $email)
                    ->count();

		return ( $isThere == '0' ) ? false : true;  

	}







	/*
		Validates key code
	*/
	public function getValidateKeyCode() {

		$data = Input::all(true); //Enable XSS filtering
		$keycode = $data['keycode'];		
		$regId = $data['regid'];		

		//Check if the user really own this code
		if ( $this->valideKeyCodeOwner( $regId , $keycode ) ) {

			if ($this->isAlreadyUsedGameKeyCode($keycode) ) {
	   			return Response::json(array(
					'status' => 'failed',
	    			'msg' => 'Ticket code has been used'
		    	));														
			} else {
	   			return Response::json(array(
					'status' => 'success',
	    			'msg' => 'valid key code'
		    	));										
			}


		} else {

   			return Response::json(array(
				'status' => 'failed',
    			'msg' => 'Invalid ticket code'
	    	));						

		}

	}


	/*
		Validate KeyCode Owner
	*/
	public function valideKeyCodeOwner( $regid, $keycode ) {

		//Check if RegId is a Sales Rep
		if ( $this->isUserRegisteredSalesRep($regid) ) {

			//User Registered Email
			$regEmail = DB::table('registrants')->where('id', $regid )->pluck('email');	
			$repId = DB::table('sales_rep')->where('email', $regEmail )->pluck('rep_no');			

			//Validate via RepId - KeyCode
			$isThere = DB::table('keys')
	                    ->where('code', '=', $keycode)
	                    ->where('sales_rep_id', '=', $repId)
	                    ->count();

			return ( $isThere == '0' ) ? false : true;	                    

		} else {

			//Validate via RegId - KeyCode
			$isThere = DB::table('keys')
	                    ->where('code', '=', $keycode)
	                    ->where('user_reg_id', '=', $regid)
	                    ->count();

			return ( $isThere == '0' ) ? false : true;

		}


	}

	/*
		Validate keyCode if it is already used
	*/
	public function isAlreadyUsedGameKeyCode( $keycode ) {
		$isThere = DB::table('gameplay')
                    ->where('keycode', '=', $keycode)
                    ->count();

		return ( $isThere == '0' ) ? false : true;     		
	}


	/*
		Check if registered user is a Sales Rep via Reg Id
	*/
	public function isUserRegisteredSalesRep($regid) {

		//User Registered Email
		$regEmail = DB::table('registrants')->where('id', $regid )->pluck('email');

		//Check if email is registered as a Sales Rep
		$isThere = DB::table('sales_rep')
                    ->where('email', '=', $regEmail)
                    ->count();

		return ( $isThere == '0' ) ? false : true;       


	}


	/*
		Saves Training Status
	*/
	public function getSaveTraining()	 {

		$data = Input::all(true); //Enable XSS filtering

		$regId = $data['regid'];
		$Module = $data['module'];		


		//Check if user is Sales Rep
		if ( $this->isUserRegisteredSalesRep($regId) ) {

	   			return Response::json(array(
					'status' => 'success',
	    			'msg' => 'Sorry, Sales Rep can not earn any ticket for completing training',
	    			'keycount' => $this->getUserKeyCount($regId)
	    		));				

		}

		if ( $this->isAlreadyCompletedTraining($regId,$Module) ) {

			//already taken the training
   			return Response::json(array(
					'status' => 'success',
	    			'msg' => 'User had previously completed this module',
	    			'keycount' => $this->getUserKeyCount($regId)
	    		));				

		} else {

			//Save Training details

			//Get Server Timestamp base on application timezone
			$appTimeStamp = $this->get_app_timestamp();	

			try {	

				DB::table('trainings')->insert(
					array(
						'user_reg_id' => $regId ,
						'module' => $Module ,				
						'status' => 'completed' ,
						'date_acquired' => $appTimeStamp
						)
				);

				//Generate key code , save to keys table and send email to the user
				$userData = $this->getUserRegDetails($regId);
				$email = $userData[0]->email;
				$this->getKeyCode($regId , 'training');	

   				return Response::json(array(
	    			'status' => 'success',
	    			'msg' => 'Successfully saved user training',
	    			'keycount' => $this->getUserKeyCount($regId)
	    		));					


			} catch(Exception $e) {			

				//Log what happen here
				$this->app_log_error('getSaveTraining', $e->getMessage());

	   			return Response::json(array(
	    			'status' => 'failed',
	    			'msg' => $e->getMessage()
	    		));												   		
			}




		}


	}



	/*
		Check if user has previously took the program(module)
	*/
	private function isAlreadyCompletedTraining( $regId , $module ) {

		$isThere = DB::table('trainings')
                    ->where('user_reg_id', '=', $regId)
                    ->count();
                    //->where('module', '=', $module)
                    //->count();

		return ( $isThere == '0' ) ? false : true;  

	}


	/*
		Validates login via email
	*/
	public function getValidate()	 {

		$data = Input::all(true); //Enable XSS filtering

		$firstname = (Input::has('fname')) ? trim($data['fname']) : null;
		$email =  (Input::has('email')) ? trim($data['email']) : null;
		$lastname = (Input::has('lname')) ? trim($data['lname']) : null;

		//check if user typein fname and lname
		if ( is_null($firstname) || ($firstname=='First Name')) {
			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'Please type in your first name to continue'
	    	));							
		} elseif ( is_null($lastname) || ($lastname=='Last Name')   ) {

			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'Please type in your last name to continue'
	    	));				

		}

		//Validate First Name + Last Name + Email				
		if ( $this->isValidLoginDetails( $firstname, $lastname, $email ) ) {

			//Get Module taking or completed
			$userRegDetails = $this->getUserRegDetailViaEmail( $email );
			$jobRole = $userRegDetails[0]->jobrole;
			//Check what module is selected based on job role
			if ($jobRole == "2"  || $jobRole == "3" ) {
				$module = "2";
			} else {
				$module = "1";
			}

			//Get Total key acquired 
			$totalKeyCount = $this->getUserKeyCount( $userRegDetails[0]->id );

			//Save Session Here
			Session::put('registereduser' , true);
			Session::put('userid', $userRegDetails[0]->id );

			//check if user has competed the training
			$trainingComplete = ( $this->hasAlreadyCompletedTraining($userRegDetails[0]->id) ) ? 'y':'n';

			if ( $this->isUserRegisteredSalesRep($userRegDetails[0]->id) ) $trainingComplete = 'y';
			

   			return Response::json(array(
    			'status' => 'success',
    			'msg' => 'Welcome back',
    			'regid' => $userRegDetails[0]->id,
    			'keycount' => $totalKeyCount, 
    			'module' => $module,
    			'trainingcomplete' => $trainingComplete,
    			'name' => $userRegDetails[0]->firstname
    		));

		} else {

			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'Invalid credentials; please register to continue.'
	    	));	

		}

	}


	/*
		Validates login credentials		
	*/
	public function isValidLoginDetails( $fname, $lname , $email ) {

		$isThere = DB::table('registrants')
                    ->where('firstname', 'like', $fname)
                    ->where('lastname', 'like', $lname)                    
					->where('email', '=', $email)                    
                    ->count();

		return ( $isThere == '0' ) ? false : true;  

	}



	/*
		Check if user has completed training selected
	*/
	public function hasAlreadyCompletedTraining( $regId ) {

		$isThere = DB::table('trainings')
                    ->where('user_reg_id', '=', $regId)
					->where('status', '=', 'completed')                    
                    ->count();

		return ( $isThere == '0' ) ? false : true;  

	}



	/*
		Get User No. of Keys acquired
	*/
	public function getUserKeyCount( $regId ) {

		//check if user is sales rep
		if ( $this->isUserRegisteredSalesRep($regId) ) {
			
			//get user registered email
			$srEmail = DB::table('registrants')->where('id', $regId )->pluck('email');
			//Get Sales Rep Id No via email
			$salesRepId = DB::table('sales_rep')->where('email', $srEmail )->pluck('rep_no');
			//Get current eligible keys
			$curCount = DB::table('sales_rep')->where('rep_no', $salesRepId )->pluck('no_credits');
			$curKeys = DB::table('sales_rep')->where('rep_no', $salesRepId )->pluck('no_keys');
			//Check no. of key versus total no. of credits
			//Check if eligible for the next key code
			$keyCount = floor($curCount / 10) - $curKeys;			

			return $keyCount;


		} else {

			//Get Total Earned Keys
			$EarnedKeys = DB::table('keys')
	                    ->where('user_reg_id', '=', $regId)
	                    ->count();

	         //Used in Gameplay
			$gamePlayKeys = DB::table('gameplay')
	                    ->where('user_reg_id', '=', $regId)
	                    ->count();

			$count = $EarnedKeys - $gamePlayKeys;
	        //$sql = "select count(*) from `keys` where user_reg_id = " . $regId
			//	 . " and code not in ( select keycode from gameplay where user_reg_id=". $regId ." )";
			//$count = DB::select(DB::raw($sql))
					
			return $count;

		}
	}



	/*
		Function to check if email registered is a Sales Rep
	*/
	public function isEmailFromSalesRep($email) {

		$isThere = DB::table('sales_rep')
                    ->where('email', '=', $email)
                    ->count();

		return ( $isThere == '0' ) ? false : true;  		

	}



	/*
		Register an Entry
	*/
	public function getRegister() {

		$data = Input::all(true); //Enable XSS filtering

		$firstName = $data['fname'];
		$lastName = $data['lname'];
		$email = $data['email'];
		$jobRole = $data['role'];
		$practiceOwner = ($data['owner']=='0') ? 'n' : 'y';
		$salesRepAssited = ($data['srassits']=='0') ? 'n' : 'y';		
		$practiceName = $data['pname'];
		$practiceActNo = $data['pactno'];
		$state = $data['state'];
		$postalCode = $data['postal'];																
		$CVRepNo = ($data['srassits']=='0') ? '' : $data['repno'];

		//Get Server Timestamp base on application timezone
		$appTimeStamp = $this->get_app_timestamp();



		//Check if Sales rep is trying to refer themselves
		if ( $this->isEmailFromSalesRep($email)  && ($this->isValidSalesRepNo($CVRepNo)) ) {

   			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'Sorry, you are trying to refer yourself as Coopervision Sales Rep.'
    		));					

		}


		//Check if user trying to register is a Sales Rep
		if ( $this->isEmailFromSalesRep($email) ) {

   			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'Sales Reps are now able to enter the consumer promotion however for your chance to Win, please login using your Cooper Vision email address to access your tickets collected through your customers.'
    		));					

			
		}





		//Check if user has previously regitered
		if ( $this->isAlreadyRegistered($email) ) {

   			return Response::json(array(
    			'status' => 'failed',
    			'msg' => 'We already have a registration with this email address. Either proceed to login page or enter another email address.'
    		));					

		}


		//Server-side validations
		$validation = $this->formValidation( $data );
	
		if ( $validation['status'] == 'failed' ) {
			return Response::json($validation);				
		} else {

			try {	

					$newRegId = DB::table('registrants')->insertGetId(
						array(
							'firstname' => $firstName ,
							'lastname' => $lastName ,
							'email' => $email ,
							'jobrole' => $jobRole ,
							'practiceowner' => $practiceOwner ,
							'srassisted' => $salesRepAssited ,
							'practicename' => $practiceName ,
							'practiceactno' => $practiceActNo ,
							'state' => $state ,
							'postcode' => $postalCode ,
							'cvrepno' => $CVRepNo ,
							'date_registered' => $appTimeStamp
							)
					);

					//Check what module is selected based on job role
					if ($jobRole == "2"  || $jobRole == "3" ) {
						$module = "2";
					} else {
						$module = "1";
					}

					//Generate key code , save to keys table and send email to the user
					//Reward a registration key code credit
					//Don't reward sales rep from registering
					if ( !$this->isEmailFromSalesRep($email) ) $this->getKeyCode($newRegId , 'registration');	
					


					//Check if this registrant was previously refered to by someone
					//If Someone refered to this registrant and nobody from snder  to this registrant email
					//is rewarded with the credits
					//Give the keycode credits to the referee (sndr), check on referal table
					/*
						This will always works, because rcvr is a unique email
						if two sender sent a referall to the same rcvr email, only the first sender
						detail will be saved and the second referer details will not be registered
						because of duplicate rcvr_email checks.

						//Don't reward Sales Rep for referring
					*/
					if ( $this->isReferredBySomeone($email) && !$this->isEmailFromSalesRep($email) ) {
						//Generate Key code and send email credit to snder
						//Get referee(sndr email) via rcvr_email
						$sndrRegId = $this->getRefereeEmailAndRegId($email);
						$this->getKeyCode($sndrRegId , 'referal');	

						//Update referal table field "sndr_reward=y" afterwards
						DB::table('referral')->where('rcvr_email', $email )->update(array('sndr_rewarded' => 'y'));
					} 

					//Auto logged user					
					Session::put('registereduser' , true);
					Session::put('userid', $newRegId );


					//Give 1 credit to the sales rep
					//Don't reward Sales Rep from registering himself
					if ( (strlen( $CVRepNo )	> 2) && !$this->isEmailFromSalesRep($email) ) $this->giveCVRepCredit($CVRepNo);
					

					/*	
					return Response::json(array(
			    		'insertid' => $newRegId,
			    		'module' => $module,
			    		'name' => $firstName,
		    			'status' => 'success',
		    			'msg' => 'Successfully Registered a user'
			    	));	
					*/




					$userRegId =$newRegId;
					$userData = $this->getUserRegDetails( $userRegId ); 
					$userFname = $userData[0]->firstname;
					$jobRole = $userData[0]->jobrole;
					//Check what module is selected based on job role
					if ($jobRole == "2"  || $jobRole == "3" ) {
						$module = "2";
					} else {
						$module = "1";
					}					

					//Get Total key acquired 
					$totalKeyCount = $this->getUserKeyCount( $userData[0]->id );

					//check if user has competed the training
					$trainingComplete = (  $this->hasAlreadyCompletedTraining($userData[0]->id) ) ? 'y':'n';

					/*
					if ( $this->isUserRegisteredSalesRep( $userRegId ) ) {
						$autoLogin = Config::get('facebook.BASE_URL')."index.php/dashboard/complete/".$module."/".$userFname."/".$totalKeyCount."?t=".$trainingComplete."&reg=1&rid=".$userData[0]->id;						
					} else {
						$autoLogin = Config::get('facebook.BASE_URL')."index.php/dashboard/complete/".$module."/".$userFname."/".$totalKeyCount."?t=".$trainingComplete."&reg=1";
					}
					*/
				

					return Response::json(array(
						'issr' => ( $this->isUserRegisteredSalesRep( $userRegId ) ) ? 'y' : 'n',
			    		'regid' => $newRegId,
			    		'module' => $module,
			    		'name' => $firstName,
			    		'keycount' => $totalKeyCount,
			    		'training' => $trainingComplete,
		    			'status' => 'success',
		    			'msg' => 'Successfully Registered a user'
			    	));	






			} catch(Exception $e) {			

				//Log what happen here
				$this->app_log_error('getRegister', $e->getMessage());

	   			return Response::json(array(
	    			'status' => 'failed',
	    			'msg' => $e->getMessage()
	    		));												   		
			}


		}
		
	}

	/*
		Cron Process that checks for sales rep eligibility
	*/
	public function cvchecker(){
		$salesRepList = DB::table('sales_rep')->get();

		$today = date("Y-m-d H:i:s");

		foreach ($salesRepList as $sr) {
			$rep_id =  $sr->rep_no;
			//check for credit eligibiltiy
			if ( $this->checkSalesRepKeyCodeEligility($rep_id) ) {
				//Generate key code and send a notification email to rep. email
				$this->awardKeyCodeToSalesRep( $rep_id );
				echo "\nRep Id : " . $rep_id . " Has been rewarded @" . $today;
				$this->app_log_error('cvchecker', "Rep Id : " . $rep_id . " Has be rewarded");

			} 		
		}
		//logging Cron execution
		$this->app_log_error('cvchecker', 'Run by cron job');

	}


	/*
		Award keycode to the sales rep
	*/
	public function awardKeyCodeToSalesRep( $rep_id ) {


		$keyCode = Str::random(8);
		//Try one more time if key code already exists
		if ( $this->isKeyExists($keyCode) ) $keyCode = Str::random(8);


		//Save to Keys Table

		//Get Server Timestamp base on application timezone
		$appTimeStamp = $this->get_app_timestamp();	

		try {	

			DB::table('keys')->insert(
				array(
					'code' => $keyCode ,
					'source' => 'credits' ,				
					'sales_rep_id' => $rep_id,
					'date_acquired' => $appTimeStamp
					)
			);

			//Send Email here
			$emailStatus = $this->SendSalesRepKeyCodeEmail($rep_id, $keyCode);

			//Log the issue
			if ( $emailStatus != 'SENT') $this->app_log_error($emailStatus);


		} catch(Exception $e) {			

			//Log what happen here
			$this->app_log_error('awardKeyCodeToSalesRep', $e->getMessage());
										   		
		}



	}



	/*
		Check if Sales Rep is eligible for a key
	*/
	private function checkSalesRepKeyCodeEligility( $salesRepId ) {

		$curCount = DB::table('sales_rep')->where('rep_no', $salesRepId )->pluck('no_credits');
		$curKeys = DB::table('sales_rep')->where('rep_no', $salesRepId )->pluck('no_keys');
		$acquiredKeys = $this->getSalesRepNoOfAquiredKeys($salesRepId);

		//Check no. of key versus total no. of credits
		//Check if eligible for the next key code
		$keyCount = floor($curCount / 10) - $acquiredKeys;

		if ( $keyCount > $curKeys ) {
			return true;
		} else {
			return false;
		}

	}



	/*
		Get No. of acquired keys by Sales Rep
	*/
	public function getSalesRepNoOfAquiredKeys($salesRepId) {
		return  DB::table('keys')->where('sales_rep_id', $salesRepId )->count();
	}



	/*
		Give 1 credit to sales rep
	*/
	private function giveCVRepCredit($saleRepId) {
		
		$curCount = DB::table('sales_rep')->where('rep_no', $saleRepId )->pluck('no_credits');
		$curKeys = DB::table('sales_rep')->where('rep_no', $saleRepId )->pluck('no_keys');

		//Get Server Timestamp base on application timezone
		$appTimeStamp = $this->get_app_timestamp();	

		try {	

			DB::table('sales_rep')->where('rep_no', $saleRepId  )->update(
				array(
						'no_credits' => $curCount+1,
						'last_updated' => $appTimeStamp
					)
				);	

		} catch(Exception $e) {			

			//Log what happen here
			$this->app_log_error('giveCVRepCredit', $e->getMessage());
		}

		//Check if Sales Rep is eligible to get a key code
		//check for credit eligibiltiy
		if ( $this->checkSalesRepKeyCodeEligility($saleRepId) ) {
			//Generate key code and send a notification email to rep. email
			$this->awardKeyCodeToSalesRep( $saleRepId );
			$this->app_log_error('giveCVRepCredit', "Rep Id : " . $saleRepId . " Has be rewarded");
		} 			

	}



	/*
		Get sender regId from Referal table via unique rcvr_email
	*/
	private function getRefereeEmailAndRegId( $rcvrEmail ) {
		return DB::table('referral')->where('rcvr_email', $rcvrEmail )->pluck('sndr_reg_id');		
	}






	/*
		Check if new registrant was referred by someone
		Give credit to the referee if eligible
		Must be unique Email and sndr_rewarded feild = 'n' ( Means referee is never granted a reward)
	*/
	private function isReferredBySomeone( $registrantEmail ) {
		$isThere = DB::table('referral')
                    ->where('rcvr_email', '=', $registrantEmail)
                    ->where('sndr_rewarded', '=', 'n')
                    ->count();
		return ( $isThere == '0' ) ? false : true;  		

	}




	/*
		Check if user has previously registered
	*/
	private function isAlreadyRegistered( $email ) {

		$isThere = DB::table('registrants')
                    ->where('email', '=', $email)
                    ->count();

		return ( $isThere == '0' ) ? false : true;  

	}



	/*
		Double confirm if keyCode is not existing
	*/
	private function isKeyExists( $keyCode ) {
		
		$isThere = DB::table('keys')
                    ->where('code', '=', $keyCode)
                    ->count();

		return ( $isThere == '0' ) ? false : true;     

	}


	/*
		Generate Key Code and save details to Key table
	*/
	private function getKeyCode($userRegId, $source ) {

		$keyCode = Str::random(8);
		//Try one more time if key code already exists
		if ( $this->isKeyExists($keyCode) ) $keyCode = Str::random(8);


		//Save to Keys Table

		//Get Server Timestamp base on application timezone
		$appTimeStamp = $this->get_app_timestamp();	

		try {	

			DB::table('keys')->insert(
				array(
					'code' => $keyCode ,
					'source' => $source ,				
					'user_reg_id' => $userRegId ,
					'date_acquired' => $appTimeStamp
					)
			);

			//Send Email here
			$emailStatus = $this->send_email_notification($userRegId,$keyCode);

			//Log the issue
			if ( $emailStatus != 'SENT') $this->app_log_error($emailStatus);


		} catch(Exception $e) {			

			//Log what happen here
			$this->app_log_error('getKeyCode', $e->getMessage());

   			return Response::json(array(
    			'status' => 'failed',
    			'msg' => $e->getMessage()
    		));												   		
		}


	}

	/*
		Get User Registration details based on Email
	*/
	private function getUserRegDetailViaEmail( $email ) {
		return DB::table('registrants')->where('email', $email )->get();
	}


	/*
		Get User Registration details based on RegId
	*/
	public function getUserRegDetails( $regId ) {
		return DB::table('registrants')->where('id', $regId )->get();
	}


	/*
		Get Sales Rep Details based on RepId
	*/
	public function getSalesRepDetails( $repId ) {
		return DB::table('sales_rep')->where('rep_no', $repId )->get();
	}






	/*
		Send Notification Email 
	*/
	protected function send_email_notification($userRegId,$keyCode) {

		//get User Reg. Email and Name
		$userRegData = $this->getUserRegDetails( $userRegId );
		$userName = $userRegData[0]->firstname;
		$userEmail = $userRegData[0]->email;

		try {

			$emailData = array(
				'subject' => '"Visions of Vegas" Ticket from CooperVision',
				'emailFrom' => 'noreply@coopervision.com',
				'SenderName' => 'CooperVision',
			    'emailTo'=> $userEmail,
			    'RcvrName'=> $userName
			);
			 
			// the data that will be passed into the mail view blade template
			$data = array(
				'name' => $userName,
			    'keycode'=> $keyCode			    
			);
			 
			Mail::send('pages.useremail', $data, function($message) use ($emailData)
			{
			  $message->from( $emailData['emailFrom'] , $emailData['SenderName'] );
			  $message->to( $emailData['emailTo'] , $emailData['RcvrName']  )->subject( $emailData['subject'] );
			});

			return "SENT";

		} catch(Exception $e) {

			//Log what happen here
			$this->app_log_error('send_email_notification', $e->getMessage());

			return $e->getMessage();
		}
	}



	/*
		Server Side Validatation
	*/
	private function formValidation( $data ) {

		$firstName = $data['fname'];
		$lastName = $data['lname'];
		$email = $data['email'];
		$jobRole = $data['role'];
		$practiceOwner = $data['owner'];
		$salesRepAssited = ($data['srassits']=='0') ? 'n' : 'y';			
		$practiceName = $data['pname'];
		$practiceActNo = $data['pactno'];
		$state = $data['state'];
		$postalCode = $data['postal'];																
		$CVRepNo = ($data['srassits']=='0') ? '' : $data['repno'];	

		if ( (strlen($firstName) < 2)  || ($firstName=='First Name') ) {

 			return array('status' => 'failed',
    					'msg' => 'Please type in your first name to continue.'
    					);				

		} elseif ( (strlen($lastName) < 2) || ($lastName=='Last Name') ){    		

 			return array('status' => 'failed',
    					'msg' => 'Please type in your last name to continue.'
    					);

		} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

 			return array('status' => 'failed',
    					'msg' => 'Please type in valid email address to continue.'
    					);			

    	} elseif ($jobRole == '00') {

 			return array('status' => 'failed',
    					'msg' => 'Please select desired job role to continue.'
    					);				
	
		} elseif ((strlen($practiceName) < 2)  || ($practiceName == "Practice Name") ) {    

 			return array('status' => 'failed',
    					'msg' => 'Please type in practice name to continue.'
    					);				

		} elseif ( (strlen($practiceActNo) < 2) || ($practiceActNo=="Practice Account Number")) {    

 			return array('status' => 'failed',
    					'msg' => 'Please type in practice account number to continue.'
    					);				

		} elseif ($state == '00') {

 			return array('status' => 'failed',
    					'msg' => 'Please select a state to continue.'
    					);	

		} elseif (!is_numeric($postalCode)) {    

 			return array('status' => 'failed',
    					'msg' => 'Please type in valid postcode to continue.'
    					);	

 		//Check if SR assisted and SRID is valid	
		} elseif (!$this->isValidSalesRepNo($CVRepNo) && $salesRepAssited=='y') {
			
 			return array('status' => 'failed',
    					'msg' => 'Please type in valid CooperVision Rep Number to continue.'
    					);	

		} else {

			return array('status' => 'ok' , 'msg' => 'Input all valid' );
		}


	}






	/*
		Check if Sales Rep Account No is valid
	*/
	private function isValidSalesRepNo( $srid ) {

		$isThere = DB::table('sales_rep')
                    ->where('rep_no', '=', $srid)
                    ->count();

		return ( $isThere == '0' ) ? false : true;       

	}



	/*
		Log Error message found	
	*/
	protected function app_log_error($functionName, $message){
		$timeStamp = $this->get_app_timestamp();
		DB::table('error_logs')->insert(
			array(
				'function_name' => $functionName,
				'message' => $message ,
				'date_happen' => $timeStamp
				)
		);
	}


	/*
		Get new Server Date base on application timezone
	*/
	protected function get_app_timestamp() {
		date_default_timezone_set(Config::get('facebook.APP_TIMEZONE'));
		return date('Y-m-d H:i:s');
	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return Response::json(array(
			'name' => 'Xerxis Anthony B. Alvar',
			'email' => 'kaixersoft@gmail.com'
		));
	}


	//--------------- ADMIN FUNCTIONS ------------------------------------

	/*
		Get Campaign Settings
	*/
	public function get_campaign_settings() {
		return DB::table('campaign_settings')->get();
	}



}
