<?php

class AdminController extends \BaseController {


	/**
	 * Check for valid Admin acces
	 *
	*/
	public function is_validadmin( $username , $password ) {

		$settings = App::make('AppController')->get_campaign_settings();
		$validUsername = null;
		$validPassword = null;

		//Get valid Username and Password from settings
		foreach ($settings as $attr) {			
		    if ($attr->property  == 'admin_username')  $validUsername = $attr->value;
		    if ($attr->property  == 'admin_password')  $validPassword = $attr->value;
		}	

		return ( $username == $validUsername  &&  $password == $validPassword ) ? true : false;

	}



	/*
		Get Total Reports
	*/
	public function get_total_reports() {
		$data = [
			'total_registrants' => $this->get_total_registrants(),
			'total_keys' => $this->get_total_keys(),
			'total_key_via_training' => $this->get_total_keys_by_training(),
			'total_key_via_referral' => $this->get_total_keys_by_referal()
		];
		return $data;
	}


	/*
		Get Total No. of Registrants
	*/
	public function get_total_registrants() {
		return DB::table('registrants')->count();
	}

	/*
		Get Total No. of Keys
	*/
	public function get_total_keys() {
		return DB::table('keys')->count();
	}


	/*
		Get Total No. of Keys by Training
	*/
	public function get_total_keys_by_training() {
		return DB::table('keys')
			->where('source','=','training')
			->count();
	}


	/*
		Get Total No. of by referall
	*/
	public function get_total_keys_by_referal() {
		return DB::table('keys')
			->where('source','=','referal')
			->count();
	}



	/* ////////// DOWNLOADS \\\\\\\\\\\\\\\\\\\\\\*/

	/*
		Registrants Report
	*/
	public function registrants_report() {

	    //CSV File Headers
	    $titles = array("FIRST NAME","LAST NAME","EMAIL","JOB ROLE","PRACTICE OWNER","SALES REP ASSISTED","PRACTICE NAME","PRACTICE ACCOUNT","STATE","POSTAL CODE","SALES REP NO","DATE REGISTERED",
		    			"TRAINING MODULE","STATUS","DATE FINISHED"
	    		  );	    		

	    //Database Fields
		$fields = [
			'registrants.firstname','registrants.lastname','registrants.email',
			'registrants.jobrole','registrants.practiceowner','registrants.srassisted','registrants.practicename',			
			'registrants.practiceactno','registrants.state','registrants.postcode','registrants.cvrepno','registrants.date_registered',
			'trainings.module','trainings.status','trainings.date_acquired'
		];

		$data = DB::table('registrants')
					->join('trainings','registrants.id','=','trainings.user_reg_id','left')
					->select($fields)
					->get();
	
		$table = $data;
		$filename = time() . '_tempfile.csv';
	    $file = fopen('downloads/' . $filename , 'w');    
	    $headerTitles = false;
	    foreach ($table as $row) {
	    	if ( !$headerTitles) {
				fputcsv($file, $titles);
				$headerTitles=true;
	    	}
	        fputcsv( $file, get_object_vars($row) );
	    }
	    fclose($file);		

		$contents =  file_get_contents('http:' . Config::get('facebook.BASE_URL').'downloads/' . $filename);

		return $contents;
		
	}

	/*
		Key Codes Report
	*/
	public function keycodes_report() {

	    //CSV File Headers
	    $titles = array("FIRST NAME","LAST NAME","EMAIL","KEY CODE","SOURCE","SALES REP. NO","DATE ACQUIRED");	    		

	    //Database Fields
		$fields = [
			'registrants.firstname','registrants.lastname','registrants.email',
			'keys.code','keys.source','keys.sales_rep_id','keys.date_acquired'
		];


	$data = DB::table('registrants')
					->join('keys', 'registrants.id' ,'=' , 'keys.user_reg_id' , 'right')
					->select($fields)
					->get();					
	
		$table = $data;
		$filename = time() . '_tempfile.csv';
	    $file = fopen('downloads/' . $filename , 'w');    
	    $headerTitles = false;
	    foreach ($table as $row) {
	    	if ( !$headerTitles) {
				fputcsv($file, $titles);
				$headerTitles=true;
	    	}
	        fputcsv( $file, get_object_vars($row) );
	    }
	    fclose($file);		

		$contents =  file_get_contents('http:' . Config::get('facebook.BASE_URL').'downloads/' . $filename);

		return $contents;

	}


	/*
		Sales Rep Report
	*/
	public function sales_rep_report() {

	    //CSV File Headers
	    $titles = array("REP NO","FIRST NAME","LAST NAME","EMAIL","NO. OF CREDITS","NO. OF KEYS","LAST UPDATED");	    		

	    //Database Fields
		$fields = [
			'sales_rep.rep_no','sales_rep.first_name','sales_rep.last_name','sales_rep.email','sales_rep.no_credits',
			'sales_rep.no_keys','sales_rep.last_updated'
		];


	$data = DB::table('sales_rep')
					->select($fields)
					->get();					
	
		$table = $data;
		$filename = time() . '_tempfile.csv';
	    $file = fopen('downloads/' . $filename , 'w');    
	    $headerTitles = false;
	    foreach ($table as $row) {
	    	if ( !$headerTitles) {
				fputcsv($file, $titles);
				$headerTitles=true;
	    	}
	        fputcsv( $file, get_object_vars($row) );
	    }
	    fclose($file);		

		$contents =  file_get_contents('http:' . Config::get('facebook.BASE_URL').'downloads/' . $filename);

		return $contents;


	}



	/*
		Ranking Game Report
	*/
	public function rankinggame_report() {

	    //CSV File Headers
	    $titles = array("FIRST NAME","LAST NAME","EMAIL","SORTINGS","TICKET CODE","DATE PLAYED");	    		

	    //Database Fields
		$fields = [
			'registrants.firstname','registrants.lastname','registrants.email',
			'gameplay.sortings','gameplay.keycode','gameplay.date_played'
		];


	$data = DB::table('registrants')
					->join('gameplay', 'registrants.id' ,'=' , 'gameplay.user_reg_id' , 'right')
					->select($fields)
					->get();					
	
		$table = $data;
		$filename = time() . '_tempfile.csv';
	    $file = fopen('downloads/' . $filename , 'w');    
	    $headerTitles = false;
	    foreach ($table as $row) {
	    	if ( !$headerTitles) {
				fputcsv($file, $titles);
				$headerTitles=true;
	    	}
	        fputcsv( $file, get_object_vars($row) );
	    }
	    fclose($file);		

		$contents =  file_get_contents('http:' . Config::get('facebook.BASE_URL').'downloads/' . $filename);

		return $contents;

	}


	

}