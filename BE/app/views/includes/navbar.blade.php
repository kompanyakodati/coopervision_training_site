<header>
	<div class="colorline"></div>
	<div class="header-content clearfix">
		<h1></h1>
		<h2>
        	MyDay&#153; Training Program
        </h2>
		<span class="login">
			<?php 	if ( Session::get('registereduser') ) { ?>
			<a id="btnLogout" href="{{Config::get('facebook.BASE_URL')}}index.php/logout">Logout</a> 
			<?php } else { ?>
			<a id="btnLogin" href="{{Config::get('facebook.BASE_URL')}}index.php/login">Login</a> 
			<?php } ?>
			<span class="mlrpx">|</span> 
			<a href="{{Config::get('facebook.BASE_URL')}}index.php/register">Register</a>
		</span>
        
        <select id="main_nav">
			<?php 	if ( Session::get('registereduser') ) { 
					$userRegId = Session::get('userid');
					$userData = App::make('AppController')->getUserRegDetails( $userRegId ); 
					$userFname = $userData[0]->firstname;
					$jobRole = $userData[0]->jobrole;
					//Check what module is selected based on job role
					if ($jobRole == "2"  || $jobRole == "3" ) {
						$module = "2";
					} else {
						$module = "1";
					}					

					//Get Total key acquired 
					$totalKeyCount = App::make('AppController')->getUserKeyCount( $userData[0]->id );

					//check if user has competed the training
					$trainingComplete = (  App::make('AppController')->hasAlreadyCompletedTraining($userData[0]->id) ) ? 'y':'n';

					if ( App::make('AppController')->isUserRegisteredSalesRep( $userRegId ) ) {
						$autoLogin = Config::get('facebook.BASE_URL')."index.php/dashboard/complete/".$module."/".$userFname."/".$totalKeyCount."?&t=y&reg=1&rid=".$userData[0]->id;						
					} else {
						$autoLogin = Config::get('facebook.BASE_URL')."index.php/dashboard/complete/".$module."/".$userFname."/".$totalKeyCount."?&t=".$trainingComplete."&reg=1";
					}
					
				?>     
			<option value="{{$autoLogin}}">Home</option>
			<?php } else { ?>
			<option value="{{Config::get('facebook.BASE_URL')}}">Home</option>
			<?php } ?>
			<option link="HOW2P"  value="{{Config::get('facebook.BASE_URL')}}index.php/howtoplay">How to Play</option>                     

			<?php 	if ( Session::get('registereduser') ) { 
					$userRegId = Session::get('userid');
					if ( !App::make('AppController')->isUserRegisteredSalesRep( $userRegId ) ) { ?>				
			<option link="LEARNING"  value="{{Config::get('facebook.BASE_URL')}}index.php/learnmodule?&mod={{$module}}">Learning Module</option>                    
			<?php 	} 
				}
			?>

			<?php if ( Session::get('registereduser') ) { ?>
				<option link="RANK"  value="{{Config::get('facebook.BASE_URL')}}index.php/rank">Play the "Rank to Win" Game</option>
			<?php } ?>

			<?php
				if ( Session::get('registereduser') ) {
					$userRegId = Session::get('userid');
					if ( !App::make('AppController')->isUserRegisteredSalesRep( $userRegId ) ) { ?>
						<option link="REFER"  value="{{Config::get('facebook.BASE_URL')}}index.php/refer">Refer a Colleague</option>
			<?php 	} 
				}
			?>
        </select>
	</div>
</header>    