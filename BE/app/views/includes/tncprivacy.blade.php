<footer>
	<div class="w960ma">
		<p class="fl"><a href="../../index.php/tnc">Terms and Conditions</a> | <a href="http://coopervision.net.au/sites/coopervision.net.au/files/0a9151fa-5292-11e1-a35b-00163e55ae82.pdf" target="_blank">Privacy Policy</a></p>
		<p class="fr">&#169; 2014 CooperVision. Part of the <a href="http://coopervision.net.au/about-us" target="_blank">Cooper Companies</a></p>
	</div>
</footer>