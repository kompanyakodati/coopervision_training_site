



<!-- Load plugin scripts -->
<script type="text/javascript" src="{{Config::get('facebook.BASE_URL')}}assets/js/vendor/jquery.selectbox/jquery.selectbox-0.2.js?v=1"></script>
<script type="text/javascript" src="{{Config::get('facebook.BASE_URL')}}assets/js/vendor/shadowbox-3.0.3/shadowbox.js"></script>       
<!--      <script type="text/javascript" src="js/vendor/jquery.h5validate.js"></script>       -->
<script src="{{Config::get('facebook.BASE_URL')}}assets/js/plugins.js"></script>
<script src="{{Config::get('facebook.BASE_URL')}}assets/js/main.js"></script>

{{-- Facebook SDK --}}
<script>
/*
  window.fbAsyncInit = function() {
    FB.init({
    appId  : "{{Config::get('facebook.AppId')}}",
    status : true,
    cookie : true,
    xfbml  : true,
    oauth  : true 
    });
    FB.Canvas.setAutoGrow();        
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  */
</script>