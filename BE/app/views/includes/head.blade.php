<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=810">
<link rel="stylesheet" href="{{Config::get('facebook.BASE_URL')}}assets/css/normalize.css" />
<link rel="stylesheet" href="{{Config::get('facebook.BASE_URL')}}assets/css/font.css" />
<link rel="stylesheet" href="{{Config::get('facebook.BASE_URL')}}assets/css/main.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link rel="stylesheet" href="{{Config::get('facebook.BASE_URL')}}assets/js/vendor/shadowbox-3.0.3/shadowbox.css" />        
<link href="{{Config::get('facebook.BASE_URL')}}assets/css/jquery.selectbox/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<!--		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->         
<script src="{{Config::get('facebook.BASE_URL')}}assets/js/vendor/modernizr-2.6.2.min.js"></script>
<!-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="{{Config::get('facebook.BASE_URL')}}assets/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
-->

{{-- Google Analytics --}}
<script>
/*
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', "{{Config::get('facebook.GA_TRACKING_ID')}}", 'flok.co');
	ga('send', 'pageview');
*/	
</script>  