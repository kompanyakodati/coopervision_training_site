@extends('layouts.master')

@section('content')

            <section class="refer w960ma">

                <h2>Refer a Colleague</h2>
                <h3>Refer a colleague to the Visions of Vegas program to give you more chances to win!  Each time one of your <br />
                referred colleagues registers, you will be sent another ticket code, provided you are the first person to refer them.  
                <br /><br />There are no limits ... the more colleagues you refer, the greater your chances are of winning!</h3>
                <br />
                <div class="w960ma clearfix">
                    <form>
                        <div class="clearfix">
                            <section id="firstname">
                                <input type="text" value="First Name" class="txtFirstName" />
                            </section>
                            <section id="lastname">
                                <input type="text" value="Last Name"  class="txtLastName" />
                            </section>                            
                            <section id="email">
                                <input type="text" value="Email" id="email1" class="txtEmail" />
                                <label for="email1" class="asterix">*</label>                                
                            </section>  
                        </div>
                        <a href="javascript:addField();" class="addfield">[+] Add more fields</a>
                        <br />
                        <br />
                        <p class="errorfeedback">* Please ensure that these are valid email addresses.</p>                        
                        <br />
                        <br />                        
                        <div class="center"><input type="image" onclick="sendReReferal();return false;" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit.png"></div>                        
                    </form>
                </div>
                
            </section>

@stop

@section('contentjs') 

<script>

<?php if (Session::get('userid') ) { ?>    
    _PageManager.setPage("REFER");
    
    function addField() {
        $("#firstname").append("<input type='text' value='First Name'  class='txtFirstName' />");
        $("#lastname").append("<input type='text' value='Last Name' class='txtLastName' />");
        $("#email").append("<input type='text' value='Email' class='txtEmail' />");      
    }    



    <?php
        if (Session::get('userid') ) {     
            $regId = Session::get('userid');
            $userRegData = App::make('AppController')->getUserRegDetails( $regId );
            $userEmail = $userRegData[0]->email;
        }
    ?>
    function sendReReferal() {
        var _rcvrdata = colectDetails();
        var _sndrdata = {
            'sndrregid' : "{{$regId}}",
            'sndremail' : "{{$userEmail}}"
        };
        var _params = {
            'url' : "{{Config::get('facebook.BASE_URL')}}" + "index.php/api/send-referals?",
            'args' : { 
                'sndr' : _sndrdata,
                'rcvr' : _rcvrdata
            }
        }
        $.getJSON(_params.url, _params.args,function(res){
            if (res.status == 'success') {


                var _url = "{{Config::get('facebook.BASE_URL')}}" + "index.php/response?&pg=1";                
                location.href = _url;

            } else {
                alert( res.msg );
            }          
        });        


    }

    function colectDetails() {
        var _fnames = $('.txtFirstName');
        var _lnames = $('.txtLastName');
        var _emails  = $('.txtEmail');
        var __fnames=[], __lnames=[], __email=[];
        var _data = [];

        //fnames
        $.each(_fnames,function(i,v){ __fnames.push($(v).val()); });

        //_lnames
        $.each(_lnames,function(i,v){ __lnames.push($(v).val()); });

        //emails
        $.each(_emails,function(i,v){ __email.push($(v).val()); });

        //put it together
        for(var i=0; i<__email.length; i++) {
            var _info = { 
                'fname' : __fnames[i],
                'lname' : __lnames[i],
                'email' : __email[i]
            }
            _data.push( _info );
        }

        return _data;

    }


<?php } else { ?>

       location.href = "{{Config::get('facebook.BASE_URL')}}index.php/login";
       
<?php } ?>
</script>


@stop
