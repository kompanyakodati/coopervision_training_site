@extends('layouts.master')

@section('content')
 <script> var selectedOptions = []; </script>

        <section class="rank w960ma">
        
            <h2>Rank to Win Game</h2>

                <article class='intro center'>
                    <h3>Please confirm your entries before submission:</h3>
                </article>
               

        </section>

		
		<div class="answers_container confirmation_answers" id="step2">
			<section class="answers">
				<p> 
					What are the top 10 attributes of MyDay&#153; Contact Lenses for the best patient experience?
					<span>Please check to make sure you are happy with your answers before submission:</span>
				</p>
				<form>
					<ul id="answers">
						<li class="clearfix">
							<label for="option1">1.</label>
							<select id="option1"></select>							
						</li>
						<li class="clearfix">
							<label for="option2">2.</label>
							<select id="option2"></select>							
						</li>
						<li class="clearfix">
							<label for="option3">3.</label>
							<select id="option3"></select>							
						</li>
						<li class="clearfix">
							<label for="option4">4.</label>
							<select id="option4"></select>							
						</li>						
						<li class="clearfix">
							<label for="option5">5.</label>
							<select id="option5"></select>							
						</li>	
						<li class="clearfix">
							<label for="option6">6.</label>
							<select id="option6"></select>							
						</li>
						<li class="clearfix">
							<label for="option7">7.</label>
							<select id="option7"></select>							
						</li>						
						<li class="clearfix">
							<label for="option8">8.</label>
							<select id="option8"></select>							
						</li>	
						<li class="clearfix">
							<label for="option9">9.</label>
							<select id="option9"></select>							
						</li>	
						<li class="clearfix">
							<label for="option10">10.</label>
							<select id="option10"></select>							
						</li>							
					</ul>
					
					<div class="center">
						<input type="image" onclick="confirmEntry();return false;" value="Submit" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit.png" />
						<!-- <input type="image" onclick="gobacktoRanking();return false;" value="Back" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_back_large.png" /> -->
					</div>
					<br />
					<br />
				</form>
			</section>
		</div>


@stop

@section('contentjs') 

<script>

	<?php if (Session::get('userid') ) { ?>	

		_PageManager.setPage("RANK");		
			
		$(function () {


		        var _GameMaster = new GameMaster();     
		        _GameMaster.init();
		                    
		        for (var i = 1; i < 11; i++) { 
		            $("#option"+i).selectbox({
		                onChange: function(e) {
		                    _GameMaster.update($(this), e);
		                }
		            }); 
		        };

		        $("#answers").find(".sbHolder .sbSelector").each(function(i,v) {
		            if ($(this).html() == "SELECT AN OPTION") {
		            	
		            	//Pre Select base on ranking page selection
		            	$(this).html(_sortAr[i]);
		            	$(this).addClass("default");
		            }
		        });		        

	
		});


	<?php
		$sortings = Input::get('sort');
		$keycode = Input::get('kcode');
	?>
	var _sortings = "{{$sortings}}";
	var _sortAr = _sortings.split("|"); //originally from ranking page
	var _values = [];

	function confirmEntry() {

		//Re-Collect updated sortings
        var _data = [];

        //collect data
        $("#answers .sbSelector").each(function() { _values.push($(this).html()); });

        //check if all are set
        $.each(_values,function(i,v){
            if ( v == "SELECT AN OPTION" ) {
                delete _values[i];
            } else {
                _data.push( v );
            }
        });

        _values = [];//reset for the next session

        if (_data.length < 10) {
            alert('Please check your ranking, to continue');
            return;
        } 
        var sortings = _data.join("|");

        //Let's save the damn fuckin thing!!
		var _params = {
            'url' : "{{Config::get('facebook.BASE_URL')}}" + "index.php/api/save-game-play?",
            'args' : { 
            		'regid'	  : "{{Session::get('userid')}}",
            		'kcode'   : "{{$keycode}}",
            		'sorting' : sortings 
            }
        }

        $.getJSON(_params.url, _params.args,function(res){
          
            if (res.status == 'success') {
            	
            	
        		location.href = "{{Config::get('facebook.BASE_URL')}}index.php/response";

            } else {
                alert( res.msg );
            }

        });

		

	}





	<?php } else { ?>

	       location.href = "{{Config::get('facebook.BASE_URL')}}index.php/login";
	       
	<?php } ?>

	</script>

@stop

