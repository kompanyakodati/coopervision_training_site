<form>
<div id="slide0" class="slide-content slide">
    <br />
    <br />
    <br />
    <h2 class="headline">
        Focus on MyDay and you could <span>WIN a trip to <br />Vision Expo West, Las Vegas!</span>
    </h2>	
    <p>In planning for the development of MyDay, the CooperVision team identified that no daily disposable offered excellent long-lasting comfort, high levels of breathability and easy handling.</p>
    <br />
	<input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_begin_quiz.png" onclick="_LearningManager.beginQuiz()" />
</div>

<div id="slide1" class="slide-content slide-background1 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span>CONTACT LENS FACTS</span></h2>
    <br />
    <br />
	<p class="center"><b>Fact:</b> Half of the Australian and New Zealand population needs vision correction.</p>
</div>

<div id="slide2" class="slide-content slide-background1 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="1"><li>Out of those who need vision correction, what percentage wear contact lenses?</li></ol>
    <div class="questions">
        <p><input id="1" type="radio" value="a" name="qq1" class="qq1" /><label for="option1">&nbsp;&nbsp;90%</label></p>
        <p><input id="2" type="radio" value="b" name="qq1" class="qq1" /><label for="option2">&nbsp;&nbsp;8%</label></p>
        <p><input id="3" type="radio" value="c" name="qq1" class="qq1" /><label for="option3">&nbsp;&nbsp;25%</label></p>
        <p><input id="4" type="radio" value="d" name="qq1" class="qq1" /><label for="option4">&nbsp;&nbsp;50%</label></p>
    </div>
    <div id="quiz_response_qq1" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq1');return false;" id="btn_qq1" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 
</div>

<div id="slide3" class="slide-content slide-background1 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="2"><li>What percentage of people are generally suitable for contact lenses?</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq2" class="qq2" /><label for="option1">&nbsp;&nbsp;90%</label></p>
        <p><input id="2" type="radio" value="b" name="qq2" class="qq2" /><label for="option2">&nbsp;&nbsp;8%</label></p>
        <p><input id="3" type="radio" value="c" name="qq2" class="qq2" /><label for="option3">&nbsp;&nbsp;25%</label></p>
        <p><input id="4" type="radio" value="d" name="qq2" class="qq2" /><label for="option4">&nbsp;&nbsp;50%</label></p>
    </div>
    <div id="quiz_response_qq2" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq2');return false;" id="btn_qq2" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <br />		
</div>

<div id="slide4" class="slide-content slide-background2 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span style="font-size:48px;">HOW TO TALK ABOUT CONTACT LENSES</span></h2>
    <br />
    <br />
	<p class="center"><b>Fact:</b> Most people don’t wear contact lenses because they haven’t been offered the option. Let’s find out what the best way to offer contact lenses is.</p>
</div>

<div id="slide5" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="3"><li>When is the best time to introduce contact lenses to optometry patients?</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq3" class="qq3" /><label for="option1">
        &nbsp;&nbsp;When booking an appointment with the optometrist, ask if the appointment is for glasses or glasses <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and contact lenses</label></p>
        
        <p><input id="2" type="radio" value="b" name="qq3" class="qq3" /><label for="option2">
        &nbsp;&nbsp;While they are waiting for their appointment</label></p>
        
        <p><input id="3" type="radio" value="c" name="qq3" class="qq3" /><label for="option3">&nbsp;&nbsp;
        While dispensing their glasses – You can let them know that this power is also available in contact lenses</label></p>
        
        <p><input id="4" type="radio" value="d" name="qq3" class="qq3" /><label for="option4">&nbsp;&nbsp;&nbsp;All of the above</label></p>
    </div>
    <div id="quiz_response_qq3" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq3');return false;" id="btn_qq3" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <br />		
</div>

<div id="slide6" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="4"><li>If all those times are a good opportunity to talk about contact lenses, how do you start the contact lens conversation.</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq4" class="qq4" /><label for="option1">
        &nbsp;&nbsp;By talking about the weather</label></p>
        <p><input id="2" type="radio" value="b" name="qq4" class="qq4" /><label for="option2">
        &nbsp;&nbsp;By talking about lifestyle</label></p>
        <p><input id="3" type="radio" value="c" name="qq4" class="qq4" /><label for="option3">&nbsp;&nbsp;
        By talking about pets</label></p>
    </div>
    <div id="quiz_response_qq4" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq4');return false;" id="btn_qq4" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <br />		
</div>

<div id="slide7" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="5"><li>
    	What lifestyle questions can you ask patients prior to their appointment to identify whether contact lenses may be suitable?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq5" class="qq5" />
        <label for="option1">&nbsp;&nbsp;Are there ever times you wish you didn’t have to wear your glasses?</label></p>
        <p><input id="2" type="radio" value="b" name="qq5" class="qq5" />
        <label for="option2">&nbsp;&nbsp;Do you play any sports?</label></p>
        <p><input id="3" type="radio" value="c" name="qq5" class="qq5" />
        <label for="option3">&nbsp;&nbsp;Do you always wear the same clothes every time you go out?</label></p>
        <p><input id="4" type="radio" value="d" name="qq5" class="qq5" />
        <label for="option4">&nbsp;&nbsp;All of the above</label></p>        
    </div>
    <div id="quiz_response_qq5" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq5');return false;" id="btn_qq5" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <br />		
</div>

<div id="slide8" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="6"><li>
    	What follow-on questions can be asked to find out what kind of contact lenses may be suitable?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq6" class="qq6" />
        <label for="option1">&nbsp;&nbsp;How often do you play sports?</label></p>
        <p><input id="2" type="radio" value="b" name="qq6" class="qq6" />
        <label for="option2">&nbsp;&nbsp;When do you wish you didn’t have to wear your glasses?</label></p>
        <p><input id="3" type="radio" value="c" name="qq6" class="qq6" />
        <label for="option3">&nbsp;&nbsp;All of the above</label></p>        
    </div>

    <div id="quiz_response_qq6" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq6');return false;" id="btn_qq6" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq6" class="remarks">Contact lenses are available as 2-weekly and monthly replacement; these are good for patients who want to wear their contact lenses everyday. For patients that wish to wear their lenses part time or occasionally, 1 day lenses are also available.</p>	    
    <br />		
</div>

<div id="slide9" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="7"><li>
    	What are the primary motivators to wear contact lenses?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq7" class="qq7" />
        <label for="option1">&nbsp;&nbsp;Self confidence</label></p>
        <p><input id="2" type="radio" value="b" name="qq7" class="qq7" />
        <label for="option2">&nbsp;&nbsp;Lifestyle</label></p>
        <p><input id="3" type="radio" value="c" name="qq7" class="qq7" />
        <label for="option3">&nbsp;&nbsp;Special occasions</label></p>             
		<!--
        <p><input id="4" type="radio" value="d" name="qq7" class="qq7" />
        <label for="option3">&nbsp;&nbsp;List all of them</label></p>                
        -->
        <p><input id="4" type="radio" value="d" name="qq7" class="qq7" />
        <label for="option3">&nbsp;&nbsp;All of the above</label></p>                        
    </div>

    <div id="quiz_response_qq7" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq7');return false;" id="btn_qq7" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq7" class="remarks">There are lots of reasons why people choose to wear contact lenses. Don’t hesitate to ask about their motivation.</p>	    
    <br />		
</div>

<div id="slide10" class="slide-content slide-background4 slide">

    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span>MYTH QUESTIONS</span></h2>
    <br />
    <br />
	<p class="center">Can you pick which of these are facts and which are myths?</p>
</div>

<div id="slide11" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="8"><li>Contact lenses can get lost in the back of my eye.</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq8" class="qq8">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq8" class="qq8">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq8" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq8');return false;" id="btn_qq8" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq8" class="remarks">No, it is not physically possible. There is a membrane that prevents anything going into the back of your eyes. </p>	          
    <br />	
</div>

<div id="slide12" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="9"><li>Make-up and contact lenses: I must give up one for the other. </li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq9" class="qq9">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq9" class="qq9">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq9" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq9');return false;" id="btn_qq9" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 



    <p id="remqq9" class="remarks">
	No, you can wear both. You just need to respect certain rules; contact lenses first, make up second.
    </p>	          
    <br />	
</div>

<div id="slide13" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="10"><li>Contact lenses are uncomfortable.</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq10" class="qq10">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq10" class="qq10">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq10" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq10');return false;" id="btn_qq10" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq10" class="remarks">
		Contact lenses are very comfortable; try one for yourself.
    </p>	          
    <br />		
</div>

<div id="slide14" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="11"><li>Contact lenses can get stuck to my eye.</li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq11" class="qq11">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq11" class="qq11">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq11" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq11');return false;" id="btn_qq11" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p id="remqq11" class="remarks">
		Optometrists will be able to teach you how to remove contact lenses safely in any situation.
    </p>	          
    <br />		
</div>

<div id="slide15" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="12"><li>Contact lenses are hard work to take care of.</li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq12" class="qq12">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq12" class="qq12">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq12" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq12');return false;" id="btn_qq12" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p id="remqq12" class="remarks">
		There are now multi-purpose solutions that are very easy to use, and there is also the option of 1 day lenses that you throw away at the end of each day.
    </p>	          
    <br />		
</div>

<div id="slide16" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="13"><li>Contacts can pop out of my eye.</li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq13" class="qq13">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq13" class="qq13">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq13" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq13');return false;" id="btn_qq13" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p id="remqq13" class="remarks">
		Contact lenses don't come out unless you decide to take them out.
    </p>	          
    <br />	
</div>

<div id="slide17" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="14"><li>Contact lenses are only suitable for young people.</li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq14" class="qq14">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq14" class="qq14">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq14" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq14');return false;" id="btn_qq14" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p  id="remqq14" class="remarks">
		They are actually suitable for everyone, even presbyopes – those who are over 40 can now wear multifocal contact lenses.
        Multifocal contact lenses are available in monthly or 1 day.
    </p>	          
    <br />	        
</div>

<div id="slide18" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="15"><li>Contact lenses are too expensive.</li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq15" class="qq15">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq15" class="qq15">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq15" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq15');return false;" id="btn_qq15" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p id="remqq15" class="remarks">
Contact lenses can cost as little as $1 per day.
    </p>	          
    <br />	               
</div>

<div id="slide19" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />    
    <br />        
    <ol start="16"><li>
 My friends, aunties, cousins and friends tried them, and they didn’t work.  
    </li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq16" class="qq16">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq16" class="qq16">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq16" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq16');return false;" id="btn_qq16" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq16" class="remarks">
Every patient is differrent, and now there are all sorts of contact lenses for different patients' needs.
    </p>	          
    <br />	     
    
</div>

<div id="slide20" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="17"><li>
I’ve heard contact lenses are made of glass that you put on your eye.
    </li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq17" class="qq17">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq17" class="qq17">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq17" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq17');return false;" id="btn_qq17" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p id="remqq17" class="remarks">
We recommend that you open a 1 day contact lens blister and make your patient touch the lens; so they realise how soft today’s contact lenses are.
    </p>	          
    <br />	    
         
</div>

<div id="slide21" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="18"><li>
My mother tried them twenty years ago, and they didn’t work.
    </li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq18" class="qq18">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq18" class="qq18">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq18" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq18');return false;" id="btn_qq18" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq18" class="remarks">
There is now new technology in contact lenses; we recommend that your Mum comes in and tries them again.
    </p>	          
    <br />	    
    
</div>

<div id="slide22" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="19"><li>
I’ve heard contact lenses are not healthy for my eye.
    </li></ol> 

    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq19" class="qq19">
        <label for="option1">&nbsp;&nbsp;True</label></p>
        <p><input id="2" type="radio" value="b" name="qq19" class="qq19">
        <label for="option2">&nbsp;&nbsp;False</label></p>          
    </div>

    <div id="quiz_response_qq19" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq19');return false;" id="btn_qq19" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <p id="remqq19" class="remarks">
Contact lenses have new material, made of silicone hydrogel, that is much more breathable and allow almost 100% oxygen to reach the eyes.
    </p>	          
    <br />	   
            
</div>

<div id="slide23" class="slide-content slide-background1 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline">Did you know contact lenses can cost as little as $1 per day – <br />That’s less than your daily cup of coffee! </h2>
    <br />
    <br />
</div>

<div id="slide24" class="slide-content slide-background1 question">

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="20"><li>
If a patient asks about the cost, how do you respond?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq20" class="qq20" />
        <label for="option1">&nbsp;&nbsp;Well it’s quite complicated, it depends what that modality is, there are lots of options. You would need to <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;speak to the optometrists, I can’t tell you.</label></p>
        <p><input id="2" type="radio" value="b" name="qq20" class="qq20" />
        <label for="option2">&nbsp;&nbsp;If it’s a monthly, a pack of 6 is $150, but if it is a toric, it will be $200, but if it’s a multifocal it’s also $200. <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;But if you want a 1 day, a pack of 30 will cost about $50, but if you need toric or multifocal it will be about <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$70</label></p>
        <p><input id="3" type="radio" value="c" name="qq20" class="qq20" />
        <label for="option3">&nbsp;&nbsp;It varies but most patient pay between $1-3 per day, which is less than your daily cup of coffee.</label></p>
    </div>

    <div id="quiz_response_qq20" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq20');return false;" id="btn_qq20" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <br />		
</div>


<div id="slide25" class="slide-content slide-background question">
	<br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="21"><li>
What should I do next, once I’ve had the conversation with the patient about contact lenses?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq21" class="qq21" />
        <label for="option1">&nbsp;&nbsp;Go into the fitting sets and give them a free trial.</label></p>
        <p><input id="2" type="radio" value="b" name="qq21" class="qq21" />
        <label for="option2">&nbsp;&nbsp;Let the optometrist know that we have had this conversation and the patient is interested in knowing more <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;about contact lenses.</label></p>
        <p><input id="3" type="radio" value="c" name="qq21" class="qq21" />
        <label for="option3">&nbsp;&nbsp;Don’t say anything as I already have enough work.</label></p>
    </div>
    <div id="quiz_response_qq21" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq21');return false;" id="btn_qq21" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 

    <br />		
</div>

<div id="slide26" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span>General Knowledge Questions</span></h2>
    <br />
    <br />
</div>

<div id="slide27" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="22"><li>
What is the ideal lens for someone who wants to wear their lenses occasionally?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq22" class="qq22" />
        <label for="option1">&nbsp;&nbsp;Daily</label></p>
        <p><input id="2" type="radio" value="b" name="qq22" class="qq22" />
        <label for="option2">&nbsp;&nbsp;2-weekly</label></p>
        <p><input id="3" type="radio" value="c" name="qq22" class="qq22" />
        <label for="option3">&nbsp;&nbsp;Monthly</label></p>
    </div>

    <div id="quiz_response_qq22" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq22');return false;" id="btn_qq22" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <p id="remqq22" class="remarks">1 day contact lenses are an affordable option to wear occasionally or daily in some circumstances.</p>	           
    <br />		
</div>

<div id="slide28" class="slide-content slide-background3 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><b>Fact:</b> Did you know there are two types of material, hydrogel which is the original soft contact lens and now there is silicone hydrogel material.</h2>
    <br />
    <br />
</div>

<div id="slide29" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="23"><li>
        Do you know the main benefits of the latest material silicone hydrogel over hyrogel?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq23" class="qq23" />
        <label for="option1">&nbsp;&nbsp;A. Smells better</label></p>
        <p><input id="2" type="radio" value="b" name="qq23" class="qq23" />
        <label for="option2">&nbsp;&nbsp;B. Looks better</label></p>
        <p><input id="3" type="radio" value="c" name="qq23" class="qq23" />
        <label for="option3">&nbsp;&nbsp;C. Allows more oxygen to the eyes</label></p>
        <p><input id="4" type="radio" value="d" name="qq23" class="qq23" />
        <label for="option3">&nbsp;&nbsp;D. Easier to handle</label></p>
        <p><input id="5" type="radio" value="e" name="qq23" class="qq23" />
        <label for="option3">&nbsp;&nbsp;E. C and D</label></p>                
    </div>

    <div id="quiz_response_qq23" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq23');return false;" id="btn_qq23" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <br />		
</div>


<div id="slide30" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="24"><li>
    We understand that more and more patients are now wearing 1 day lenses and that silicone hydrogel is the healthier material for patients. Do you know what contact lenses are made of silicone hydrogel 1 day?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq24" class="qq24" />
        <label for="option1">&nbsp;&nbsp;1 day Acuvue<sup>&reg;</sup> Moist<sup>&reg;</sup></label></p>
        <p><input id="2" type="radio" value="b" name="qq24" class="qq24" />
        <label for="option2">&nbsp;&nbsp;Focus dailies<sup>&reg;</sup> aqua comfort plus<sup>&reg;</sup></label></p>
        <p><input id="3" type="radio" value="c" name="qq24" class="qq24" />
        <label for="option3">&nbsp;&nbsp;Proclear<sup>&reg;</sup> 1 day</label></p>
        <p><input id="4" type="radio" value="d" name="qq24" class="qq24" />
        <label for="option3">&nbsp;&nbsp;MyDay</label></p>
        <p><input id="5" type="radio" value="e" name="qq24" class="qq24" />
        <label for="option3">&nbsp;&nbsp;Biomedics<sup>&reg;</sup> 1 day Extra</label></p>                
    </div>

    <div id="quiz_response_qq24" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq24');return false;" id="btn_qq24" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <br />		
</div>

<div id="slide31" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="25"><li>
What proportion of patients preferred MyDay for comfort when compared with 1 Day Acuvue<sup>&reg;</sup> Moist<sup>&reg;</sup>?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq25" class="qq25" />
        <label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="b" name="qq25" class="qq25" />
        <label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="c" name="qq25" class="qq25" />
        <label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="d" name="qq25" class="qq25" />
        <label for="option3">&nbsp;&nbsp;83%</label></p>
    </div>

    <div id="quiz_response_qq25" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq25');return false;" id="btn_qq25" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <br />		
</div>

<div id="slide32" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="26"><li>
What proportion of patients preferred MyDay overall when compared with 1 Day Acuvue<sup>&reg;</sup> Moist<sup>&reg;</sup>?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq26" class="qq26" />
        <label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="b" name="qq26" class="qq26" />
        <label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="c" name="qq26" class="qq26" />
        <label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="d" name="qq26" class="qq26" />
        <label for="option3">&nbsp;&nbsp;83%</label></p>
    </div>

    <div id="quiz_response_qq26" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq26');return false;" id="btn_qq26" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <br />		
</div>

<div id="slide33" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="27"><li>
What proportion of patients preferred 1 Day Acuvue<sup>&reg;</sup> TruEye<sup>&reg;</sup> when compared with MyDay?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq27" class="qq27" />
        <label for="option1">&nbsp;&nbsp;14%</label></p>
        <p><input id="2" type="radio" value="b" name="qq27" class="qq27" />
        <label for="option2">&nbsp;&nbsp;28%</label></p>
        <p><input id="3" type="radio" value="c" name="qq27" class="qq27" />
        <label for="option3">&nbsp;&nbsp;58%</label></p>
        <p><input id="4" type="radio" value="d" name="qq27" class="qq27" />
        <label for="option3">&nbsp;&nbsp;70%</label></p>
    </div>

    <div id="quiz_response_qq27" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq27');return false;" id="btn_qq27" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <br />		
</div>

<div id="slide34" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="28"><li>
Was the level of end of day comfort for MyDay statistically significantly greater than that for 1 Day Acuvue<sup>&reg;</sup> TruEye<sup>&reg;</sup>?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="a" name="qq28" class="qq28">
        <label for="option1">&nbsp;&nbsp;Yes</label></p>
        <p><input id="2" type="radio" value="b" name="qq28" class="qq28">
        <label for="option2">&nbsp;&nbsp;No</label></p>          
    </div>

    <div id="quiz_response_qq28" >
          <span class="xmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_cross.png" /><b class="ans"></b></span>
          <span class="cmark"><img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_check.png" /><b class="ans"></b></span>
    </div>
    <input onclick="checker('qq28');return false;" id="btn_qq28" type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_submit_smaller.png" class="quiz_submit"  /> 


    <br />		
</div>
</form>