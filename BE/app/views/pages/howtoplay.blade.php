@extends('layouts.master')

@section('content')

    <section class="how2p w960ma">
        <h2>How to Play</h2>
        <div class="w960ma clearfix">
            <ol>
                <li>
                    Earn one ticket to unlock a ranking game each time you do one of these tasks:
                    <ul>
                        <li>Register</li>
                        <li>Complete your designated learning module</li>
                        <li>Refer a colleague</li>
                    </ul>
                </li>
                <li>
                    Rank the Top 10 MyDay Contact Lens attributes in order of importance for achieving 	the best patient experience.
                    If you match the order set by the Promoter, you will win a trip to the International
                    Vision Expo and Conference 2014 held in Las Vegas.
                </li>
            </ol>
            <br />
            <div class="center">
                <input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_register.png" onclick="window.location='{{Config::get('facebook.BASE_URL')}}index.php/register';return false;" class="mr-b">
                <input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_login.png" onClick="window.location='{{Config::get('facebook.BASE_URL')}}index.php/login';return false;"> 
            </div>
        </div>
    </section>

@stop

@section('contentjs') 

<script>
    _PageManager.setPage("HOW2P");
</script>

@stop
