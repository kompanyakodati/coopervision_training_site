@extends('layouts.master')

@section('content')

            <section class="register content w960ma">

				<h2>Welcome to the Visions of Vegas website</h2>
                <h3>Please complete the details below to register. If you have already registered, please login <a href="{{Config::get('facebook.BASE_URL')}}index.php/login">HERE</a>.</h3>
                
                <div class="w960ma clearfix">
                    <form id="frmRegister">
                    	<div class="clearfix">
                            <section id="personal">
                                <h3>Personal Details</h3>
                                <input type="text" value="First Name" id="first_name" maxlength="250" />
                                <label for="first_name" class="asterix">*</label>
                                <input type="text" value="Last Name" id="lastname"  maxlength="250" />
                                <label for="last_name" class="asterix">*</label>
                                <input type="text" value="Email Address"  id="email"  maxlength="50"/>
                                <label for="email" class="asterix">*</label>
                                <select id="role">
                                    <option value="00">Job Role</option>
                                    <option value="1">Optometrist</option>                                
                                    <option value="2">Optical Dispenser</option>
                                    <option value="3">Practice Staff</option>                                
                                </select>
                                <label for="role" class="asterix jq-replace-ast">*</label>     
                                <input id="chkOwner" type="checkbox" > I am the owner of the practice.</input>
                                <br />
                                <input id="chkSrAssists" type="checkbox" > CooperVision rep promoted this training to me.</input>
                            </section>
                            <section id="practice">
                                <h3>Practice Details</h3>       
                                <input type="text" value="Practice Name" id="practice_name" maxlength="250" />
                                <label for="practice_name" class="asterix">*</label>
                                <input type="text" value="Practice Account Number" id="practice_account_number" maxlength="250" />
                                <label for="practice_account_number" class="asterix">*</label>                                
                                <select id="state">
                                    <option value="00">State</option>
                                    <option value="ACT">Australian Capital Territory</option>
                                    <option value="NSW">New South Wales</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NT">Northern Territory</option>
                                    <option value="QLD">Queensland</option>
                                    <option value="SA">South Australia</option>
                                    <option value="TAS">Tasmania</option>
                                    <option value="VIC">Victoria</option>
                                    <option value="WA">Western Australia</option>
                                </select>     
                                <label for="state" class="asterix jq-replace-ast">*</label>                                
                                <input type="text" value="Postcode" id="post_code"  maxlength="20"/>                                        
                                <label for="post_code" class="asterix">*</label>                                
                                <input type="text" value="CooperVision Rep Number" name="rep_number" id="rep_number" maxlength="9" style="display:none;" />
                                <!--
                                <select id="rep_number" >
                                    <option value="00">--Select Sales Rep--</option>
                                    <option value="AAAAAAAAA" >uat flok</option>                                    
                                    <option value="AU0000S06">Angi Hill</option>
                                    <option value="NZ0000S04">Cherie Norton</option>
                                    <option value="AU0000S13">Erlen Septiana</option>
                                    <option value="AU0000S04">Leah Hollway</option>
                                    <option value="AU0000S03">Deepa Bhana</option>
                                    <option value="AU0000S07">Paul Churchill</option>
                                    <option value="AU0000S02">Peter Fahey</option>
                                    <option value="AU0000S09">Phil Zirbel</option>
                                    <option value="AU0000S05">Sarah Mahony</option>
                                    <option value="AU0000S10">Sharon Steinert</option>
                                </select>
                                -->
                                <label for="rep_number" class="asterix">*</label>                                
                            </section>  
                        </div>                      
	                    <br />
                        <br />
		                <p class="errorfeedback" style="display:block;">If you do not receive your email with a ticket code, 
                            please check your Junk Mail folder and then add Sender to Safe Senders list.</p>
	                    <div class="center"><input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_register.png" onClick="register()"></div>                        
                    </form>
                </div>
            </section>



<script>
    $(function () {
        $(".register form select").selectbox();

        $('#rep_number').selectbox('disable');


        $('#chkSrAssists').change(function(){
            if ( $(this).is(':checked') ) {

                //$('#rep_number').selectbox('enable');

                $('input[name=rep_number]').show();
                $('input[name=rep_number]').val('CooperVision Rep Number');

            } else {
                
                //$('#rep_number').selectbox('disable');

                $('input[name=rep_number]').val("");
                $('input[name=rep_number]').hide();
            }
        });
    });

    $('#frmRegister').submit(function(e){  e.preventDefault(); return false;   });

    function getSelectValue( id ) {
        //Mac Safari fix
        var _val = false;
        $('#' + id ).find('option').each(function(){
            if ( $(this).prop('selected') ) _val = $(this).val();
        });
        return _val;
    }

    function register() {


        var _params = {
            'url' : "{{Config::get('facebook.BASE_URL')}}" + "index.php/api/register?",
            'args' : {
                'fname' : $.trim($('#first_name').val()),
                'lname' : $.trim($('#lastname').val()),
                'email' : $.trim($('#email').val()),
                'role' : getSelectValue('role'),
                'owner' : (($('#chkOwner').is(":checked"))) ? 1 : 0,
                'srassits' : (($('#chkSrAssists').is(":checked"))) ? 1 : 0,                
                'pname' : $.trim($('#practice_name').val()),
                'pactno' : $.trim($('#practice_account_number').val()),
                'state' : getSelectValue('state'),            
                'postal' : $.trim($('#post_code').val()),
                'repno' : $.trim($('#rep_number').val())                                                                                       
            }
        }

        $.getJSON(_params.url, _params.args,function(res){

            if (res.status == 'success') {

                if (res.issr == 'y') {

                    var _url = "{{Config::get('facebook.BASE_URL')}}" + "index.php/dashboard/complete/" + res.module + "/" + res.name + "/" + res.keycount + "?&t=y" + "&reg=1&rid=" + res.regid;

                } else {

                    var _url = "{{Config::get('facebook.BASE_URL')}}" + "index.php/dashboard/thankyou/" + res.module + "/" + res.name;                                      

                }

                console.log(_url);

                window.location.href = _url;


            } else {

                alert( res.msg );

            }

            

        });

    }
</script>




@stop

