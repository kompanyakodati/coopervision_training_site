<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Cooper Vision Training</title>	
</head>

<body>

<div style="font-family:arial;color:rgb(99,100,102);font-size:11pt;" >
<style type="text/css"> 
	a { font-family:arial;color:rgb(99,100,102); }
	.banner { width: 798px; height: 380px;} 
</style>
<div class="banner">
	<img src="http://chilp.it/a32859" />
</div>
<br /><br />

Hi, {{ucfirst($username)}}

<p>Hurry! There are only 2 weeks left of the "Visions of Vegas" program, and we we can see you are still yet to complete your learning module.</p>

<p>The training module only takes a few minutes to complete. You will earn an extra ticket to play the "Rank to Win" game when you complete the learning module and each time you refer a colleague who then registers for the program (provided you are the first person to make the referral).</p>

<p>It's quick and easy to participate. Visit 
<a href="http://www.coopervisiontraining.com.au" target="_blank" style="color:rgb(45,154,67)" >www.CooperVisionTraining.com.au</a> and click the login button. You will be asked to enter your first name, last name and email address that you used when registering.  Each ticket you earn is your entry to play a "Rank to Win" game for a chance to win a trip to attend the September International Vision Expo West in Las Vegas.
</p>

<p>So don't delay … visit <a href="http://www.coopervisiontraining.com.au" target="_blank" style="color:rgb(99,100,102);" >the site</a>
 now to increase your knowledge and your chances of winning!</p>

<p>Thanks and good luck!</p>

<p>CooperVision</p>
<br>
<br>

<div style="text-align:left;">
<div class="logo">
	<img src="http://chilp.it/bd8f0c" width=116 height=100>
</div>
<p style="font-weight:bold;color:rgb(45,154,67);font-size:11pt;"><span style="color:rgb(245,128,37);">NEW daily disposable silicone hydrogel</span><br>
MyDay™ daily disposable with Smart Silicone™.<br>
Delivers on everything. Compromises on nothing.<br>
<a href="http://coopervision.net.au/practitioner/contact-lens/myday" target="_blank" style="color:rgb(45,154,67);" >Click here</a> for more information</p>
</div>                

</div>

<p style="font-weight:bold;color:rgb(99,100,102);font-size:9pt;">See <a style="font-weight:bold;color:rgb(99,100,102);font-size:9pt;" href="http://coopervisiontraining.com.au/index.php/tnc" target="_blank">www.CooperVisionTraining.com.au</a> for full Terms & Conditions.</p>            
<br>
<br>
</div>
</body>
</html>
