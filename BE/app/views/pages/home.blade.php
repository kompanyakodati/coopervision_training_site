@extends('layouts.master')

@section('content')

    <section class="home w960ma center">

        <img src="{{Config::get('facebook.BASE_URL')}}assets/img/img_visionsVegas.png" alt="Super Cooper" />
        
        <h2 class="headline">
            Focus on MyDay™ and you could <span>WIN a trip to<br /> Vision Expo West, Las Vegas!</span>
        </h2>

        <div class="content-container clearfix">
            <br />
            <input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_register.png" onclick="window.location='{{Config::get('facebook.BASE_URL')}}index.php/register';return false;" class="mr-b">
            <input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_how2p_video.png" onClick="javascript:openVideo();">
        </div>
    </section>

@stop

@section('contentjs') 

<script>
    Shadowbox.init({
        skipSetup: true
    });
    $(window).load(function () {      
        openVideo();
    });
</script>

@stop
