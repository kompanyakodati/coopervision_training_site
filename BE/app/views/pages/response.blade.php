@extends('layouts.master')

@section('content')

    <section class="response w960ma">
        


        <h2 class="response">
            <?php if ( Input::get('pg')== '1' ) { ?>

                Thank you for Referring a Colleague to the <br /> Visions of Vegas Training Program!
                <br />
                You can refer an unlimited amount of colleagues throughout <br /> 
                the program; so keep referring for more chances to win!                
            
            <?php } else { ?>
                
                Thank you for submitting your ranking choice. 
                <br />

            <?php   if ( Session::get('registereduser') ) { 
                    $userRegId = Session::get('userid');
                    $userData = App::make('AppController')->getUserRegDetails( $userRegId ); 
                    $userFname = $userData[0]->firstname;
                    $jobRole = $userData[0]->jobrole;
                    //Check what module is selected based on job role
                    if ($jobRole == "2"  || $jobRole == "3" ) {
                        $module = "2";
                    } else {
                        $module = "1";
                    }                   

                    //Get Total key acquired 
                    $totalKeyCount = App::make('AppController')->getUserKeyCount( $userData[0]->id );

                    //check if user has competed the training
                    $trainingComplete = (  App::make('AppController')->hasAlreadyCompletedTraining($userData[0]->id) ) ? 'y':'n';

                    $autoLogin = Config::get('facebook.BASE_URL')."index.php/dashboard/complete/".$module."/".$userFname."/".$totalKeyCount."?t=".$trainingComplete."&reg=1";
                    
                ?>                    
                    Please return to the <a href="{{$autoLogin}}">Home Screen</a> to see more ways to earn tickets!
                <?php } else { ?>
                    Please return to the <a href="{{Config::get('facebook.BASE_URL')}}">Home Screen</a> to see more ways to earn tickets!
                <?php } ?>


            <?php } ?>

        </h2>
        <div class="w960ma clearfix">
				
                
                <!--
                <input type="button"  onclick="playAgain();return false;" value="Play another Rank To Win Game" />

                <input type="button"  onclick="referAfriend();return false;" value="Refer a Colleague" />
                   --> 

        </div>
    </section>

@stop

@section('contentjs') 

<script>

<?php if ( Input::get('pg')== '1' ) { ?>

    _PageManager.setPage("REFER");

<?php } else { ?>

    _PageManager.setPage("LEARNING");

<?php } ?>

  

function playAgain() {
	location.href = "{{Config::get('facebook.BASE_URL')}}index.php/rank";
}

function referAfriend() {	
	location.href = "{{Config::get('facebook.BASE_URL')}}index.php/refer";
}


</script>

@stop