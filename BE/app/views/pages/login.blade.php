@extends('layouts.master')

@section('content')

    <section class="login-form w960ma clearfix">

            <form>
		        <div class="form">            
					<h2>Already Registered?</h2>                
                    <input type="text" value="First Name" id="first_name" />
                    <label for="first_name" class="asterix">*</label>
                    <input type="text" value="Last Name" id="last_name"/>
                    <label for="last_name" class="asterix">*</label>
                    <input type="text" value="Email" id="email" />
                    <label for="email" class="asterix">*</label>
                </div>
                <p class="errorfeedback">* These are required fields.</p>
                <div class="btn_login">
					<input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_login.png" onclick="checkLoginCredentials();return false;" >
                </div>
            </form>
        

    </section>

@stop

@section('contentjs') 

<script>

    function checkLoginCredentials() {
        var _params = {
            'url' : "{{Config::get('facebook.BASE_URL')}}" + "index.php/api/validate?",
            'args' : { 
                'fname' : $.trim($('#first_name').val()),
                'lname' : $.trim($('#last_name').val()),
                'email' : $.trim($('#email').val()) 
            }
        }

        $.getJSON(_params.url, _params.args,function(res){
            
            if (res.status == 'success') {

                var _url = "{{Config::get('facebook.BASE_URL')}}" + "index.php/dashboard/complete/" + res.module + "/" + res.name + "/" + res.keycount + '?t=' + res.trainingcomplete + '&reg=1&rid=' + res.regid;
                $('.container').load(_url);

            } else {
                alert( res.msg );
            }

            

        });



    }

</script>


@stop
