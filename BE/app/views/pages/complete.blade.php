@extends('layouts.master')

@section('content')

   <section class="logged-in w960ma clearfix">

  		<?php if ( !Input::get('reg') ) { ?>
		<h2 class='response'>You have completed the training module.</h2>
		<?php } ?>
			<h2 class='response'>Hello, <span class='user'>{{$name}}</span></h2>
		
			<h3>You have this number of tickets available for play: <span id='keycount'>{{$keycount}}</span>. <a href='{{Config::get('facebook.BASE_URL')}}index.php/howtoplay'>How do I earn more tickets?</a></h3>
					<ul>
						<?php if ( Input::get('t') == 'n' ) { ?>
						<li class='giant-button'>
							<a href='{{Config::get('facebook.BASE_URL')}}index.php/learnmodule?&mod={{$module}}'>
								<img src='{{Config::get('facebook.BASE_URL')}}assets/img/img_coopered_tick.png' alt='Tick' />
									<p><span>REVIEW</span> Learning Module</p>
							</a>
						</li>
						<?php } ?>
						<?php if ( !App::make('AppController')->isUserRegisteredSalesRep( Input::get('rid')) ) { ?>
						<li class='giant-button'>
        					<a href='{{Config::get('facebook.BASE_URL')}}index.php/refer'>
	    						<img src='{{Config::get('facebook.BASE_URL')}}assets/img/img_coopered_avatar.png' alt='Avatar' />
								<p><span>REFER</span> a Colleague</p>
        					</a>
        				</li>
        				<?php } ?>
        				<li class='giant-button'>
        					<a href='{{Config::get('facebook.BASE_URL')}}index.php/rank'>
	    						<img src='{{Config::get('facebook.BASE_URL')}}assets/img/img_ticket.png' alt='Key' />
								<p><span>PLAY</span> Rank to Win Game</p>
							</a>
						</li>
		        </ul>
        <br />
        <br />

   </section>

@stop