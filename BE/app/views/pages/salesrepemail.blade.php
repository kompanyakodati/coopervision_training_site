<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Cooper Vision Training</title>	
</head>

<body>


<div style="font-family:arial;color:rgb(99,100,102);font-size:11pt;" >
<style type="text/css"> 
	a { font-family:arial;color:rgb(99,100,102); }
	.banner { width: 798px; height: 380px;} 
</style>
<div class="banner">
	<img src="http://chilp.it/a32859" />
</div>
<br /><br />


To {{ucfirst($name)}}

<p>Thank you for your assistance with creating awareness of the Vision of Vegas
Program with Opticians & Practice Staff & earning credits towards receiving a 
ticket for your chance to Rank & Win.</p>

<p>You have 10 registered credits, you are now entitled to 1x ticket for your 
chance to Win a Trip to Vision Expo West, Las Vegas!</p>

<p>For your chance to win, simply go online to www.CooperVisionTraining.com.au,
enter your unique code & rank the Top 10 MyDay contact lens attributes in 
order of importance for achieving the best patient experience.</p>

<br />
<p>Your Unique code is - <b>{{$keycode}}</b></p>
<br />

<p>If you match the order set by the Promoter, you will WIN a trip to Vision Expo
West, Las Vegas.</p>

<p>All entries and answers will be stored until the end of the promotion with 
judging of entries to take place on 18th June 2014. 
All Winners will be notified by phone or email within 5 working days of judging 
taking place.</p>

<p>For further information please see full terms & conditions at 
<a href="http://www.CooperVisionTraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a></p>

<p>Good Luck !!</p>


<p>CooperVision</p>
<br>
<br>

<div style="text-align:left;">
<div class="logo">
	<img src="http://chilp.it/bd8f0c" width=116 height=100>
</div>
<p style="font-weight:bold;color:rgb(45,154,67);font-size:11pt;"><span style="color:rgb(245,128,37);">NEW daily disposable silicone hydrogel</span><br>
MyDay™ daily disposable with Smart Silicone™.<br>
Delivers on everything. Compromises on nothing.<br>
<a href="http://coopervision.net.au/practitioner/contact-lens/myday" target="_blank" style="color:rgb(45,154,67);" >Click here</a> for more information</p>
</div>                

</div>

<p style="font-weight:bold;color:rgb(99,100,102);font-size:11pt;">See <a style="font-weight:bold;color:rgb(99,100,102);font-size:11pt;" href="http://coopervisiontraining.com.au/index.php/tnc" target="_blank">www.CooperVisionTraining.com.au</a> for full Terms & Conditions.</p>            
<br>
<br>
</div>
</body>
</html>
