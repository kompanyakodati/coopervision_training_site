@extends('layouts.master')

@section('content')
    
    <section class="module w960ma">
        
        <section id="slide">
        </section>
        
        <section class="progress clearfix">
            <p>Your Progress: <span id="progress">1/22</span></p>
            <p><span id="timer"></span></p>
            <div class="buttons">
                <input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/1x1.png" class="mr-sb back" text="">
                <input type="image" src="{{Config::get('facebook.BASE_URL')}}assets/img/1x1.png" class="next">
            </div>
        </section>
        
        
    </section>


@stop

@section('contentjs') 

    <script>

    
<?php if (Session::get('userid') ) { ?>

        {{-- Get what module is for the current user  --}}
        <?php
            if (Session::get('userid') ) {   
                $req = Input::all();
                $module = ( isset( $req['mod'] ) ) ? $req['mod'] : "1";

                //Get User Name
                $regId = Session::get('userid');
                $userRegData = App::make('AppController')->getUserRegDetails( $regId );
                $userName = $userRegData[0]->firstname;
            }   
        ?>

        var _LearningManager = new LearningManager();

        _LearningManager.base_url ="{{Config::get('facebook.BASE_URL')}}";
        _LearningManager.username = "{{$userName}}";
        _LearningManager.regid = "{{$regId}}";
        
        _PageManager.setPage("LEARNING");
        
        <?php if (  $module == "2" ) { ?>

            //Module 2
            _LearningManager.module = "2";
            _LearningManager.setRole("OPTICALDISPENSER");
            //_LearningManager.setRole("PRACTICE_STAFF");

        <?php } else { ?>

            //Module 1
            _LearningManager.module = "1";            
            _LearningManager.setRole("OPTOMETRISTS");

        <?php } ?>

        _LearningManager.init();        


    
        
        /* checkers */
        function checker(qcode) {
            
        var _chks = $("div.questions input."+qcode);

        var _val = $('input[name='+qcode+']:checked').val();
        
        if (_val === undefined) {

            $('#quiz_response_'+ qcode +' .xmark').show();
            $('#quiz_response_'+ qcode +' .cmark').hide();
            $('#quiz_response_'+ qcode).find('b.ans').html('Please select an answer');                        
            if  ($('#rem'+qcode).length) { $('#rem'+qcode).hide();  }            
        } else {

            //$.each(_chks,function(i,v){
            //    if ( $(v).prop('checked') === true ) _val = $(v).val();
            //});
            //unset var
            //delete _chks;

            switch(qcode) {

                case 'q1' : 
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;   

                case 'q2' : 
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;     

                case 'q3' : 
                    if ( _val  == 'a' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;        

               case 'q4' : 
                    if ( _val  == 'd' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;           

               case 'q5' : 
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                         

               case 'q6' : 
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                         

               case 'q7' : 
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;  

               case 'q8' : 
                    if ( _val  == 'd' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break; 

               case 'q9' : 
                    if ( _val  == 'a' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;      

               case 'q10' : 
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                                    

               case 'q11' : 
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;      

               case 'q12' : 
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;      

               case 'q13' : 
                    if ( _val  == 'a' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;             

           

                 ///Practicestaff qs
               case 'qq1' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                                  

               case 'qq2' :
                    if ( _val  == 'a' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;   

               case 'qq3' :
                    if ( _val  == 'd' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;      

               case 'qq4' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;      

               case 'qq5' :
                    if ( _val  == 'd' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                                                                                                            

               case 'qq6' :
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                                                                                                            

               case 'qq7' :
                    if ( _val  == 'd' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;  

               case 'qq8' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;  

               case 'qq9' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;  

               case 'qq10' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq11' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;


               case 'qq12' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;


               case 'qq13' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq14' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq15' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;


               case 'qq16' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq17' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;


               case 'qq18' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq19' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;


               case 'qq20' :
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq21' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq22' :
                    if ( _val  == 'a' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;

               case 'qq23' :
                    if ( _val  == 'e' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break;                    


               case 'qq24' :
                    if ( _val  == 'd' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break; 

               case 'qq25' :
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break; 

               case 'qq26' :
                    if ( _val  == 'c' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break; 


               case 'qq27' :
                    if ( _val  == 'b' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break; 

               case 'qq28' :
                    if ( _val  == 'a' ) {
                        disabler(true,qcode);
                    } else {
                        disabler(false,qcode);
                    }    
                    break; 

                }
            } 

        }


        function disabler(answright,qcode) {

            var _chks = $("div.questions input."+qcode);


            if ( answright === true ) {
                
                $('#quiz_response_'+ qcode +' .cmark').show();
                $('#quiz_response_'+ qcode +' .xmark').hide();                
                $('#quiz_response_'+ qcode).find('b.ans').html('Correct.');
                _LearningManager.enable($(".module .next"), _LearningManager.next);
                _chks.attr('disabled','disabled');    $('#btn_' + qcode).hide();        
                if  ($('#rem'+qcode).length) $('#rem'+qcode).show();
                                
            } else {

                $('#quiz_response_'+ qcode +' .xmark').show();
                $('#quiz_response_'+ qcode +' .cmark').hide();
                $('#quiz_response_'+ qcode).find('b.ans').html('Wrong answer');                        
                if  ($('#rem'+qcode).length) $('#rem'+qcode).hide();


            }
            

        }



<?php } else { ?>

       location.href = "{{Config::get('facebook.BASE_URL')}}index.php/login";
       
<?php } ?>


    </script>


@stop
