@extends('layouts.master')

@section('content')
  <section class="logged-in w960ma clearfix">

		<h2 class='response'>Thank you for registering, <span class='user'>{{$name}}</span></h2>
		<ul>
			<li class='giant-button'>
				<a href="{{Config::get('facebook.BASE_URL')}}index.php/learnmodule?&mod={{$module}}">
				<img src='{{Config::get('facebook.BASE_URL')}}assets/img/img_coopered_tick.png' alt='Tick' />
				<p><span>Commence</span> Learning Module</p>
				</a>
			</li>
        </ul>
        <br />
        <br />
        
 </section>

@stop