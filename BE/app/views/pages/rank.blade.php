@extends('layouts.master')

@section('content')


<script>
    var selectedOptions = [];
</script>

        <section class="rank w960ma">
        
            <h2>Rank to Win Game</h2>
            <div class="clearfix" style="padding-bottom:20px;">
                <article class='intro center'>
                    <h3>Rank the top 10 attributes of MyDay in order of importance for achieving the best patient experience 
                    by selecting the right order from 1-10, with 1 being the most important.</h3>
                    <h3>If you match the Promoter's ranking, you will win a <b>Trip to Vision Expo West, Las Vegas</b>. It’s that easy!</h3>
                </article>
                
                <article id="step1" class="clearfix">
                    <h3>Step 1: Verify your code</h3>                    
                    <div class="step1">
                        <label for="step1_input">Enter Your Ticket Code Here</label>
                        <input id="step1_input" maxlength="10"></input>
                        <div id="loader"></div>                        
                    </div>
                    <span class="input_feedback error" style="display:block">
                        You should have received an email with your unique Ticket Code.
                        If you can't find it, check your Junk Mail folder and then add Sender to Safe Senders list.
                    </span>
                </article>

            </div>
        </section>

        
        <div class="answers_container" id="step2">
            <section class="answers">
                <h3>Step 2: Choose your answers</h3>
                <p> 
                    What are the top 10 attributes of MyDay&#153; Contact Lenses for achieving the best patient experience?
                    <span>Rearrange the options from 1-10, with 1 being the most important.</span>
                </p>

                    <ul id="answers">
                        <li class="clearfix">
                            <label for="option1">1.</label>
                            <select id="option1"></select>                          
                        </li>
                        <li class="clearfix">
                            <label for="option2">2.</label>
                            <select id="option2"></select>                          
                        </li>
                        <li class="clearfix">
                            <label for="option3">3.</label>
                            <select id="option3"></select>                          
                        </li>
                        <li class="clearfix">
                            <label for="option4">4.</label>
                            <select id="option4"></select>                          
                        </li>                       
                        <li class="clearfix">
                            <label for="option5">5.</label>
                            <select id="option5"></select>                          
                        </li>   
                        <li class="clearfix">
                            <label for="option6">6.</label>
                            <select id="option6"></select>                          
                        </li>
                        <li class="clearfix">
                            <label for="option7">7.</label>
                            <select id="option7"></select>                          
                        </li>                       
                        <li class="clearfix">
                            <label for="option8">8.</label>
                            <select id="option8"></select>                          
                        </li>   
                        <li class="clearfix">
                            <label for="option9">9.</label>
                            <select id="option9"></select>                          
                        </li>   
                        <li class="clearfix">
                            <label for="option10">10.</label>
                            <select id="option10"></select>                         
                        </li>                           
                    </ul>
                    
                    <input type="image" id="submit" value="Submit" src="{{Config::get('facebook.BASE_URL')}}assets/img/btn_confirm.png" onClick="gotoConfirmationPage();return false;" />

            </section>
        </div>
        

@stop

@section('contentjs') 

<script>
    
<?php if (Session::get('userid') ) { ?>

    <?php
        if (Session::get('userid') ) {  
            $regId = Session::get('userid');
            $userRegData = App::make('AppController')->getUserRegDetails( $regId );
            $userName = $userRegData[0]->firstname;
            $jobRole = $userRegData[0]->jobrole;
            //Check what module is selected based on job role
            if ($jobRole == "2"  || $jobRole == "3" ) {
                $module = "2";
            } else {
                $module = "1";
            }    
        }      
    ?>

    _PageManager.setPage("RANK");       
    _PageManager.base_url = "{{Config::get('facebook.BASE_URL')}}";
    _PageManager.module = "{{$module}}";
    _PageManager.username = "{{$userName}}";
    _PageManager.regid = "{{$regId}}";
    
        
    $(function () {
        
        var _GameMaster = new GameMaster();     
        _GameMaster.init();
                    
        for (var i = 1; i < 11; i++) { 
            $("#option"+i).selectbox({
                onChange: function(e) {
                    _GameMaster.update($(this), e);
                }
            }); 
        };
        $("#answers").find(".sbHolder .sbSelector").each(function(e) {
            if ($(this).html() == "SELECT AN OPTION") {
            $(this).addClass("default");

            }
        });
        
        var $input = $('#step1 input');
        $input.keyup(function(e) {
            var max = 10;
            if ($input.val().length > max) {
                $input.val($input.val().substr(0, max));
            }
        });         
    });    


    var _values = [];
    function gotoConfirmationPage() {
        var _data = [];

        //collect data
        $("#answers .sbSelector").each(function() { _values.push($(this).html()); });

        //check if all are set
        $.each(_values,function(i,v){
            if ( v == "SELECT AN OPTION" ) {
                delete _values[i];
            } else {
                _data.push( v );
            }
        });

        _values = [];//reset for the next session

        if (_data.length < 10) {
            alert('Please check your ranking, to continue');
            return;
        } 
        var sortings = _data.join("|");
        var _params = 'sort='+encodeURIComponent(sortings)+'&kcode=' + $('#step1 input').val();
        location.href = "{{Config::get('facebook.BASE_URL')}}index.php/confirmation?&" + _params ;     

    }


<?php } else { ?>

       location.href = "{{Config::get('facebook.BASE_URL')}}index.php/login";
       
<?php } ?>
</script>

@stop
