<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Cooper Vision Training</title>	
</head>

<body>

<div style="font-family:arial;color:rgb(99,100,102);font-size:11pt;" >
<style type="text/css"> 
	a { font-family:arial;color:rgb(99,100,102); }
	.banner { width: 798px; height: 380px;} 
</style>
<div class="banner">
	<img src="http://chilp.it/a32859" />
</div>
<br /><br />

Hi, {{ucfirst($username)}}

<p>Thanks for registering for the CooperVision "Visions of Vegas" online training program and for <br />
completing your learning module. Just a reminder, you can earn more tickets and increase your chances <br />
of winning by referring a colleague who signs up for the program.</p>


<p>For each ticket you earn you can play the "Rank to Win" game for a chance to win a trip to
   the September International Vision Expo West in Las Vegas!</p>


<p>It's quick and easy to participate. Visit 
<a href="http://www.coopervisiontraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a> and click the login button.
You will be asked to enter your first name, last name and email address that you used when registering. 
You may refer as many colleagues as you like, and you will earn an extra ticket to play the "Rank to Win" game each time 
one of your referrals registers for the program (provided you are the first person to make the referral).</p>


<p>So don't delay … visit <a href="http://www.coopervisiontraining.com.au" target="_blank">the site</a>
 now to increase your knowledge and your chances of winning!</p>

<p>Thanks and good luck!</p>

<p>CooperVision</p>
<br>
<br>

<div style="text-align:left;">
<div class="logo">
	<img src="http://chilp.it/bd8f0c" width=116 height=100>
</div>
<p style="font-weight:bold;color:rgb(45,154,67);font-size:11pt;"><span style="color:rgb(245,128,37);">NEW daily disposable silicone hydrogel</span><br>
MyDay™ daily disposable with Smart Silicone™.<br>
Delivers on everything. Compromises on nothing.<br>
<a href="http://coopervision.net.au/practitioner/contact-lens/myday" target="_blank" style="color:rgb(45,154,67);" >Click here</a> for more information</p>
</div>                

</div>

<p style="font-weight:bold;color:rgb(99,100,102);font-size:11pt;">See <a style="font-weight:bold;color:rgb(99,100,102);font-size:11pt;" href="http://coopervisiontraining.com.au/index.php/tnc" target="_blank">www.CooperVisionTraining.com.au</a> for full Terms & Conditions.</p>            
<br>
<br>
</div>
</body>
</html>
