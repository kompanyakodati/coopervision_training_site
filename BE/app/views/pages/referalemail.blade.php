<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Cooper Vision Training</title>	
</head>

<body>


<div style="font-family:arial;color:rgb(99,100,102);font-size:11pt;" >
<style type="text/css"> 
	a { font-family:arial;color:rgb(99,100,102); }
	.banner { width: 798px; height: 380px;} 
</style>
<div class="banner">
	<img src="http://chilp.it/a32859" />
</div>
<br /><br />


Hello, {{ucfirst($rcvrname)}}

<br />

<p>{{$sndername}} has referred you to register and complete the CooperVision "Visions of Vegas" 
training program to give you the chance to Win a trip to Vision Expo West, Las Vegas!</p>

<p>To earn tickets that give you chances to win, simply go online to <a href="http://www.CooperVisionTraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a> and</p>

<ul>
	<li>Register</li>
	<li>Complete your designated learning module</li>
	<li>Refer a colleague</li>
</ul>	

<p>Each time you earn a ticket, you will be emailed a unique code, which unlocks a "Rank To Win" game. 
Rank the Top 10 MyDay contact lens attributes in order of importance for achieving the best patient experience. 
If you match the order set by the Promoter, you will Win a trip to Vision Expo West, Las Vegas.</p>

<p>For further information please see full <a href="http://www.coopervisiontraining.com.au/index.php/tnc" target="_blank">terms & conditions</a>.</p>

<p>Good Luck!</p>

<p>CooperVision</p>
<br>
<br>

<div style="text-align:left;">
<div class="logo">
	<img src="http://chilp.it/bd8f0c" width=116 height=100>
</div>
<p style="font-weight:bold;color:rgb(45,154,67);font-size:11pt;"><span style="color:rgb(245,128,37);">NEW daily disposable silicone hydrogel</span><br>
MyDay™ daily disposable with Smart Silicone™.<br>
Delivers on everything. Compromises on nothing.<br>
<a href="http://coopervision.net.au/practitioner/contact-lens/myday" target="_blank" style="color:rgb(45,154,67);" >Click here</a> for more information</p>
</div>                

</div>

<p style="font-weight:bold;color:rgb(99,100,102);font-size:11pt;">See <a style="font-weight:bold;color:rgb(99,100,102);font-size:11pt;" href="http://coopervisiontraining.com.au/index.php/tnc" target="_blank">www.CooperVisionTraining.com.au</a> for full Terms & Conditions.</p>            
<br>
<br>
</div>
</body>
</html>
