@extends('layouts.admin')

@section('content')

      <div class="jumbotron">

      	  <h3>Reports</h3>

	      <b>Total No. of Registrants:</b> {{$total_registrants}}
	      <br />
	      <b>Total No. of Keys: </b> {{$total_keys}}
	      <br />
	      <b>Total No. of Keys earned by Training: </b> {{$total_key_via_training}}
	      <br />
	      <b>Total No. of Keys by Referral: </b> {{$total_key_via_referral}}
	      <br />



      </div>

@stop