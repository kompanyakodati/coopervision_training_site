<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesRepTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_rep', function($table)
		{			
			$table->engine = 'InnoDB';						
			$table->increments("id"); 				
			$table->string('rep_no', 20); //Sales Rep No.
			$table->string("first_name", 100);			
			$table->string("last_name", 100);			
			$table->string("email", 50);	
			$table->integer("no_credits")->default(0);  //Accumulated no. of earned credits from Registraion with SR_ID
			$table->integer("no_keys")->default(0);	//Accumulated no. of keys, based on Keys Table		
			$table->dateTime('last_updated'); //Date last updated ( no. of credits, no. keys )

		    //Add indexes
		    $table->unique('rep_no'); //Unique sales rep id

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("sales_rep");
	}

}
