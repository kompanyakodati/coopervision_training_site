<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamePlayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gameplay', function($table)
		{			
			$table->engine = 'InnoDB';						
			$table->increments("id"); 				
			$table->integer("user_reg_id");
			$table->string('keycode',8); //Alphanumeric
			$table->mediumText('sortings'); //Pipe delimited sorted items
			$table->dateTime('date_played');		

		    //Add indexes
		    $table->unique('keycode');
		   	$table->unique(array('user_reg_id','keycode'));

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("gameplay");
	}

}
