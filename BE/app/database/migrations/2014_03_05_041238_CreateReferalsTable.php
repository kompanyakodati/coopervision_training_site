<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referral', function($table)
		{			
			$table->engine = 'InnoDB';						
			$table->increments("id"); 				

			//Referer Details
			$table->integer('sndr_reg_id');
			$table->string("sndr_email", 50);
			//Colegue Details
			$table->string("rcvr_firstname", 200); 
			$table->string("rcvr_lastname", 200);
			$table->string("rcvr_email", 50);			

			//Flag to determine if the first referee already rewarding with keycode				
			//Second referee can not earned any credit anymore, it's only the first referee can get the reward credit keycode
			$table->enum('sndr_rewarded', array('y', 'n'))->default('n'); 

			$table->dateTime('date_sent');

		    //Add indexes
		    $table->unique('rcvr_email'); //Unique email to make a valid referal


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("referral");
	}

}
