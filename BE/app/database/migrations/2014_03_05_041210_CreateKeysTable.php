<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('keys', function($table)
		{			
			$table->engine = 'InnoDB';						
			$table->increments("id"); 						
			$table->string('code',8); //Alphanumeric
			$table->enum('source', array('registration', 'referal','training','credits'));	//Credits is for SR, after accumulating 10 credits						
			$table->integer('user_reg_id');
			$table->string('sales_rep_id',20);
			$table->enum('isuser' ,array('y','n'))->default('y');
			$table->dateTime('date_acquired');

		    //Add indexes
		    $table->unique('code');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("keys");
	}

}
