<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrantsTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registrants', function($table)
		{			
			$table->engine = 'InnoDB';						
			$table->increments("id"); 			
			
			//Personal Details
			$table->string("firstname" , 250);	
			$table->string("lastname" , 250);				
			$table->string("email", 50);	
			/*
				1 - Optometrist
				2 - Optical Dispenser
				3 - Practice Staff
			*/
			$table->enum('jobrole', array('1', '2','3'))->default('1');				
			$table->enum('practiceowner', array('y', 'n'))->default('n');				
			$table->enum('srassisted', array('y', 'n'))->default('n'); //Sales Rep Assisted checked
			
			//Practice Details
			$table->string("practicename" , 250);	
			$table->string("practiceactno" , 50);				
			$table->string("state" , 50);							
			$table->string("postcode" , 20);
			$table->string("cvrepno" , 50);
			$table->dateTime('date_registered');

		    //Add indexes
		    $table->unique('email');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("registrants");
	}


}
