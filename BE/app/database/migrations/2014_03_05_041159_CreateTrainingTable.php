<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trainings', function($table)
		{			
			$table->engine = 'InnoDB';						
			$table->increments("id"); 						
			$table->integer('user_reg_id');
			$table->integer('module');			
			$table->enum('status', array('completed', 'pending'))->default('pending');							
			$table->dateTime('date_acquired');

		    //Add indexes
		    $table->unique(array('user_reg_id','module')); //Modules can only be completed once per user

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("trainings");
	}

}
