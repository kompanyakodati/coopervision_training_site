<?php

// CampaignTableSeeder.php    
class RegistrantTableSeeder extends Seeder {

    public function run()
    {

        $salesRepDetais = [
            ['firstname'=> 'Angi'   , 'lastname'=> 'Hill'       , 'email'=> 'ahill@au.coopervision.com'     ],
            ['firstname'=> 'Cherie' , 'lastname'=> 'Norton'     , 'email'=> 'cnorton@coopervision.com'      ],            
            ['firstname'=> 'Erlen'  , 'lastname'=> 'Septiana'   , 'email'=> 'eseptiana@au.coopervision.com' ],
            ['firstname'=> 'Leah'   , 'lastname'=> 'Hollway'    , 'email'=> 'Lhollway@au.coopervision.com'  ],            
            ['firstname'=> 'Deepa'  , 'lastname'=> 'Bhana'      , 'email'=> 'Dbhana@au.coopervision.com'    ],
            ['firstname'=> 'Paul'   , 'lastname'=> 'Churchill'  , 'email'=> 'Pchurchill@au.coopervision.com'],            
            ['firstname'=> 'Peter'  , 'lastname'=> 'Fahey'      , 'email'=> 'Pfahey@au.coopervision.com'    ],
            ['firstname'=> 'Phil'   , 'lastname'=> 'Zirbel'     , 'email'=> 'Pzirbel@au.coopervision.com'   ],            
            ['firstname'=> 'Sarah'  , 'lastname'=> 'Mahony'     , 'email'=> 'Smahony@au.coopervision.com'   ],
            ['firstname'=> 'Sharon' , 'lastname'=> 'Steinert'   , 'email'=> 'Ssteinert@au.coopervision.com' ],            
            ['firstname'=> 'uat'    , 'lastname'=> 'flok'       , 'email'=> 'amelia@bamboomarketing.com.au' ]            
        ];

        foreach ($salesRepDetais as $sr) {
            try {
                DB::table('registrants')->insert($sr);
            } catch(Exception $e) {}            
        }


    }

}