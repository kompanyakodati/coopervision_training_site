<?php

// CampaignTableSeeder.php    
class CampaignSettingsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('campaign_settings')->truncate();

        $campaignTable = [
            //Admin User Account
            ['property' => 'admin_username' , 'value' => 'adminuser'],
            ['property' => 'admin_password' , 'value' => 'admin007'],

            //Campaign End
            ['property' => 'campaign_end' , 'value' => '2014-06-16 11:59:59']

        ];
        DB::table('campaign_settings')->insert($campaignTable);
    }

}