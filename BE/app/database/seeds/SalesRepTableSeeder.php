<?php

// CampaignTableSeeder.php    
class SalesRepTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sales_rep')->truncate();

        $salesRepTable = [
            ['rep_no'=> 'AU0000S06' , 'first_name'=> 'Angi'     , 'last_name'=> 'Hill'      , 'email'=> 'ahill@au.coopervision.com'     ],
            ['rep_no'=> 'NZ0000S04' , 'first_name'=> 'Cherie'   , 'last_name'=> 'Norton'    , 'email'=> 'cnorton@coopervision.com'      ],
            ['rep_no'=> 'AU0000S13' , 'first_name'=> 'Erlen'    , 'last_name'=> 'Septiana'  , 'email'=> 'eseptiana@au.coopervision.com' ],
            ['rep_no'=> 'AU0000S04' , 'first_name'=> 'Leah'     , 'last_name'=> 'Hollway'   , 'email'=> 'Lhollway@au.coopervision.com'  ],
            ['rep_no'=> 'AU0000S03' , 'first_name'=> 'Deepa'    , 'last_name'=> 'Bhana'     , 'email'=> 'Dbhana@au.coopervision.com'    ],
            ['rep_no'=> 'AU0000S07' , 'first_name'=> 'Paul'     , 'last_name'=> 'Churchill' , 'email'=> 'Pchurchill@au.coopervision.com'],
            ['rep_no'=> 'AU0000S02' , 'first_name'=> 'Peter'    , 'last_name'=> 'Fahey'     , 'email'=> 'Pfahey@au.coopervision.com'    ],
            ['rep_no'=> 'AU0000S09' , 'first_name'=> 'Phil'     , 'last_name'=> 'Zirbel'    , 'email'=> 'Pzirbel@au.coopervision.com'   ],
            ['rep_no'=> 'AU0000S05' , 'first_name'=> 'Sarah'    , 'last_name'=> 'Mahony'    , 'email'=> 'Smahony@au.coopervision.com'   ],
            ['rep_no'=> 'AU0000S10' , 'first_name'=> 'Sharon'   , 'last_name'=> 'Steinert'  , 'email'=> 'Ssteinert@au.coopervision.com' ],
            ['rep_no'=> 'AAAAAAAAA' , 'first_name'=> 'flok'     , 'last_name'=> 'sales rep' , 'email'=> 'xerxis@flok.co' ]
        ];

        DB::table('sales_rep')->insert($salesRepTable);
    }

}