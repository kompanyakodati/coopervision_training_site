var indexOf = function(needle) {
    if(typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function(needle) {
            var i = -1, index = -1;

            for(i = 0; i < this.length; i++) {
                if(this[i] === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle);
};

function findDeselectedItem(CurrentArray, PreviousArray) {

   var CurrentArrSize = CurrentArray.length;
   var PreviousArrSize = PreviousArray.length;

   // loop through previous array
   for(var j = 0; j < PreviousArrSize; j++) {

      // look for same thing in new array
      if (CurrentArray.indexOf(PreviousArray[j]) == -1)
         return PreviousArray[j];
   }
   return null;
}


function ajaxpage(url, container, target) {

	//var _url = _LearningManager.base_url  + url;
	var _url = '/index.php' + url;

	$('#' + container).load(_url + ' #' + target, function() {		
		_LearningManager.checkSlideType();
	});	
}

function openVideo() {
	Shadowbox.open({
	   content: '<video controls autoplay style="width:100%;height:100%;">'
					+'<source src="http://coopervisiontraining.com.au/assets/vid/CooperVisions_Vegas_v2_720P.mp4" type="video/mp4" />'
					+'<source src="http://coopervisiontraining.com.au/assets/vid/CooperVisions_Vegas_v2_720P.webmsd.webm" type="video/webm" />'
					+'<source src="http://coopervisiontraining.com.au/assets/vid/CooperVisions_Vegas_v2_720P.webmsd.ogv" type="video/ogg" />'
					+'<object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="100%" height="100%">'
						+'<param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />'
						+'<param name="allowFullScreen" value="true" />'
						+'<param name="wmode" value="transparent" />'
						+'<param name="flashVars" value="config={&#39;playlist&#39;:[{&#39;url&#39;:&#39;http%3A%2F%2Fcoopervisiontraining.com.au%2Fassets%2Fvid%2FCooperVisions_Vegas_v2_720P.mp4&#39;,&#39;autoPlay&#39;:true}]}" />'
						+'<span title="No video playback capabilities, please download the video below">CooperVision MyDay</span>'
						+'<p style="color:#fff;"><strong>Download video:</strong> <a href="http://coopervisiontraining.com.au/assets/vid/CooperVisions_Vegas_v2_720P.mp4">MP4 format</a> | <a href="http://coopervisiontraining.com.au/assets/vid/CooperVisions_Vegas_v2_720P.webmsd.ogv">Ogg format</a> | <a href="http://coopervisiontraining.com.au/assets/vid/CooperVisions_Vegas_v2_720P.webmsd.webm">WebM format</a></p>'
					+'</object>'
				+'</video>',
	   player:  "html",
	   title:   'CooperVision MyDay',
	   width:   750,
	   height:  422,
	   options: {
		   modal:           true,
		   overlayOpacity: .75
	   }
	 });						
}

function PageManager() {
	this.PAGE;
	this.base_url;
	this.module;
	this.username;
	this.regid;
}
					
PageManager.prototype = {
	setPage: function(value) {
		switch(value) {
			case "RANK":
				$("#main_nav option[link='RANK']").attr("selected", "selected");
				this.PAGE = "RANK";
			break;	
			case "HOW2P":
				$("#main_nav option[link='HOW2P']").attr("selected", "selected");
				this.PAGE = "HOW2P";
			break;
			case "LEARNING":
				$("#main_nav option[link='LEARNING']").attr("selected", "selected");
				this.PAGE = "LEARNING";		
			break;
			case "REFER":
				$("#main_nav option[link='REFER']").attr("selected", "selected");
				this.PAGE = "REFER";
			break;						
		}
	},
	getPage: function() {
		return this.PAGE;	
	}
}


function GameMaster() {
	
	this.questions = [
		"SELECT AN OPTION",
		"Daily disposable",
		"Silicone hydrogel with Smart Silicone chemistry",
		"High oxygen transmissiblity",
		"Easy handling",
		"UV filter",
		"Available between -10.00D and +6.00D",
		"After 1 week of wear MyDay lenses are preferred overall compared to 1 day Acuvue TrueEye (p less than 0.01). Note: TruEye used is narafilcon a. Refer to sales aid XSM3430.",
		"MyDay lenses provide better end of day comfort versus 1 day Acuvue TruEye (p less than 0.001). Note: TruEye used is narafilcon a. Refer to sales aid XSM3430.",
		"MyDay provides superior overall comfort compared to 1 day Acuvue Moist. Refer to sales aid XSM3430.",
		"MyDay provides better comfort than 1 day Acuvue Moist throughout the day. Refer to sales aid XSM3430."
	];

}

GameMaster.prototype = {
	init: function() {
		var b = [];
		$("#answers").find("li select").each(function() { 
			b.push($(this)); 
		});
		
		for (var i = 0; i < 10; i++) {
			for (var j = 0; j <= 10; j++) {
				var c = "<option>" + this.questions[j] + "</option>";
				b[i].append(c);
			}
		}
	},
	update: function(target, inst) {
		$("#answers").find(".sbHolder .sbSelector").each(function(e) {
			
			if ($(this).parent().prev().attr('id') != target.attr('id')) {
				if ($(this).html() == inst) {
					$(this).html("SELECT AN OPTION");
					

				}
			}

			if ($(this).html() != "SELECT AN OPTION") $(this).removeClass("default"); else $(this).addClass("default");
		});
	}
}


function LearningManager() {	
	this.i;
	this.MAX_PAGE;
	this.timer;
	this.slide_target;
	this.base_url;
	this.__this = this;
	this.module;
	this.username;
	this.regid;
}

LearningManager.prototype = {
	init: function() {
		var _self = this;
		_self.i = 0;
		
		ajaxpage(this.slide_target,"slide","slide"+this.i);

		$("#progress").html(_self.i+"/"+_self.MAX_PAGE);		
		if (_self.i != 0) { $(".progress").fadeIn(); }



	},
	
	setRole: function(role) {
		if (role == "PRACTICE_STAFF") {
			this.slide_target = "/module/2";	
			this.MAX_PAGE = 34;
		}
		else if (role == "OPTOMETRISTS") {
			this.slide_target = "/module/1";	
			this.MAX_PAGE = 20; 
		}
		else if (role == "OPTICALDISPENSER") {
			this.slide_target = "/module/2";	
			this.MAX_PAGE = 34; 
		}

	},
	
	checkSlideType: function() {
		var Manager = _LearningManager;

		if ($(".slide-content").hasClass("question")) {
			//Manager.enable($(".module .next"), Manager.next);
			Manager.disable($(".module .next"), Manager.next);
			Manager.enable($(".module .back"), Manager.prev);		
			$('#timer').text('');			
		}
		else {
			Manager.disable($(".module .next"), Manager.next);
			Manager.disable($(".module .back"), Manager.prev);		
			
			$("#progress").html(Manager.i + "/" + Manager.MAX_PAGE);	
			$('#timer').text("(00:10)");			
			Manager.quizTimer();						
		}

		//Special case 26/34
		if ($("#slide26").hasClass("slide-background4")) {
			Manager.enable($(".module .next"), Manager.next);
		}
	},
	
	next: function(e) {
		var Manager = _LearningManager;
		Manager.i++; 
		ajaxpage(Manager.slide_target,"slide","slide" + Manager.i);
		$("#progress").html(Manager.i + "/" + Manager.MAX_PAGE);
		
		if (Manager.i > Manager.MAX_PAGE) {
			gotoDashBoard();
		}		
		e.stopImmediatePropagation();
	},
	
	prev: function(e) {
		var Manager = _LearningManager;
		Manager.i--; 
		ajaxpage(Manager.slide_target,"slide","slide" + Manager.i);
		$("#progress").html(Manager.i + "/" + Manager.MAX_PAGE);		
		
		Manager.disable($(".module .next"), Manager.next);
		Manager.disable($(".module .back"), Manager.prev);

		if (Manager.i == 0) { $(".progress").fadeOut(500); }		
		e.stopImmediatePropagation();		
	},
	
	enable: function(target, handler) {
		$(target).removeClass("disabled");
		$(target).on("click", handler);	
	},
	
	disable: function(target, handler) {
		$(target).addClass("disabled");
		$(target).off("click", handler);	
	},
	
	beginQuiz: function(e) {
		_this = this;
		_this.i = 1;
		ajaxpage(_this.slide_target,'slide','slide1');
		$('.progress').fadeIn();
	},
	
	quizTimer: function() {		
		var sec = 10; //Timer max
		var _this = this;
		
		clearInterval(_this.timer);
		_this.timer = setInterval(function() { 
			if (sec < 10) $('#timer').text("(00:0"+sec--+")"); else $('#timer').text("(00:"+sec--+")");
			if (sec == -1) {
				clearInterval(_this.timer);
				_this.enable($(".module .next"), _this.next);
				_this.enable($(".module .back"), _this.prev);	
			 } 
		}, 1000);		
	}	
};


function gotoDashBoard() {

	//Save Module taken and send email
	//Redirect to complete dashboard after
    var _params = {
        'url' : _LearningManager.base_url + "index.php/api/save-training?",
        'args' : {
            'regid' : _LearningManager.regid,
            'module' : _LearningManager.module
        }
    }
    $.getJSON(_params.url, _params.args,function(res){

        if (res.status == 'success') {

			var _url = _LearningManager.base_url + "index.php/dashboard/complete/" +   _LearningManager.module + "/" +   _LearningManager.username + "/" + res.keycount;
    		$('.container').load(_url);	

        } else {
            alert( res.msg );
        }       

    });

}



$(window).load(function () {
	Shadowbox.init({ language: 'en', players: ['html','iframe'] })
});

$(document).ready(function(){
	
	$.ajaxSettings.cache = false;
	var temp;

	// setup navigation combo	
	$("#main_nav").selectbox({
		classHolder: 'navigation'
	});	
	
	var step1 = $("#step1_input");
	var loader = $("#loader");
	var validated = false;	
	
	// Option Boxes
	var option1 = $("#option1");
	var option2 = $("#option2");
	var option3 = $("#option3");
	var option4 = $("#option4");
	var option5 = $("#option5");
	var option6 = $("#option6");
	var option7 = $("#option7");
	var option8 = $("#option8");
	var option9 = $("#option9");
	var option10 = $("#option10");								
	
	step1.keyup(validateStep1);
	
	function validateStep1() {

		if (step1.val().length < 8) {
			loader.html("");	
			$(".rank .input_feedback").stop().fadeOut(250);							
		} else { 

		    var _params = {
		        'url' : _PageManager.base_url + "index.php/api/validate-key-code?",
		        'args' : { 
		        	'regid' : _PageManager.regid,
		        	'keycode' : step1.val()
		         }
		    }
		    $.getJSON(_params.url, _params.args,function(res){
		    	if  (res.status == 'success') {

					loader.html("<img src='../../assets/img/img_check.png'>");
					$(".rank .input_feedback").stop().delay(250).fadeOut(250);			
					if ($(".answers_container").css('display') == 'none') $(".answers_container").stop(true, true).fadeIn({ duration: 800, queue: false }).css('display', 'none').slideDown(800); 
					validated = true;
					step1.addClass(function() {
						$(this).attr("disabled", true);
						return "disabled";	
					});		    		

		    	} else {

		    		alert(res.msg);

					$(".rank .input_feedback").html("This key is invalid. Please enter a valid key.").delay(250).fadeIn(250);		    		

		    	}
		    });		

		}
	}
	
	// Navigation dropdown fix
	$(".navigation ul li a").on("click", function() {
		var a = $(this).attr("href");
		a = a.replace("#","");
		window.location.href = a;
	});	
	
	$("input[type='text']").on("focus", function() {
		temp = $(this).val();
		$(this).val("");
	});
	
	$("input[type='text']").on("blur", function() {
		if (($(this).val() == temp) || ($(this).val() == "")) $(this).val(temp);		
	});
	
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i) ? true : false;
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i) ? true : false;
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i) ? true : false;
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
   		}
	};		
	
	if (isMobile.any()) $("body").addClass("mobile");
	if (isMobile.Android()) { $("body").addClass("android"); }	
	
});

var _PageManager = new PageManager();	

