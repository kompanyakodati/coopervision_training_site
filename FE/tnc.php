<?php include 'includes/header.php'; ?>

    <section class="tnc w960ma">
        <h2>Terms and Conditions</h2>
        <div class="w960ma clearfix">
            <ol>
                <li>Information on how to enter and prizes form part of these Terms and Conditions. Participation in this promotion is deemed acceptance of these Terms and Conditions.</li>
                <li>Entry is only open to Australian and New Zealand residents aged 18 years or over who are either: (a) registered Optometrists/practice members at participating Optometrist practices (<b>practice members</b>); or (b) sales representatives of the Promoter (<b>sales representatives</b>).</li>
                <li>Promotion commences on 3/2/2014 and entries close at 11:59pm AEST 30/4/2014 (Promotional Period).  </li>
                
                <h3>Entry (Practice Members)</h3>
                <li>To be eligible to enter, eligible practice members must register for the promotion during the Promotional Period, by visiting <a href="http://www.CooperVisionTraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a>, following the prompts to the promotion registration page, inputting the requested details and submitting the fully completed registration form. </li>
                <li>
                
                To be eligible to obtain bonus entries, registered eligible practice members must complete the online training program and/or refer colleagues to the promotion, as detailed below: 
                <ol>
                	<li>online training program: visit <a href="http://www.CooperVisionTraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a>, follow the prompts to the online training program page and input the requested details to complete and submit the online training program; and/or</li>
                    <li>refer a colleague: invite other eligible practice members to visit  <a href="http://www.CooperVisionTraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a> and register for the promotion. To send an invite, eligible practice members must submit the referred person’s name and email address (and any other required information) in the manner required and where indicated when submitting the online referral form. Referred persons must be Australian residents aged 18 years or over who are practice members at participating Optometrist practices, and eligible to enter the promotion. The operation of the referral mechanic is subject to the Spam Act 2003. An eligible practice member may only refer a particular person once during the Promotional Period and an eligible practice member cannot refer persons who have already registered for the promotion or already been referred to the promotion. Each eligible practice member warrants that each referred person is known to them personally and that they have permission to send email communications to such persons. </li>
                </ol>
                 </li>
                 <li>Eligible practice members will be emailed one (1) ‘key’ (in the form of a unique code) for completing the above steps in clauses 4 and 5, subject to the following limitations:
                 <ol>
                 	<li>maximum one (1) key awarded per eligible practice member for completing registration;</li>
                 	<li>bmaximum one (1) key awarded per eligible practice member for completing the online training program; and</li>
                 	<li>multiple keys permitted for referrals, subject to the following: (a) one (1) key awarded per eligible practice member who invites a person to register, and the person then validly registers for the promotion during the Promotional Period; and (b) each referred person’s details must be valid, submitted separately and in accordance with referral requirements.</li>                                        
                 </ol>
                 </li>
                 <li>
                 To enter the promotion, eligible practice members must then, during the Promotional Period:
                 <ol>
                 	<li>visit www.CooperVisionTraining.com.au, and follow the prompts to the entry page;</li>
<li>input an awarded ‘key’ (in the form of the unique code) in the manner required; and        </li>
<li>rank the ten (10) provided statements in order of preference, using skill and knowledge gained as a practice member.          </li>
                 </ol>
                 </li>
                 <li>8.	Multiple entries permitted per eligible practice member, subject to the following: (a) each entry must be based on a separate ‘key’ (with a unique code); (b) each entry must be submitted separately and in accordance with entry requirements; (c) the same unique code cannot be used more than once; and (d) unrecognised unique codes will be deemed invalid. </li>
                 
                 <h3>Entry (Practice Members)</h3>
                 <li>To be eligible to enter, eligible sales representatives must, during the Promotional Period:
                     <ol style="list-style-type:lower-alpha;">
                        <li>assist an eligible practice member to register for the promotion; and</li>
                        <li>ensure that the eligible practice enters the eligible sales representative’s SR Unique ID in the manner required, before submitting their registration form. </li>
                     </ol>
                 </li>
                 <li>
					Eligible sales representatives will be awarded one (1) credit for the completion of the above steps. Once an eligible sales representative has accumulated ten (10) credits during the Promotional Period, they will be emailed one (1) ‘key’ (in the form of a unique code).                  
                 </li>
                 <li>To enter the promotion, eligible sales representatives must then, during the Promotional Period:
                 	<ol>
                    	<li>visit <a href="http://www.CooperVisionTraining.com.au" target="_blank">www.CooperVisionTraining.com.au</a>, and follow the prompts to the entry page;</li>
                        <li>input an awarded ‘key’ (in the form of the unique code) in the manner required; and</li>
                        <li>rank the ten (10) provided statements in order of preference, using skill and knowledge gained during their employment as a sales representative of the Promoter. </li>
                    </ol>
                 </li>
                 <li>Multiple entries permitted per eligible sales representative, subject to the following: (a) only one (1) entry permitted per ten (10) credits accumulated during the Promotional Period; (b) each credit must be accumulated from a separate eligible practice member; (c) each entry must be based on a separate ‘key’ (with a unique code); (d) each entry must be submitted separately and in accordance with entry requirements; (e) the same unique code cannot be used more than once; and (f) unrecognised unique codes will be deemed invalid. </li>
                 
                 <h3>Winners</h3>             
                 <li>This is a game of skill and chance plays no part in determining a winner. The judging will take place at A 222/117 Old Pittwater Road, Brookvale NSW 2100 on 12/5/2014 at 1:00pm AEST. Any winners will be notified by telephone and mail. Maximum one (1) prize permitted per entrant. </li>    
                 <li>Each entry will be individually judged based on an entrant’s ranking of the ten (10) provided statements, ordered using their skill and knowledge gained as a practice member/employed as a sales representative of the Promoter.</li>
                 <li>Before the Promotional Period, the Promoter will rank the ten (10) provided statements in order of preference (<b>Final Ranking</b>). To determine any winners, the Promoter will compare each entrant’s ranking of the ten (10) provided statements with the Final Ranking. In the event an entrant’s ranking matches the Final Ranking, the entrant will be deemed a winner, subject to final verification by the Promoter and judges. </li>
                 <li>Any winners, as determined by the judges, will each win a Mini Cooper Cabrio with manual transmission, valued at up to AU$45,917.60, including registration, compulsory third party insurance, stamp duty and dealer delivery charges (which may vary State by State). Additional insurance, options, petrol and all other ancillary costs are the responsibility of a winner. A winner must collect the prize from their nearest authorised Mini Cooper dealership in Australia or New Zealand, as applicable. </li>
                 <li>A winner must have a current valid drivers licence issued in an Australian State or Territory or in New Zealand. If a winner is, through any legal incapacity or otherwise, unable to register the car in their own name, then such a winner may assign the prize to another person (who consents to such assignment) with legal capacity for the purpose of registration. The Promoter takes no responsibility for any such arrangements between the winner and the assignee. A winner must provide the Promoter with certified copies of all required documentation as required by the Promoter before the car is awarded. </li>
                 <li>As a condition of accepting a prize, a winner must sign any legal documentation as and in the form required by the Promoter and/or prize suppliers in their absolute discretion, including but not limited to a legal release and indemnity form.  </li>
                 
                 <h3>General</h3>
                 <li>The Promoter reserves the right, at any time, to verify the validity of entries and entrants (including an entrant’s identity, age and place of residence and employment) and to disqualify any entrant who submits an entry/registration that is not in accordance with these Terms and Conditions or who tampers with the entry/registration process. Errors and omissions will be accepted at the Promoter's discretion. Failure by the Promoter to enforce any of its rights at any stage does not constitute a waiver of those rights.</li>
                 <li>Incomplete, indecipherable or illegible entries will be deemed invalid.</li>
                 <li>If there is a dispute as to the identity of an entrant, the Promoter reserves the right, in its sole discretion, to determine the identity of the entrant.</li>
                 <li>The Promoter’s decision is final and no correspondence will be entered into.</li>
                 <li>If a prize (or part of a prize) is unavailable, the Promoter, in its discretion, reserves the right to substitute the prize (or that part of the prize) with a prize to the equal value and/or specification.</li>
                 <li>A prize is not transferable or exchangeable and cannot be taken as cash, unless otherwise specified. </li>
                 <li>Entrants consent to the Promoter using the entrant's name, likeness, image and/or voice in the event they are a winner (including photograph, film and/or recording of the same) in any media for an unlimited period without remuneration for the purpose of promoting this competition (including any outcome), and promoting any products manufactured, distributed and/or supplied by the Promoter.</li>
                 <li>If this promotion is interfered with in any way or is not capable of being conducted as reasonably anticipated due to any reason beyond the reasonable control of the Promoter, including but not limited to technical difficulties, unauthorised intervention or fraud, the Promoter reserves the right, in its sole discretion, to the fullest extent permitted by law: (a) to disqualify any entrant; or (b) to modify, suspend, terminate or cancel the promotion, as appropriate.</li>
					<li>Any cost associated with accessing the promotional website is the entrant’s responsibility and is dependent on the Internet service provider used. The use of any automated entry software or any other mechanical or electronic means that allows an entrant to automatically enter repeatedly is prohibited and will render all entries submitted by that entrant invalid.</li>
                    <li>Liability for any tax (other than FBT) arising out of participation in this promotion (including acceptance of a prize) is the sole responsibility of the entrant. Entrants should seek independent financial advice in this regard.</li>
                    <li>Nothing in these Terms and Conditions limit, exclude or modify or purports to limit, exclude or modify the statutory consumer guarantees as provided under the Competition and Consumer Act, as well as any other implied warranties under the ASIC Act or similar consumer protection laws in the State and Territories of Australia and New Zealand (<b>Non-Excludable Guarantees</b>). Except for any liability that cannot by law be excluded, including the Non-Excludable Guarantees, the Promoter (including its respective officers, employees and agents) excludes all liability (including negligence), for any personal injury; or any loss or damage (including loss of opportunity); whether direct, indirect, special or consequential, arising in any way out of the promotion. </li>
                    <li>30.	Except for any liability that cannot by law be excluded, including the Non-Excludable Guarantees, the Promoter (including its respective officers, employees and agents) is not responsible for and excludes all liability (including negligence), for any personal injury; or any loss or damage (including loss of opportunity); whether direct, indirect, special or consequential, arising in any way out of: (a) any technical difficulties or equipment malfunction (whether or not under the Promoter’s control); (b) any theft, unauthorised access or third party interference; (c) any entry or prize claim that is late, lost, altered, damaged or misdirected (whether or not after their receipt by the Promoter) due to any reason beyond the reasonable control of the Promoter; (d) any variation in prize value to that stated in these Terms and Conditions; (e) any tax liability incurred by a winner or entrant; or (f) use of a prize.</li>
                    <li>31.	The Promoter collects personal information (PI) in order to conduct the promotion and may, for this purpose, disclose PI to third parties, including but not limited to agents, contractors, service providers and prize suppliers. Entry is conditional on providing this PI. The Promoter will also use and handle PI as set out in its Privacy Policy, which can be viewed <a href="http://coopervision.net.au/privacy-policy-pdf" target="_blank">http://coopervision.net.au/privacy-policy-pdf</a>. In addition to any use that may be outlined in the Promoter’s Privacy Policy, the Promoter may, for an indefinite period, unless otherwise advised, use the PI for promotional, marketing, publicity, research and profiling purposes, including sending electronic messages or telephoning the entrant. The Privacy Policy also contains information about how entrants may opt out, access, update or correct their PI, how entrants may complain about a breach of the Australian Privacy Principles or any other applicable law and how those complaints will be dealt with.  All entries become the property of the Promoter. Unless otherwise indicated by the Promoter, the Promoter may disclose personal information to entities outside of Australia.</li>
                    <li>The Promoter is CooperVision Australia Pty Ltd (ABN 12 060 200 553) of Suite 3.01, 26 Rodborough Road, Frenchs Forest NSW 2086. </li>
            </ol>


        </div>
    </section>

<?php include 'includes/footer.php'; ?>

<script>
</script>