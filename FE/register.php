<?php include 'includes/header.php'; ?>

            <section class="register content w960ma">

				<h2>Welcome to the Super Cooper website</h2>
                <h3>Please complete the details below to register. If you have already registered, please login <a href="login.php">HERE</a>.</h3>
                
                <div class="w960ma clearfix">
                    <form>
                    	<div class="clearfix">
                            <section id="personal">
                                <h3>Personal Details</h3>
                                <input type="text" value="First Name" id="first_name" />
                                <label for="first_name" class="asterix">*</label>
                                <input type="text" value="Last Name"/>
                                <label for="last_name" class="asterix">*</label>
                                <input type="text" value="Email Address" />
                                <label for="email" class="asterix">*</label>
                                <select id="role">
                                    <option>Job Role</option>
                                    <option>Optometrist</option>                                
                                    <option>Optical Dispenser</option>
                                    <option>Practice Staff</option>                                
                                </select>
                                <label for="role" class="asterix jq-replace-ast">*</label>     
                                <input type="checkbox" value="Are you the business owner"> Are you the owner of the practice</input>
                            </section>
                            <section id="practice">
                                <h3>Practice Details</h3>       
                                <input type="text" value="Practice Name" id="practice_name" />
                                <label for="practice_name" class="asterix">*</label>
                                <input type="text" value="Practice Account Number" id="practice_account_number" />
                                <label for="practice_account_number" class="asterix">*</label>                                
                                <select id="state">
                                    <option>State</option>
                                </select>     
                                <label for="state" class="asterix jq-replace-ast">*</label>                                
                                <input type="text" value="Postcode" id="post_code" />                                        
                                <label for="post_code" class="asterix">*</label>                                
                                <input type="text" value="Cooper Vision Rep Number" od="rep_number" />                                                                         
                                <label for="rep_number" class="asterix">*</label>                                
                            </section>  
                        </div>                      
	                    <br />
                        <br />
		                <p class="errorfeedback">* These are required fields.</p>
	                    <div class="center"><input type="image" src="img/btn_register.png" onClick="javascript:openVideo()"></div>                        
                    </form>
                </div>
            </section>

<?php include 'includes/footer.php'; ?>

<script>
		$(function () {
			$(".register form select").selectbox();
		});
</script>