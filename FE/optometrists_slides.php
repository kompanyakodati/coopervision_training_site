<div id="slide0" class="slide-content slide">
    <br />
    <br />
    <br />
    <h2 class="headline">
        Focus on MyDay and you could <span>Win a Trip to Las Vegas!</span>
    </h2>	
    <p>In planning for the development of MyDay, the CooperVision team identified that no daily disposable offered excellent long-lasting comfort, high levels of breathability and easy handling.</p>
    <br />
	<input type="image" src="img/btn_begin_quiz.png" onclick="_LearningManager.beginQuiz()" />
</div>

<div id="slide1" class="slide-content slide-background1 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <p>Hydrogel daily disposables can offer higher water contents, which soften the modulus and help comfort, but their low Dk values compromise oxygen flow to the eye. </p>
	<p>Silicone hydrogel daily disposables provide much better levels of oxygen transmissibility but tend to have lower water contents and higher modulus values which can affect comfort.  </p>
	<br />
</div>

<div id="slide2" class="slide-content slide-background1 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="1"><li>What are the three key attributes of lens performance targeted by the <br />CooperVision R&D team in developing MyDay?</li></ol>
    <div class="questions">
        <p><input id="1" type="radio" value="Comfort, vision and handling" name="group1" /><label for="option1">&nbsp;&nbsp;Comfort, vision and handling</label></p>
        <p><input id="2" type="radio" value="Comfort, breathability and handling" name="group1" /><label for="option2">&nbsp;&nbsp;Comfort, breathability and handling</label></p>
        <p><input id="3" type="radio" value="Comfort, breathability and UV inhibition" name="group1" /><label for="option3">&nbsp;&nbsp;Comfort, breathability and UV inhibition</label></p>
        <p><input id="4" type="radio" value="Comfort, vision and UV inhibition" name="group1" /><label for="option4">&nbsp;&nbsp;Comfort, vision and UV inhibition</label></p>
    </div>
	<div id="quiz_response">
		<img src="img/img_cross.png" /> This is a test message.
	</div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
</div>

<div id="slide3" class="slide-content slide-background1 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="2"><li>What is one reason silicone hydrogel materials may be less comfortable than hydrogels?</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Poor edge design" name="group1" /><label for="option1">&nbsp;&nbsp;Poor edge design</label></p>
        <p><input id="2" type="radio" value="Lack of UV protection" name="group1" /><label for="option2">&nbsp;&nbsp;Lack of UV protection</label></p>
        <p><input id="3" type="radio" value="Higher modulus values" name="group1" /><label for="option3">&nbsp;&nbsp;Higher modulus values</label></p>
        <p><input id="4" type="radio" value="Larger lens diameter" name="group1" /><label for="option4">&nbsp;&nbsp;Larger lens diameter</label></p>
    </div>
	<div id="quiz_response">
		<img src="img/img_check.png" /> This is a test message.
	</div>	
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>

<div id="slide4" class="slide-content slide-background2 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br /> 
	<p>MyDay uses a unique material chemistry called Smart Silicone™.</p>
	<p>This refers to the shaping of silicone into a network of channels that allow for more efficient transport of oxygen through the lens material.  </p>
</div>

<div id="slide5" class="slide-content slide-background2 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br /> 
	<p>Because of this, less raw silicon is needed in the material to achieve the desired oxygen permeability and this in turn allows more room for hydrophilic groups.  </p>
	<p>These hydrophilic groups help provide better wettability and a lower modulus. </p>
    <p>In short, this means a material, stenfilcon A, which offers the best of both the hydrogel and silicone hydrogel worlds.</p>
</div>

<div id="slide6" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="3"><li>What is the name of the unique material chemistry used in MyDay?</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Smart Silicone" name="group1" /><label for="option1">&nbsp;&nbsp;Smart Silicone</label></p>
        <p><input id="2" type="radio" value="Smarter Silicone" name="group1" /><label for="option2">&nbsp;&nbsp;Smarter Silicone</label></p>
        <p><input id="3" type="radio" value="Hydrophilic Silicone" name="group1" /><label for="option3">&nbsp;&nbsp;Hydrophilic Silicone</label></p>
        <p><input id="4" type="radio" value="Raw Silicon" name="group1" /><label for="option4">&nbsp;&nbsp;Raw Silicon</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide7" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="4"><li>What is the name of the MyDay material?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="enfilcon A" name="group1" /><label for="option1">&nbsp;&nbsp;enfilcon A</label></p>
        <p><input id="2" type="radio" value="comfilcon A" name="group1" /><label for="option2">&nbsp;&nbsp;comfilcon A</label></p>
        <p><input id="3" type="radio" value="omafilcon A" name="group1" /><label for="option3">&nbsp;&nbsp;omafilcon A</label></p>
        <p><input id="4" type="radio" value="stenfilcon A " name="group1" /><label for="option4">&nbsp;&nbsp;stenfilcon A </label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide8" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="5"><li>What properties do the hydrophilic groups within the MyDay material confer?</li></ol> 
    
    <div class="questions">    
        <p><input id="1" type="radio" value="Better handling and UV protection" name="group1" /><label for="option1">&nbsp;&nbsp;Better handling and UV protection</label></p>
        <p><input id="2" type="radio" value="Better wettability and a lower modulus" name="group1" /><label for="option2">&nbsp;&nbsp;Better wettability and a lower modulus</label></p>
        <p><input id="3" type="radio" value="Better wettability and high Dk" name="group1" /><label for="option3">&nbsp;&nbsp;Better wettability and high Dk</label></p>
        <p><input id="4" type="radio" value="High Dk and a lower modulus" name="group1" /><label for="option4">&nbsp;&nbsp;High Dk and a lower modulus</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide9" class="slide-content slide-background3 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br /> 
	<p>Because of the properties of the MyDay material, a lens can be produced that has no inherent compromises.  The material has a high Dk of 80, giving a Dk/t of 100 at the centre of a -3.00D lens.  A water content of 54% helps create softness and wettability.  </p>
	<p>Like other CooperVision silicone hydrogel lenses, the surfaces are naturally wettable without the need for coatings or additives.  These factors all assist with creating long-lasting comfort.  </p>
</div>

<div id="slide10" class="slide-content slide-background3 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br /> 
	<p>The modulus is only 0.4MPa meaning the lens is soft and flexible yet the lens handles very easily.  MyDay is being launched with a comprehensive power range of +6.00 to -10.00D and a versatile single-fit 8.40/14.20 design.  </p>
	<p>The lens also features a UV inhibitor.  The optical design includes CooperVision’s Aberration Neutralising System™ to reduce the effects of spherical aberration arising from both the lens and the eye. </p>
    <p>A rounded edge design and optimised back surface combine to create a lens that successfully fits a very wide range of corneas.</p>
</div>

<div id="slide11" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="6"><li>What is the Dk of the MyDay lens material?</li></ol> 
    
    <div class="questions">    
        <p><input id="1" type="radio" value="70" name="group1" /><label for="option1">&nbsp;&nbsp;70</label></p>
        <p><input id="2" type="radio" value="80" name="group1" /><label for="option2">&nbsp;&nbsp;80</label></p>
        <p><input id="3" type="radio" value="87" name="group1" /><label for="option3">&nbsp;&nbsp;87</label></p>
        <p><input id="4" type="radio" value="100" name="group1" /><label for="option4">&nbsp;&nbsp;100</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide12" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="7"><li>What is the water content of the MyDay lens material?</li></ol> 
    
    <div class="questions">    
        <p><input id="1" type="radio" value="46%" name="group1" /><label for="option1">&nbsp;&nbsp;46%</label></p>
        <p><input id="2" type="radio" value="48%" name="group1" /><label for="option2">&nbsp;&nbsp;48%</label></p>
        <p><input id="3" type="radio" value="54%" name="group1" /><label for="option3">&nbsp;&nbsp;54%</label></p>
        <p><input id="4" type="radio" value="60%" name="group1" /><label for="option4">&nbsp;&nbsp;60%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide13" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="8"><li>What is the power range available at launch?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="-0.25 to -10.00D" name="group1" /><label for="option1">&nbsp;&nbsp;-0.25 to -10.00D</label></p>
        <p><input id="2" type="radio" value="+6.00 to -8.00D" name="group1" /><label for="option2">&nbsp;&nbsp;+6.00 to -8.00D</label></p>
        <p><input id="3" type="radio" value="+6.00 to -12.00D" name="group1" /><label for="option3">&nbsp;&nbsp;+6.00 to -12.00D</label></p>
        <p><input id="4" type="radio" value="+6.00 to -10.00D" name="group1" /><label for="option4">&nbsp;&nbsp;+6.00 to -10.00D</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide14" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="8"><li>Does MyDay incorporate an UV ihibitor</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="Yes" name="group1" /><label for="option1">&nbsp;&nbsp;Yes</label></p>
        <p><input id="2" type="radio" value="No" name="group1" /><label for="option2">&nbsp;&nbsp;No</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />	
</div>

<div id="slide15" class="slide-content slide">
        
	<p>MyDay lenses have been subjected to in-depth comparison studies against leading alternatives that seek to mimic real-world prescribing and wearing conditions.  </p>
	<p>In one study of 100 patients carried out across six optometry practices in the UK, MyDay and 1 Day Acuvue® Moist® lenses were compared in a two-week, randomised and masked cross-over study.  Some of the key findings are shown in the following graphs of patient preferences at the end of the study.</p>
	<img src="img/img_slidecontent1.png" class="center" />
    <br />
</div>

<div id="slide16" class="slide-content slide">
	<p>In a second study of 57 patients carried out at two research centres in the USA, MyDay and 1 Day Acuvue® TruEye® lenses were compared in a one-week, randomised and asked cross-over study.  Some of the key findings are shown in these graphs:</p>
	<img src="img/img_slidecontent2.png" class="center" />    
	<p>MyDay clearly performs very well by comparison to these market-leading options and therefore represents an outstanding new choice for practitioners and patients.</p>
    <br />
</div>

<div id="slide17" class="slide-content slide-background4 question">
        
    <br />
    <br />
    <ol start="9"><li>What proportion of patients preferred MyDay for comfort when compared with 1 Day Acuvue Moist?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="50%" name="group1" /><label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="68%" name="group1" /><label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="70%" name="group1" /><label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="83%" name="group1" /><label for="option4">&nbsp;&nbsp;83%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		        
</div>

<div id="slide18" class="slide-content slide-background4 question">
        
    <br />
    <br />
    <ol start="10"><li>What proportion of patients preferred MyDay overall when compared with 1 Day Acuvue Moist?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="50%" name="group1" /><label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="68%" name="group1" /><label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="70%" name="group1" /><label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="83%" name="group1" /><label for="option4">&nbsp;&nbsp;83%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		        
</div>

<div id="slide19" class="slide-content slide-background4 question">
        
    <br />
    <br />
    <ol start="11"><li>What proportion of patients preferred MyDay for comfort when compared with 1 Day Acuvue Moist?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="50%" name="group1" /><label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="68%" name="group1" /><label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="70%" name="group1" /><label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="83%" name="group1" /><label for="option4">&nbsp;&nbsp;83%</label></p>
    </div>
    
</div>

<div id="slide20" class="slide-content slide-background4 question">
        
    <br />
    <br />
    <ol start="12"><li>What proportion of patients preferred MyDay overall when compared with 1 Day Acuvue Moist?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="50%" name="group1" /><label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="68%" name="group1" /><label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="70%" name="group1" /><label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="83%" name="group1" /><label for="option4">&nbsp;&nbsp;83%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		        
</div>

<div id="slide21" class="slide-content slide-background4 question">
        
    <br />
    <br />
    <ol start="13"><li>What proportion of patients preferred 1 Day Acuvue TruEye when compared with MyDay?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="14%" name="group1" /><label for="option1">&nbsp;&nbsp;14%</label></p>
        <p><input id="2" type="radio" value="28%" name="group1" /><label for="option2">&nbsp;&nbsp;28%</label></p>
        <p><input id="3" type="radio" value="58%" name="group1" /><label for="option3">&nbsp;&nbsp;58%</label></p>
        <p><input id="4" type="radio" value="70%" name="group1" /><label for="option4">&nbsp;&nbsp;70%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		        
</div>

<div id="slide22" class="slide-content slide-background4 question">
        
    <br />
    <br />
    <ol start="14"><li>Was the level of end of day comfort for MyDay statistically significantly greater than that for 1 Day Acuvue TruEye?</li></ol> 
    <div class="questions">    
        <p><input id="1" type="radio" value="Yes" name="group1" /><label for="option1">&nbsp;&nbsp;Yes</label></p>
        <p><input id="2" type="radio" value="No" name="group1" /><label for="option2">&nbsp;&nbsp;No</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		        
</div>