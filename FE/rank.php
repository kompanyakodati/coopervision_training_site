
<?php include 'includes/header.php' ?>

<script>
	var selectedOptions = [];
</script>

        <section class="rank w960ma">
        
            <h2>Rank to Win Game</h2>
            <div class="clearfix" style="padding-bottom:20px;">
                <article class='intro center'>
                    <h3>Rank the top 10 attributes of CooperVision Contact Lenses 
                    (in order of importance for helping you build a <br />profitable and sustainable business) 
                    by selecting the right order from 1-10, with 1 being the most important.</h3>
                    <h3>If you match the correct order, you will win a <b>Trip to Vegas</b>! It’s that easy!</h3>
                </article>
                
                <article id="step1" class="clearfix">
                    <h3>Step 1: Verify your code</h3>
                    <div class="step1">
                        <label for="step1_input">Enter Your Key Code Here</label>
                        <input id="step1_input" maxlength="10"></input>
	                    <div id="loader"></div>                        
                    </div>
	                <span class="input_feedback error"></span>                    
                </article>

            </div>
        </section>

		
		<div class="answers_container" id="step2">
			<section class="answers">
				<h3>Step 2: Choose your answers</h3>
				<p> 
					What are the top 10 attributes of MyDay&#153; Contact Lenses for the best patient experience?
					<span>Rearange the options from 1-10, with 1 being the most important</span>
				</p>
				<form>
					<ul id="answers">
						<li class="clearfix">
							<label for="option1">1.</label>
							<select id="option1"></select>							
						</li>
						<li class="clearfix">
							<label for="option2">2.</label>
							<select id="option2"></select>							
						</li>
						<li class="clearfix">
							<label for="option3">3.</label>
							<select id="option3"></select>							
						</li>
						<li class="clearfix">
							<label for="option4">4.</label>
							<select id="option4"></select>							
						</li>						
						<li class="clearfix">
							<label for="option5">5.</label>
							<select id="option5"></select>							
						</li>	
						<li class="clearfix">
							<label for="option6">6.</label>
							<select id="option6"></select>							
						</li>
						<li class="clearfix">
							<label for="option7">7.</label>
							<select id="option7"></select>							
						</li>						
						<li class="clearfix">
							<label for="option8">8.</label>
							<select id="option8"></select>							
						</li>	
						<li class="clearfix">
							<label for="option9">9.</label>
							<select id="option9"></select>							
						</li>	
						<li class="clearfix">
							<label for="option10">10.</label>
							<select id="option10"></select>							
						</li>							
					</ul>
					
					<input type="image" id="submit" value="Submit" src="img/btn_submit.png" onClick="storeEntries();return false;" />
				</form>
			</section>
		</div>
		
		
		<?php include 'includes/footer.php' ?>


		<script>
		
		var _values = [];
		var _valueCheckPass;
		_PageManager.setPage("RANK");		
			
		$(function () {
			
			var _GameMaster = new GameMaster();		
			_GameMaster.init();
						
			for (var i = 1; i < 11; i++) { 
				$("#option"+i).selectbox({
					onChange: function(e) {
						_GameMaster.update($(this), e);
					}
				}); 
			};
			$("#answers").find(".sbHolder .sbSelector").each(function(e) {
				if ($(this).html() == "SELECT AN OPTION") {
				$(this).addClass("default");

				}
			});
			
			var $input = $('#step1 input');
			$input.keyup(function(e) {
				var max = 10;
				if ($input.val().length > max) {
					$input.val($input.val().substr(0, max));
				}
			});			
			
		});
		
		function storeEntries() {
			$("#answers .sbSelector").each(function() {	_values.push($(this).html()); });
			
			for (var i = 0; i < _values.length; i++) {
				if (_values[i] == "SELECT AN OPTION") _valueCheckPass = false; else { _valueCheckPass = true; }
			}
			
			if (_valueCheckPass) alert("process"); else alert("failed");
		}		
        </script>
