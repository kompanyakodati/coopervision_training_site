<?php include 'includes/header.php'; ?>

            <section class="refer w960ma">

				<h2>Refer a Colleague</h2>
                <h3>Refer a colleague to the Super Cooper program to give you more chances to win!  Each time one of your <br />referred colleagues registers, you will be sent another key code.  
				<br /><br />There are no limits ... the more colleagues you refer, the greater your chances are of winning!</h3>
                <br />
                <div class="w960ma clearfix">
                    <form>
						<div class="clearfix">
                            <section id="firstname">
                                <input type="text" value="First Name" />
                                <input type="text" value="First Name" />
                                <input type="text" value="First Name" />
                                <input type="text" value="First Name" />
                                <input type="text" value="First Name" />
                            </section>
                            <section id="lastname">
                                <input type="text" value="Last Name" />
                                <input type="text" value="Last Name" />
                                <input type="text" value="Last Name" />
                                <input type="text" value="Last Name" />
                                <input type="text" value="Last Name" />
                            </section>                            
                            <section id="email">
                                <input type="text" value="Email" id="email1" />
			                    <label for="email1" class="asterix">*</label>                                
                                <input type="text" value="Email" id="email2" />
			                    <label for="email2" class="asterix">*</label>                                                                
                                <input type="text" value="Email" id="email3" />
			                    <label for="email3" class="asterix">*</label>                                                                
                                <input type="text" value="Email" id="email4" />
			                    <label for="email4" class="asterix">*</label>                                                                
                                <input type="text" value="Email" id="email5" />                          
			                    <label for="email5" class="asterix">*</label>                                                                
                            </section>  
                   		</div>
                        <a href="javascript:addField();" class="addfield">[+] Add more fields</a>
	                    <br />
                        <br />
		                <p class="errorfeedback">* Please ensure that these are valid email addresses.</p>                        
                        <br />
                        <br />                        
	                    <div class="center"><input type="image" src="img/btn_submit.png"></div>                        
                    </form>
                </div>
                
            </section>

<?php include 'includes/footer.php'; ?>
<script>
	_PageManager.setPage("REFER");
	
function addField() {
	$("#firstname").append("<input type='text' value='First Name' />");
	$("#lastname").append("<input type='text' value='Last Name' />");
	$("#email").append("<input type='text' value='Email' />");		
}
</script>