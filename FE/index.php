<?php include 'includes/header.php'; ?>

    <section class="home w960ma center">
        <img src="img/img_visionsVegas.png" alt="Vegas Hero Image" />
        
        <h2 class="headline">
            Focus on MyDay™ and you could <span>WIN a trip to Vision Expo West, Las Vegas!</span>
        </h2>

        <div class="content-container clearfix">
            <br />
            <input type="image" src="img/btn_register.png" onclick="window.location='register.php';return false;" class="mr-b">
            <input type="image" src="img/btn_how2p_video.png" onClick="javascript:openVideo();">
        </div>
    </section>

<?php include 'includes/footer.php'; ?>

<script>
	$(window).load(function () {			
		openVideo();
	});
</script>