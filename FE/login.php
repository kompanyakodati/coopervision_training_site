<?php include 'includes/header.php'; ?>

    <section class="login-form w960ma clearfix">

            <form>
		        <div class="form">            
					<h2>Already Registered?</h2>                
                    <input type="text" value="First Name" id="first_name" />
                    <label for="first_name" class="asterix">*</label>
                    <input type="text" value="Last Name" id="last_name"/>
                    <label for="last_name" class="asterix">*</label>
                    <input type="text" value="Email" id="email" />
                    <label for="email" class="asterix">*</label>
                </div>
                <p class="errorfeedback">* These are required fields.</p>
                <div class="btn_login">
					<input type="image" src="img/btn_login.png" onclick="window.location='dashboard.php?user=true&first=1';return false;">                                   
                </div>
            </form>
        

    </section>

<?php include 'includes/footer.php'; ?>