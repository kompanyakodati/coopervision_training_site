<?php include 'includes/header.php'; ?>

    <section class="response w960ma">
        <h2 class="response">Thank you for your submission.</h2>
        <div class="w960ma clearfix">
				
                <p class="center">Thank you for your participation. You will be informed if you win.</p>
                <!-- input other items here -->

        </div>
    </section>

<?php include 'includes/footer.php'; ?>

<script>
	_PageManager.setPage("HOW2P");
</script>