<?php include 'includes/header.php'; ?>

    <section class="module w960ma">
        
        <section id="slide">
        </section>
        
        <section class="progress clearfix">
        	<p>Your Progress: <span id="progress">1/22</span></p>
            <p>(00:<span id="timer">10</span>)</p>
            <div class="buttons">
                <input type="image" src="img/1x1.png" class="mr-sb back" text="">
                <input type="image" src="img/1x1.png" class="next">
            </div>
        </section>
        
        
    </section>

<?php include 'includes/footer.php'; ?>
<script>

	var _LearningManager = new LearningManager();
	
	_PageManager.setPage("LEARNING");
	//_LearningManager.setRole("PRACTICE_STAFF");
	_LearningManager.setRole("OPTOMETRISTS");

	_LearningManager.init();
	
</script>