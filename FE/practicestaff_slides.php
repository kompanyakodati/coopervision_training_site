<div id="slide0" class="slide-content slide">
    <br />
    <br />
    <br />
    <h2 class="headline">
        Focus on MyDay and you could <span>Win a Trip to Las Vegas!</span>
    </h2>	
    <p>In planning for the development of MyDay, the CooperVision team identified that no daily disposable offered excellent long-lasting comfort, high levels of breathability and easy handling.</p>
    <br />
	<input type="image" src="img/btn_begin_quiz.png" onclick="_LearningManager.beginQuiz()" />
</div>

<div id="slide1" class="slide-content slide-background1 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span>CONTACT LENS FACTS</span></h2>
    <br />
    <br />
	<p class="center"><b>Fact:</b> Half of the Australian and New Zealand population needs vision correction.</p>
</div>

<div id="slide2" class="slide-content slide-background1 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="1"><li>Out of those who need vision correct, what percentage wear contact lenses?</li></ol>
    <div class="questions">
        <p><input id="1" type="radio" value="90%" name="group1" /><label for="option1">&nbsp;&nbsp;90%</label></p>
        <p><input id="2" type="radio" value="8%" name="group1" /><label for="option2">&nbsp;&nbsp;8%</label></p>
        <p><input id="3" type="radio" value="25%" name="group1" /><label for="option3">&nbsp;&nbsp;25%</label></p>
        <p><input id="4" type="radio" value="50%" name="group1" /><label for="option4">&nbsp;&nbsp;50%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
</div>

<div id="slide3" class="slide-content slide-background1 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="2"><li>What percentage of people are generally suitable for contact lenses?</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="90%" name="group1" /><label for="option1">&nbsp;&nbsp;90%</label></p>
        <p><input id="2" type="radio" value="8%" name="group1" /><label for="option2">&nbsp;&nbsp;8%</label></p>
        <p><input id="3" type="radio" value="25%" name="group1" /><label for="option3">&nbsp;&nbsp;25%</label></p>
        <p><input id="4" type="radio" value="50%" name="group1" /><label for="option4">&nbsp;&nbsp;50%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>

<div id="slide4" class="slide-content slide-background2 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span style="font-size:48px;">HOW TO TALK ABOUT CONTACT LENSES</span></h2>
    <br />
    <br />
	<p class="center"><b>Fact:</b> Most people don’t wear contact lenses because they haven’t been offered the option. Let’s find out what the best way to offer contact lenses is.</p>
</div>

<div id="slide5" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="3"><li>When is the best time to introduce contact lenses to optometry patients?</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="When booking an appointment with the optometrist, ask if the appointment is for glasses or glasses and contact lenses" name="group1" /><label for="option1">
        &nbsp;&nbsp;When booking an appointment with the optometrist, ask if the appointment is for glasses or glasses <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and contact lenses</label></p>
        
        <p><input id="2" type="radio" value="While they are waiting for their appointment" name="group1" /><label for="option2">
        &nbsp;&nbsp;While they are waiting for their appointment</label></p>
        
        <p><input id="3" type="radio" value="While dispensing their glasses – You can let them know that this power is also available in contact lenses" name="group1" /><label for="option3">&nbsp;&nbsp;
        While dispensing their glasses – You can let them know that this power is also available in contact lenses</label></p>
        
        <p><input id="4" type="radio" value="All of the above" name="group1" /><label for="option4">&nbsp;&nbsp;&nbsp;All of the above</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>

<div id="slide6" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="4"><li>If all those times are a good opportunity to talk about contact lenses, how do you start the contact lens conversation</li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="By talking about the weather" name="group1" /><label for="option1">
        &nbsp;&nbsp;By talking about the weather</label></p>
        <p><input id="2" type="radio" value="By talking about lifestyle" name="group1" /><label for="option2">
        &nbsp;&nbsp;By talking about lifestyle</label></p>
        <p><input id="3" type="radio" value="By talking about pets" name="group1" /><label for="option3">&nbsp;&nbsp;
        By talking about pets</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>

<div id="slide7" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="5"><li>
    	What lifestyle questions can you ask patients prior to their appointment to identify whether contact lenses may be suitable?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Are there ever times you wish you didn’t have to wear your glasses?" name="group1" />
        <label for="option1">&nbsp;&nbsp;Are there ever times you wish you didn’t have to wear your glasses?</label></p>
        <p><input id="2" type="radio" value="Do you play any sports?" name="group1" />
        <label for="option2">&nbsp;&nbsp;Do you play any sports?</label></p>
        <p><input id="3" type="radio" value="Do you always wear the same clothes every time you go out?" name="group1" />
        <label for="option3">&nbsp;&nbsp;Do you always wear the same clothes every time you go out?</label></p>
        <p><input id="4" type="radio" value="All of the above" name="group1" />
        <label for="option4">&nbsp;&nbsp;All of the above</label></p>        
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>

<div id="slide8" class="slide-content slide-background2 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="6"><li>
    	What follow on questions can be asked to find out what kind of contact lenses may be suitable?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="How often do you play sports?" name="group1" />
        <label for="option1">&nbsp;&nbsp;How often do you play sports?</label></p>
        <p><input id="2" type="radio" value="When do you wish you didn’t have to wear your glasses?" name="group1" />
        <label for="option2">&nbsp;&nbsp;When do you wish you didn’t have to wear your glasses?</label></p>
        <p><input id="3" type="radio" value="All of the above" name="group1" />
        <label for="option3">&nbsp;&nbsp;All of the above</label></p>        
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />	
    <p class="remarks">Contact lenses are available as 2-weekly and monthly replacement; these are good for patients who want to wear their contact lenses everyday. For patients that wish to wear their lenses part time or occasionally, 1 day lenses are also available.</p>	    
    <br />		
</div>

<div id="slide9" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="7"><li>
    	What are the primary motivators to wear contact lenses (take from Neal’s survey)
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Self confidence" name="group1" />
        <label for="option1">&nbsp;&nbsp;Self confidence</label></p>
        <p><input id="2" type="radio" value="Lifestyle" name="group1" />
        <label for="option2">&nbsp;&nbsp;Lifestyle</label></p>
        <p><input id="3" type="radio" value="Special occasions" name="group1" />
        <label for="option3">&nbsp;&nbsp;Special occasions</label></p>             
		<p><input id="4" type="radio" value="All of the above" name="group1" />
        <label for="option3">&nbsp;&nbsp;All of the above</label></p>                
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />	
    <p class="remarks">There are lots of reasons why people choose to wear contact lenses. Don’t hesitate to ask about their motivation.</p>	    
    <br />		
</div>

<div id="slide10" class="slide-content slide-background4 slide">

    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span>MYTH QUESTIONS</span></h2>
    <br />
    <br />
	<p class="center">Can you pick which are these are facts and what is myth?</p>
</div>

<div id="slide11" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="8"><li>Can contact lenses get lost in the back of my eye</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">No it is not physically possible. There is a membrane that prevents anything going into the back of your eyes </p>	          
    <br />	
</div>

<div id="slide12" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="9"><li>Make-up and contact lenses: Must I give up one for the other? </li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
	No, you can wear both, you just need to respect certain rules; contact lenses first, make up second
    </p>	          
    <br />	
</div>

<div id="slide13" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="10"><li>Contact lenses are uncomfortable</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
		Contact lenses are very comfortable, try one for yourself.
    </p>	          
    <br />		
</div>

<div id="slide14" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="11"><li>Contact lenses can get stuck to my eye</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
		Optometrists will be able to teach you how to remove contact lenses safely in any situation
    </p>	          
    <br />		
</div>

<div id="slide15" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="12"><li>Contact lenses are hard work to take care of</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
		There are now multi-purpose solutions that are very easy to use, and there is also the option of 1 day lenses that you throw away at the end of each day.
    </p>	          
    <br />		
</div>

<div id="slide16" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="13"><li>Contact lenses are hard work to take care of</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
		There are now multi-purpose solutions that are very easy to use, and there is also the option of 1 day lenses that you throw away at the end of each day.
    </p>	          
    <br />	
</div>

<div id="slide17" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="14"><li>Contact lenses are only suitable for young people</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
		They are actually suitable for everyone, even presbyopes – those who are over 40 can now wear multifocal contact lenses
    </p>	          
    <br />	        
</div>

<div id="slide18" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="15"><li>Contact lenses are too expensive</li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
Contact lenses can cost as little as $1 per day
    </p>	          
    <br />	               
</div>

<div id="slide19" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />    
    <br />        
    <ol start="16"><li>
My friends, aunties, cousin friend tried them and they didn’t work    
    </li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
Every patient is different and there are now all sorts of contact lenses for different patients needs
    </p>	          
    <br />	     
    
</div>

<div id="slide20" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="17"><li>
I’ve heard contact lenses are made of glass that you put on your eye?
    </li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
We recommend that you open a 1 day contact lens blister and make you patient touch the lens, so they realise how soft today’s contact lenses are
    </p>	          
    <br />	    
         
</div>

<div id="slide21" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="18"><li>
My mother tried them twenty years ago and they didn’t work?
    </li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
There is now new technology in contact lenses, we recommend they come in and try them again
    </p>	          
    <br />	    
    
</div>

<div id="slide22" class="slide-content slide-background4 question">
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="19"><li>
I’ve heard it’s not healthy for my eye?
    </li></ol> 
    <div class="true_false_buttons center">
        <input type="image" src="img/btn_true.png" />	
        <input type="image" src="img/btn_false.png" />	    	
	</div>
    <p class="remarks">
Contact lenses have new material, made of silicone hydrogel, that is much more breathable and allow almost 100% oxygen to reach their eyes
    </p>	          
    <br />	   
            
</div>

<div id="slide23" class="slide-content slide-background1 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline">Did you know contact lenses can cost as little as $1 per day – <br />That’s less than your daily cup of coffee! </h2>
    <br />
    <br />
</div>

<div id="slide24" class="slide-content slide-background1 question">

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="20"><li>
If a patient asks about the cost, how do you respond?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Well it’s quite complicated, it depends what that modality is, there are lots of options. You would need to speak to the optometrists, I can’t tell you." name="group1" />
        <label for="option1">&nbsp;&nbsp;Well it’s quite complicated, it depends what that modality is, there are lots of options. You would need to <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;speak to the optometrists, I can’t tell you.</label></p>
        <p><input id="2" type="radio" value="If it’s a monthly, a pack of 6 is $150, but if it is a toric, it will be $200, but if it’s a multifocal it’s also $200. But if you want a 1 day, a pack of 30 will cost about $50, but if you need toric or multifocal it will be about $70" name="group1" />
        <label for="option2">&nbsp;&nbsp;If it’s a monthly, a pack of 6 is $150, but if it is a toric, it will be $200, but if it’s a multifocal it’s also $200. <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;But if you want a 1 day, a pack of 30 will cost about $50, but if you need toric or multifocal it will be about <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$70</label></p>
        <p><input id="3" type="radio" value="Do you always wear the same clothes every time you go out?" name="group1" />
        <label for="option3">&nbsp;&nbsp;It varies but most patient pay between $1-3 per day, which is less than your daily cup of coffee</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>


<div id="slide25" class="slide-content slide-background question1">
	<br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="21"><li>
What should I do next, once I’ve had the conversation with the patient about contact lenses?nd?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Go into the fitting sets and give them a free trial" name="group1" />
        <label for="option1">&nbsp;&nbsp;Go into the fitting sets and give them a free trial</label></p>
        <p><input id="2" type="radio" value="Let the optometrist know that we have had this conversation and the patient is interested in knowing more about contact lenses" name="group1" />
        <label for="option2">&nbsp;&nbsp;Let the optometrist know that we have had this conversation and the patient is interested in knowing more <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;about contact lenses</label></p>
        <p><input id="3" type="radio" value="Don’t say anything as I already have enough work" name="group1" />
        <label for="option3">&nbsp;&nbsp;Don’t say anything as I already have enough work</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		    
    <br />		
</div>

<div id="slide26" class="slide-content slide-background4 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><span>General Knowledge Questions</span></h2>
    <br />
    <br />
</div>

<div id="slide27" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="22"><li>
What is the ideal lens for someone who wants to wear their lenses occasionally?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Daily" name="group1" />
        <label for="option1">&nbsp;&nbsp;Daily</label></p>
        <p><input id="2" type="radio" value="2-weekly" name="group1" />
        <label for="option2">&nbsp;&nbsp;2-weekly</label></p>
        <p><input id="3" type="radio" value="Monthly" name="group1" />
        <label for="option3">&nbsp;&nbsp;Monthly</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <p class="remarks">1 day contact lenses are an affordable options to wear occasionally or daily in some circumstances.</p>	           
    <br />		
</div>

<div id="slide28" class="slide-content slide-background3 slide">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <br />
    <br />
    <h2 class="headline"><b>Fact:</b> Did you know there are two types of material, hydrogel which is the original soft contact lens and now there is silicon hydrogel material.</h2>
    <br />
    <br />
</div>

<div id="slide29" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="23"><li>
Do you know the main benefits of the latest material silicone hydrogel over hyrogel?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Smells better" name="group1" />
        <label for="option1">&nbsp;&nbsp;Smells better</label></p>
        <p><input id="2" type="radio" value="Looks better" name="group1" />
        <label for="option2">&nbsp;&nbsp;Looks better</label></p>
        <p><input id="3" type="radio" value="Allows more oxygen to the eyes" name="group1" />
        <label for="option3">&nbsp;&nbsp;Allows more oxygen to the eyes</label></p>
        <p><input id="4" type="radio" value="Easier to handle" name="group1" />
        <label for="option3">&nbsp;&nbsp;Easier to handle</label></p>
        <p><input id="5" type="radio" value="C and D" name="group1" />
        <label for="option3">&nbsp;&nbsp;C and D</label></p>                
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <br />		
</div>


<div id="slide30" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="24"><li>
We understand that more and more patient are now wearing 1 day lenses and that silicone hydrogel is the healthier material for patients. Do you know what contact lenses are made of silicone hydrogel 1 day?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Moist" name="group1" />
        <label for="option1">&nbsp;&nbsp;Moist</label></p>
        <p><input id="2" type="radio" value="Focus dailies aqua comfort plus" name="group1" />
        <label for="option2">&nbsp;&nbsp;Focus dailies aqua comfort plus</label></p>
        <p><input id="3" type="radio" value="Proclear 1 day" name="group1" />
        <label for="option3">&nbsp;&nbsp;Proclear 1 day</label></p>
        <p><input id="4" type="radio" value="MyDay" name="group1" />
        <label for="option3">&nbsp;&nbsp;MyDay</label></p>
        <p><input id="5" type="radio" value="Biomedics 1 day Extra" name="group1" />
        <label for="option3">&nbsp;&nbsp;Biomedics 1 day Extra</label></p>                
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <br />		
</div>

<div id="slide31" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="25"><li>
What proportion of patients preferred MyDay for comfort when compared with 1 Day Acuvue Moist?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="50%" name="group1" />
        <label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="68%" name="group1" />
        <label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="70%" name="group1" />
        <label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="83%" name="group1" />
        <label for="option3">&nbsp;&nbsp;83%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <br />		
</div>

<div id="slide32" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="26"><li>
What proportion of patients preferred MyDay overall when compared with 1 Day Acuvue Moist?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="50%" name="group1" />
        <label for="option1">&nbsp;&nbsp;50%</label></p>
        <p><input id="2" type="radio" value="68%" name="group1" />
        <label for="option2">&nbsp;&nbsp;68%</label></p>
        <p><input id="3" type="radio" value="70%" name="group1" />
        <label for="option3">&nbsp;&nbsp;70%</label></p>
        <p><input id="4" type="radio" value="83%" name="group1" />
        <label for="option3">&nbsp;&nbsp;83%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <br />		
</div>

<div id="slide33" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="27"><li>
What proportion of patients preferred 1 Day Acuvue TruEye when compared with MyDay?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="14%" name="group1" />
        <label for="option1">&nbsp;&nbsp;14%</label></p>
        <p><input id="2" type="radio" value="28%" name="group1" />
        <label for="option2">&nbsp;&nbsp;28%</label></p>
        <p><input id="3" type="radio" value="58%" name="group1" />
        <label for="option3">&nbsp;&nbsp;58%</label></p>
        <p><input id="4" type="radio" value="70%" name="group1" />
        <label for="option3">&nbsp;&nbsp;70%</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <br />		
</div>

<div id="slide34" class="slide-content slide-background3 question">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />    
    <br />        
    <ol start="28"><li>
Was the level of end of day comfort for MyDay statistically significantly greater than that for 1 Day Acuvue TruEye?
    </li></ol>
    <div class="questions">    
        <p><input id="1" type="radio" value="Yes" name="group1" />
        <label for="option1">&nbsp;&nbsp;Yes</label></p>
        <p><input id="2" type="radio" value="No" name="group1" />
        <label for="option2">&nbsp;&nbsp;No</label></p>
    </div>
	<input type="image" src="img/btn_submit_smaller.png" class="quiz_submit" />		
    <br />		
</div>