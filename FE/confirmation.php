
<?php include 'includes/header.php' ?>

        <section class="confirmation w960ma">
        
            <h2>Rank to Win Game</h2>

                <article class='intro center'>
                    <h3>Please confirm your entries before submission:</h3>
                </article>
               

        </section>

		
		<div class="answers_container confirmation_answers" id="step2">
			<section class="answers">
				<p> 
					What are the top 10 attributes of MyDay&#153; Contact Lenses for the best patient experience?
					<span>Rearange the options from 1-10, with 1 being the most important</span>
				</p>
				<form>
					<ul id="answers">
						<li class="clearfix">
							<label for="option1">1.</label>
							<select id="option1"></select>							
						</li>
						<li class="clearfix">
							<label for="option2">2.</label>
							<select id="option2"></select>							
						</li>
						<li class="clearfix">
							<label for="option3">3.</label>
							<select id="option3"></select>							
						</li>
						<li class="clearfix">
							<label for="option4">4.</label>
							<select id="option4"></select>							
						</li>						
						<li class="clearfix">
							<label for="option5">5.</label>
							<select id="option5"></select>							
						</li>	
						<li class="clearfix">
							<label for="option6">6.</label>
							<select id="option6"></select>							
						</li>
						<li class="clearfix">
							<label for="option7">7.</label>
							<select id="option7"></select>							
						</li>						
						<li class="clearfix">
							<label for="option8">8.</label>
							<select id="option8"></select>							
						</li>	
						<li class="clearfix">
							<label for="option9">9.</label>
							<select id="option9"></select>							
						</li>	
						<li class="clearfix">
							<label for="option10">10.</label>
							<select id="option10"></select>							
						</li>							
					</ul>
					
					<div class="center">
						<input type="image" value="Submit" src="img/btn_submit.png" />
						<input type="image" value="Back" src="img/btn_back_large.png" />
					</div>
					<br />
					<br />
				</form>
			</section>
		</div>
		
		
		<?php include 'includes/footer.php' ?>


		<script>
		
		_PageManager.setPage("RANK");		
			
		$(function () {
			var _GameMaster = new GameMaster();
			_GameMaster.init();
			
			// delete when done
			var _sampleArray = [		
				"Daily Disposable",
				"Silicone Hydrogel with Smart Silicone chemistry",
				"High oxygen transmissiblity",
				"Easy Handling",
				"UV filter",
				"Available between -10.0D and +6.00D",
				"After 1 week of wear MyDay lenses are preferred to 1 day Acuvue TrueEye(p less than 0.01). Note: TruEye used is narafilcon a.",
				"MyDay lenses provide better end of day comfort versus 1 day Acuvue TruEye(p less than 0.001). Note: TruEye used is narafilcon a.",
				"MyDay daily disposable provides superior overall comfort compared to 1-DAY ACUVUE MOIST.",
				"MyDay daily disposable provides better comfort than 1-DAY ACUVUE MOIST throughout the day."										
			];			
						
			for (var i = 1; i < 11; i++) { 
	
				$("#option"+i).val(_sampleArray[i-1]); console.log(_sampleArray[i-1]);
				$("#option"+i).selectbox();
				$("#option"+i).selectbox("disable");
				
			};
			
			
			
			
		});

        </script>
