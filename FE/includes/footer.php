
		<footer>
			<div class="w960ma">
				<p class="fl"><a href="tnc.php">Terms and Conditions</a> | <a href="#">Privacy Policy</a></p>
				<p class="fr">&#169; 2014 CooperVision. Part of the <a href="#">Cooper Companies</a></p>
			</div>
		</footer>
 </div>        

		<!-- Load plugin scripts -->
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script type="text/javascript" src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
		<script type="text/javascript" src="js/vendor/jquery.selectbox/jquery.selectbox-0.2.js"></script>
        <script type="text/javascript" src="js/vendor/shadowbox-3.0.3/shadowbox.js"></script>       
<!--	    <script type="text/javascript" src="js/vendor/jquery.h5validate.js"></script>       -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>