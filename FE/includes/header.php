<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">


        <link rel="stylesheet" href="css/normalize.css" />
		<link rel="stylesheet" href="css/font.css" />
        <link rel="stylesheet" href="css/main.css" />
        <link rel="stylesheet" href="js/vendor/shadowbox-3.0.3/shadowbox.css" />        
        <link href="css/jquery.selectbox/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<!--		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->
                   
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
    
    <div class="container">
    
		<header>
			<div class="colorline"></div>
			<div class="header-content clearfix">
				<h1></h1>
				<h2>
                	MyDay&#153; Training Program
                </h2>
				<?php include 'includes/login-widget.php' ?>
                
                <select id="main_nav">
					<option value="index.php">Home</option>
					<option value="how-to-play.php">How to Play</option>                     
					<option value="learning-module.php">Learning Module</option>                    
					<option value="rank.php">Play the 'Rank to Win' Game</option>
					<option value="refer.php">Refer a Colleague</option>  
                </select>
			</div>
		</header>    

